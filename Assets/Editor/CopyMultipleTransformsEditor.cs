﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CopyMultipleTransforms))]
public class CopyMultipleTransformsEditor : Editor
{
    public static Transform[] copiedTransforms;
    public static Transform copiedTransform;
    public override void OnInspectorGUI()
    {
        CopyMultipleTransforms myTarget = (CopyMultipleTransforms)target;
        DrawDefaultInspector();
        if (GUILayout.Button("Copy Transforms Values"))
        {
            //copiedTransforms = myTarget.GetAllTransforms();
            //Debug.Log("copied Transforms: " + copiedTransforms.Length);
            copiedTransform = myTarget.GetTransform();
        }

        if (GUILayout.Button("Paste Transforms Values"))
        {
            //if(copiedTransforms.Length == 0)
            //{
            //    Debug.Log("Please copy transforms first.");
            //    return;
            //}
            //myTarget.SetAllTransforms(copiedTransforms);

            if(copiedTransform == null)
            {
                Debug.Log("Please copy transform first.");
                return;
            }
            myTarget.SetTransforms2(copiedTransform);
        }
    }

}