﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;
using System.Collections.Generic;

public class AlonsTools : MonoBehaviour
{

    [MenuItem("AlonTools/Clear Cache")]
    static void ClearCache() {
        Caching.ClearCache();
        Debug.Log("Cache Cleaned!");
    }
    [MenuItem("AlonTools/Change Shader")]
    static void ChangeShader()
    {
        var allGameObjects = FindObjectsOfType(typeof(GameObject));
        foreach (GameObject obj in allGameObjects)
        {
            var pot = obj.GetComponentsInChildren<Renderer>();
            foreach (var item in pot)
            {
                foreach (var material in item.sharedMaterials)
                {
                    //Debug.Log(item.material.shader);
                    if (material.shader.name.ToLower().Contains("lightweightpipeline"))
                    {
                        Shader mobileDiffuse = Shader.Find("Mobile/Diffuse");
                        if (material.shader.name.ToLower().Contains("standard"))
                        {
                            material.shader = mobileDiffuse;
                        }
                        else
                        {
                            Debug.Log(material.shader.name);
                        }
                    }
                }
            }
        }
    }



    [MenuItem("AlonTools/Select children")]
    static void SelectChildren()
    {
        List<GameObject> objects = new List<GameObject>();
        
        int childrenCount = 0;

        foreach (Transform transform in Selection.transforms)
        {
            childrenCount = transform.childCount;
            for(int i = 0; i< childrenCount; i++)
            {
                objects.Add(transform.GetChild(i).gameObject);
            }
        }
        Selection.objects = objects.ToArray();
        objects.Clear();
    }




    [MenuItem("AlonTools/Select parents")]
    static void SelectParents()
    {
        List<GameObject> objects = new List<GameObject>();

        foreach (Transform trnsfrm in Selection.transforms)
        {
                objects.Add(trnsfrm.parent.gameObject);
        }
        Selection.objects = objects.ToArray();
        objects.Clear();
        
    }




    [MenuItem("AlonTools/group objects")]
    static void GroupObjects()
    {
        List<Transform> objects = new List<Transform>();

        foreach (Transform trnsfrm in Selection.transforms)
        {
            objects.Add(trnsfrm);
        }

        Vector3 centerPivot = FindSelectionCenterPivot(objects.ToArray());
        Transform selectionParent = objects[0].parent;
        GameObject parent = new GameObject("parent");
        parent.transform.position = centerPivot;
        parent.transform.SetParent(selectionParent);

        foreach(Transform trnsfrm in objects)
        {
            trnsfrm.SetParent(parent.transform);
        }

        objects.Clear();
    }


    static Vector3 FindSelectionCenterPivot(Transform[] trans)
    {
        if (trans == null || trans.Length == 0)
            return Vector3.zero;
        if (trans.Length == 1)
            return trans[0].position;

        float minX = Mathf.Infinity;
        float minY = Mathf.Infinity;
        float minZ = Mathf.Infinity;

        float maxX = -Mathf.Infinity;
        float maxY = -Mathf.Infinity;
        float maxZ = -Mathf.Infinity;

        foreach (Transform tr in trans)
        {
            if (tr.position.x < minX)
                minX = tr.position.x;
            if (tr.position.y < minY)
                minY = tr.position.y;
            if (tr.position.z < minZ)
                minZ = tr.position.z;

            if (tr.position.x > maxX)
                maxX = tr.position.x;
            if (tr.position.y > maxY)
                maxY = tr.position.y;
            if (tr.position.z > maxZ)
                maxZ = tr.position.z;
        }

        return new Vector3((minX + maxX) / 2.0f, (minY + maxY) / 2.0f, (minZ + maxZ) / 2.0f);
    }




}
