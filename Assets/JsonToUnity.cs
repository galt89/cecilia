using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using UnityEngine.UI;
using System;

public class JsonToUnity : MonoBehaviour
{
    public TextAsset jsonfile;

    [System.Serializable]
    public class Quiz
    {
        public string question;
        public string[] options;
        public string answer;
    }

    [System.Serializable]
    public class Quizlist
    {
        public Quiz[] Quiz;
    }

    public Quizlist myQuizList = new Quizlist();


    // Start is called before the first frame update
    private void Update()
    {
        myQuizList = JsonUtility.FromJson<Quizlist>(jsonfile.text);

    }
}