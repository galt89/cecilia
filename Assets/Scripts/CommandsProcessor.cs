﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public partial class CommandsProcessor : MonoBehaviour {

    public Manager manager;
    public TouchScreenUIManager UiScreenTouch;

    public bool processingCommand = false;
    public bool activatingMenuCommand;
    bool isDrink = false;
    
    public ConversationCommand currentConversationCommand;

    WaitForSeconds waitCheckCupTime = new WaitForSeconds(0.03f);

    string userPreviosDrink;

    ConversationCommand userPreviosCommand;

    bool isWaitingForDrinkQuestion;

    string[] positiveAnswers = { "yes", "ok", "why not", "sure", "yeah"};
    string[] negativeAnswers = { "no", "negative", "dont", "nea" };
    string[] responseSeperator = new string[] { "_ ", "_", " _", " _ " };

    string currentCommandResponse1;
    float currentCommandWaitTime;
    string currentCommandResponse2;
    string currentResponesAmin1;
    string currentResponesAnim2;

    public int notUnderstandIndex;

    //bool isWaitingToEndConversation = false;
    bool isWithFemiliarFace = false;
    UnityAction currentAction;
    WaitForSeconds waitCupTime = new WaitForSeconds(0.1f);

    public MicIndicationHandler MicLoudness;


    private void OnEnable()
    {
        manager.OnCupIn += this.OnCupIn;
        //manager.OnCupOut += this.OnCupOut;
    }

    private void OnDisable()
    {
        manager.OnCupIn -= this.OnCupIn;
        //manager.OnCupOut -= this.OnCupOut;
    }


    public void ProcessCommand(string inputWords)
    {
        if (processingCommand || activatingMenuCommand || isDrink)
        {
            return;
        }
        processingCommand = true;
        
        manager.uiManager.ShowUserTxt(inputWords);
        inputWords = inputWords.Trim().ToLower();
        

        string[] currentInputWords = inputWords.Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
       
        currentCommandWaitTime = 0;
        currentConversationCommand = null;

        if (isWaitingForDrinkQuestion)
        {
            isWaitingForDrinkQuestion = false;
            foreach (string inputWord in currentInputWords)
            {
                foreach(string positiveWord in positiveAnswers)
                {
                    if(inputWord == positiveWord)
                    {
                        Debug.Log("answering positive in: input: " + inputWord + ", positive word: " + positiveWord);
                        currentConversationCommand = userPreviosCommand;
                        RunCommand();
                        userPreviosCommand = null;
                        return;
                    }
                }

                if (currentInputWords.Length < 4)
                {
                    foreach (string negativeWord in negativeAnswers)
                    {
                        if (inputWord == negativeWord)
                        {
                            manager.textToSpeechHandler.Speak("Ok, what would you like to drink?");
                            manager.uiManager.ShowBarmanTxt("Ok, what would you like to drink?");
                            userPreviosCommand = null;
                            return;
                        }
                    }
                }
            }
        }

        foreach (ConversationCommand cmnd in manager.conversationSettings.conversationCommands)
        {
            foreach (string keyword in cmnd.keywords)
            {
                if (keyword.Trim().Contains(" "))
                {
                    if (inputWords.Contains(keyword.ToLower().Trim()))
                    {
                        currentConversationCommand = cmnd;
                        RunCommand();
                        return;
                    }
                }
                else
                {
                    foreach (string inputWord in currentInputWords)
                    {
                        if (string.Compare(inputWord.Trim(), keyword.Trim(), true) == 0)
                        {
                            currentConversationCommand = cmnd;
                            RunCommand();
                            return;
                        }
                    }
                }
            }
        }
       
        OnUnknownExpression(inputWords);
    }


    void OnUnknownExpression(string inputWords)
    {
        manager.sessionLogManager.AddUnknownExpression(inputWords);
        UpdateActivationResponseData(manager.notUnderstandCommand);
        manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
        manager.textToSpeechHandler.Speak(currentCommandResponse1);
        manager.animationsManager.PlayAnimation(currentResponesAmin1);
    }



    void RunCommand()
    {
        isDrink = currentConversationCommand.isDrink == 1 ? true : false;
        notUnderstandIndex = 0;
        UpdateConversationResponseData(currentConversationCommand);

        manager.textToSpeechHandler.Speak(currentCommandResponse1);
        manager.animationsManager.PlayAnimation(currentResponesAmin1);

        manager.uiManager.ShowBarmanTxt(currentCommandResponse1);

        if (currentConversationCommand.output != "" && currentConversationCommand.isDrink == 1)
        {

            manager.touchScreenUIManager.ShowWaitScreen();
            Debug.Log("sending drink to arduino: " + currentConversationCommand.output);
            manager.arduinoHandler.SendToArdu(currentConversationCommand.output);
            string drinkName = currentConversationCommand.drinkName;
            manager.faceRecognitionManager.RegisterADrinkToAUser(drinkName, currentConversationCommand);
            manager.sessionLogManager.AddDrink(currentConversationCommand.drinkName);
            isDrink = true;
            manager.firestoreManager.SendDrinkData(activatingMenuCommand, currentConversationCommand.drinkName, currentConversationCommand.output);

        }
    }


    public void OnDoneSpeaking()
    {
        StartCoroutine(OnDoneSpeakingCo());
    }

    IEnumerator OnDoneSpeakingCo(){
        activatingMenuCommand = false;
        if (currentCommandWaitTime > 0) {
            if (isDrink)
            {
                manager.withUser = true;
                manager.animationsManager.PlayAnimation("loop_shaker", currentCommandWaitTime);
            }

            Debug.Log ("Waiting for another speak... time: " + currentCommandWaitTime);
            yield return new WaitForSeconds(currentCommandWaitTime);
			
            currentCommandWaitTime = 0;
			
            manager.uiManager.ShowBarmanTxt(currentCommandResponse2);
            manager.textToSpeechHandler.Speak (currentCommandResponse2);
            manager.animationsManager.PlayAnimation(currentResponesAnim2);
        } else {
            currentConversationCommand = null;
            processingCommand = false;
           
            if (isDrink)
            {
                isDrink = false;
                OnDrinkReady();
            }
            else
            {
                if (manager.withUser)
                {
                    if (manager.voiceActivationManager.IsRecording)
                    {
                        manager.voiceActivationManager.StopRecording(false, "OnDoneSpeaking() - VoiceActivationManager is still recording...");
                        while (manager.voiceActivationManager.IsRecording)
                        {
                            yield return null;
                        }
                    }
                    
                    if (manager.isIDScanEnabled)
                    {
                        if (!manager.isCheckingID)
                        {
                            if (manager.isIDApproved)
                            {
                                manager.voiceDictationManager.StartRecording("OnDoneSpeakingCo() - !isDrink - withUser - ID Approved.");
                                manager.touchScreenUIManager.ShowDrinksMenu();
                            }
                            else
                            {
                                manager.iDScannerManager.OnPleaseShowID();
                            }
                        }
                    }
                    else
                    {
                        manager.voiceDictationManager.StartRecording("OnDoneSpeakingCo() - !isDrink - withUser");
                        manager.touchScreenUIManager.ShowDrinksMenu();
                    }
                }
                else
                {
                    manager.animationsManager.PlayWaitAnims();
                    if (manager.voiceDictationManager.IsRecording)
                    {
                        manager.voiceDictationManager.StopRecording(false, false, "OnDoneSpeaking() - VoiceDictationManager is still recording...");
                        while (manager.voiceDictationManager.IsRecording)
                        {
                            yield return null;
                        }
                    }
                    manager.voiceActivationManager.StartRecording(false, "OnDoneSpeakingCo() - !isDrink - !withUser");
                }
            }
        }
	}





    void OnCupIn()
    {
        if (!manager.isTrueRecording) return;
        StartCoroutine(OnCupInRoutine());
    }
    IEnumerator OnCupInRoutine()
    {
        if (manager.withUser)
            yield break;

        if (manager.voiceActivationManager.IsRecording)
        {
            manager.voiceActivationManager.StopRecording(false, "ListenToCupState() - isNeedToPutCupIn");
            while (manager.voiceActivationManager.IsRecording)
                yield return null;
        }

        if (manager.voiceDictationManager.IsRecording)
        {
            manager.voiceDictationManager.StopRecording(false, false, "ListenToCupState() - isNeedToPutCupIn");
            while (manager.voiceDictationManager.IsRecording)
                yield return null;
        }


        if (manager.switchingCharacters)
        {
            while (manager.switchingCharacters)
                yield return waitCheckCupTime;
        }

        if (isWithFemiliarFace)
        {
            isWithFemiliarFace = false;
            OnFemiliarFaceRecognized(userPreviosDrink, userPreviosCommand);
        }
        else if (currentAction != null)
        {
            currentAction.Invoke();
            currentAction = null;
        }
        else
        {
            OnCupOrTouchDetected();
        }
    }

    
   

    public void ResetAll()
    {
        manager.touchScreenUIManager.DeactivateDrinksMenu();
        StopAllCoroutines();
        processingCommand = false;
        currentCommandWaitTime = 0;
        isDrink = false;
        currentConversationCommand = null;
        userPreviosDrink = "";
        userPreviosCommand = null;
        isWaitingForDrinkQuestion = false;
    }


    public void UpdateActivationResponseData(ActivationCommand activationCommand)
    {
        int randomResponse = Random.Range(0, activationCommand.responses.Length);
        currentResponesAmin1 = activationCommand.anims1[randomResponse];
        currentResponesAnim2 = activationCommand.anims2[randomResponse];
        string response = activationCommand.responses[randomResponse].Trim();
        if (response.Contains("_"))
        {
            string[] responses = response.Split(responseSeperator, System.StringSplitOptions.RemoveEmptyEntries);
            if (responses.Length > 2)
            {
                currentCommandResponse1 = responses[0];
                currentCommandWaitTime = float.Parse(responses[1]);
                currentCommandResponse2 = responses[2];
                return;
            }
        }
        else
        {
            currentCommandResponse1 = response;
            currentCommandWaitTime = 0;
            currentCommandResponse2 = "";
        }
    }


    public void UpdateConversationResponseData(ConversationCommand conversationCommand)
    {
        int randomResponseIndex = Random.Range(0, conversationCommand.responses.Length);
        currentResponesAmin1 = conversationCommand.anims1[randomResponseIndex];
        currentResponesAnim2 = conversationCommand.anims2[randomResponseIndex];
        string response = conversationCommand.responses[randomResponseIndex].Trim();
        if (response.Contains("_"))
        {
            string[] responses = response.Split(responseSeperator, System.StringSplitOptions.RemoveEmptyEntries);
            if (responses.Length > 2)
            {
                currentCommandResponse1 = responses[0];
                currentCommandWaitTime = float.Parse(responses[1]);
                currentCommandResponse2 = responses[2];
            }
            else
            {
                currentCommandResponse1 = response;
                currentCommandWaitTime = 0;
                currentCommandResponse2 = "";
            }
        }
        else
        {
            currentCommandResponse1 = response;
            currentCommandWaitTime = 0;
            currentCommandResponse2 = "";
        }
    }

    
    public IEnumerator ActivateNewCommand(ConversationCommand newCommand)
    {
        if (activatingMenuCommand || isDrink) yield break;

        activatingMenuCommand = true;

        if (manager.voiceDictationManager.IsRecording)
        {
            manager.voiceDictationManager.StopRecording(false, false, "ActivateNewCommand() - Activate touch menu Command");
            while (manager.voiceDictationManager.IsRecording)
                yield return null;
        }

        //if no internet show message and stop session, else make the drink.
        manager.checkInternet.Check();
        yield return new WaitForSeconds(0.1f);
        while (manager.checkInternet.isCheckingNetwork)
            yield return null;

        if (manager.checkInternet.isNetworktError)
        {
            Manager.instance.uiManager.barmanTextUI.text = "Please check your internet connection.";
            DebugLogTxt.instance.Log("internet connection error.", true);
            manager.errorLogger.AddLog("Drinks menu button - Internet connection error.");
            yield break;
        }

        if (manager.voiceActivationManager.IsRecording)
        {
            manager.voiceActivationManager.StopRecording(false, "ActivateNewCommand");
            while (manager.voiceActivationManager.IsRecording)
                yield return null;
        }

        manager.faceRecognitionManager.ResetFaceRecognition();
        processingCommand = true;
        currentConversationCommand = newCommand;
        RunCommand();
        manager.touchScreenUIManager.EnableDrinkBtn();
    }




}
