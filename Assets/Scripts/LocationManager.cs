﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationManager : MonoBehaviour
{
    
    [SerializeField] float _latitude;
    [SerializeField] float _longitude;
    public string status = "not started";

    public float Latitude
    {
        get
        {
            if (_latitude == 0)
            {
                StartCoroutine(GetLocation());
            }
            return _latitude;
        }
    }

    public float Longitude
    {
        get
        {
            if (_longitude != 0)
            {
                StartCoroutine(GetLocation());
            }
            return _longitude;
        }
    }


    


 
    public void TryInitLocation()
    {
        if(_latitude == 0 || _longitude == 0)
        {
            StartCoroutine(GetLocation());
        }
    }

    IEnumerator GetLocation()
    {
        Debug.Log("trying to init location...");
        // First, check if user has location service enabled

        if (!Input.location.isEnabledByUser)
        {
            DebugLogTxt.instance.Log("Location service not enabled.");
            status = "Service not enabled";
            Debug.Log("Location Service not enabled");
            yield break;
        }
            

        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 10;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            DebugLogTxt.instance.Log("Location service time out.");
            Manager.instance.errorLogger.AddLog("Location service time out.");
            status = "service timeout";
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            DebugLogTxt.instance.Log("Location service - Unable to determine device location.");
            Manager.instance.errorLogger.AddLog("Location service - Unable to determine device location.");
            status = "service error";
            yield break;
        }
        else
        {
            // Access granted and location value could be retrieved
            status = "active";
            _latitude = Input.location.lastData.latitude;
            _longitude = Input.location.lastData.longitude;
            print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
            DebugLogTxt.instance.Log("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude);
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }
}
