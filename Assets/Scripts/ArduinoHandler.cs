﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

public class ArduinoHandler : MonoBehaviour {

	public static ArduinoHandler instance; // Ofek Changes

	public static Manager manager;
	public SerialPort serial;
    string portName;
	public InputField txtField;
	public bool isCupIn;
	public bool isReading = false;
    public string readValue;
	public int intValue = -1;
	public bool noPort;
	public bool portSet;
	public Text debugArduInput;
	public Text debugArduLength;
	public Text readInputTxt;

	//string valueForDebug;
	string positiveValue = "2";
	string negativeValue = "1";
	WaitForSeconds waitTime = new WaitForSeconds(0.2f);


    void Start(){
		GetPortFromPrefs ();
	}


	void GetPortFromPrefs(){

		if (serial != null && serial.IsOpen) {
			serial.Close ();
			Debug.Log ("Closing serial port!");
		}

		//portName = PlayerPrefs.GetString ("port_name");

		portName = SerialPort.GetPortNames()[0]; // The port name is automatically pulled from a list of existing ports. In this case only 1 port is active so the port in the first index is used.

		if (String.IsNullOrEmpty (portName))
			return;


		txtField.text = portName;

		if (portName == "off" || portName == "no" || portName == "non") {
			noPort = true;
			portSet = true;
			return;
		}

		if(!string.IsNullOrEmpty(portName))
			Init ();
	}


	public void GetPort(){ // This method is called only when we click ENTER on the port input field.
		if (serial != null && serial.IsOpen) {
			serial.Close ();
			Debug.Log ("Closing serial port!");
		}

		portName = txtField.text;
		PlayerPrefs.SetString ("port_name", portName);
		PlayerPrefs.Save ();

		if (portName == "off" || portName == "no" || portName == "non") {
			noPort = true;
			portSet = true;
			return;
		}

		Init ();
	}

	void Init () {
		if (string.IsNullOrEmpty(portName))
        {
			DebugLogTxt.instance.Log("Arduino init error - Please set port name.", true);
			manager.errorLogger.AddLog("Arduino init error - Please set port name.");
			return;
		}

		try{
		serial = new SerialPort(portName , 9600);
		}catch(Exception error){
			Debug.LogError (error);
			Manager.instance.uiManager.barmanTextUI.text = "port error - " + error;
			DebugLogTxt.instance.Log("port error - " + error, true);
			manager.errorLogger.AddLog("port error - " + error);
			return;
		}

		serial.ReadTimeout = 100;
		
		if (serial.IsOpen) {
			serial.Close ();
			Debug.Log ("Closing serial port!");
		} else {
			serial.Open ();
			Debug.Log ("Openning serial port!");
		}

		portSet = true;
	}





	public void SendToArdu(string output){
		if (serial == null || noPort)
        {
			if (DebugLogTxt.instance)
				DebugLogTxt.instance.Log("Trying to send data to Arduino : " + output + ", noPort = " + noPort, true);
			return;
		}
			

		if (!serial.IsOpen) {
			try{
			serial.Open ();
			}catch{
				Manager.instance.uiManager.barmanTextUI.text = "Check port name";
				return;
			}
		}

		try
		{
			serial.WriteLine(output);
			serial.BaseStream.Flush();
			if (DebugLogTxt.instance)
				DebugLogTxt.instance.Log("Data sent to Arduino : " + output);
		}
		catch (Exception e)
		{
			Manager.instance.uiManager.barmanTextUI.text = "Check port name";
			if (DebugLogTxt.instance)
				DebugLogTxt.instance.Log("Arduino excaption: " + e.ToString(), true);
		}

		Debug.Log("Sending " + output + " to arduino");
	}




	public void StartReadFromArduino()
	{
		if (isReading)
			return;

		if (serial == null || noPort)
			return;

		if (!serial.IsOpen)
		{
			try
			{
				serial.Open();
			}
			catch
			{
				Manager.instance.uiManager.barmanTextUI.text = "Check port name";
				return;
			}
		}

		StartCoroutine(ReadFromArduino());
	}

	
	IEnumerator ReadFromArduino(){
        isReading = true;
		while (isReading) {
			yield return waitTime;
			if (serial.IsOpen)
			{
				try
				{
					readValue = serial.ReadExisting (); //Read the information
					
					if (!string.IsNullOrEmpty(readValue))
					{
						//valueForDebug = readValue;
						if (readValue.Contains(positiveValue))
						{
                            if (!isCupIn)
                            {
								isCupIn = true;
								manager.OnCupIn.Invoke();
								if (manager.isDebugModeOn)
                                {
									Debug.Log("Cup state is in");
									DebugLogTxt.instance.Log("cup in event");
								}
                            }
						}
						else if (readValue.Contains(negativeValue))
						{
							if (isCupIn)
							{
								//if (!CommandsProcessor.instance.isNeedToTakeCupOut)
								//	CommandsProcessor.instance.isNeedToPutCupIn = true;
								isCupIn = false;
								//manager.OnCupOut.Invoke();
								if (manager.isDebugModeOn)
								{
									Debug.Log("Cup state is out");
									DebugLogTxt.instance.Log("cup out event");
								}
							}
						}
						if (Manager.instance.isDebugModeOn)
						{
							readInputTxt.text = "cup state = " + readValue;
							debugArduInput.text = "cup state = " + readValue;
							debugArduLength.text = "arduino Length = " + readValue.Length.ToString();
						}
					}
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
			else
			{
				try
				{
					serial.Open();
				}
				catch (Exception e)
				{
					Debug.LogError(e);
					Manager.instance.uiManager.barmanTextUI.text = "Check port name";
				}
			}
		}
	}




	public void StopReadFromArduino()
    {
        isReading = false;
    }



}
