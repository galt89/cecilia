﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class JsonManager : MonoBehaviour {


	string activationJSonPath;
	string conversationJSonPath;
	string activationJsonString;
	string conversationJsonString;





	void Start(){

		activationJSonPath = Application.dataPath + "/StreamingAssets/RobobarData/Settings/activationData.json";
		conversationJSonPath = Application.dataPath + "/StreamingAssets/RobobarData/Settings/conversationData.json";

		GetDataFromJson ();
	}




	void GetDataFromJson(){
		activationJsonString = File.ReadAllText(activationJSonPath);
		conversationJsonString = File.ReadAllText(conversationJSonPath);
		Manager.instance.activationSettings = JsonUtility.FromJson<ActivationSettings>(activationJsonString);
		Manager.instance.conversationSettings = JsonUtility.FromJson<ConversationSettings>(conversationJsonString);
		Manager.instance.SetSettingsScreen ();
	}




	public void SetNewConversationData(ConversationCommand[] commands)
	{
		Manager.instance.conversationSettings.conversationCommands = commands;
		
		conversationJsonString = JsonUtility.ToJson(Manager.instance.conversationSettings, true);
		Debug.Log("Conversation Settings as jSon:\n" + conversationJsonString);

		if (!Directory.Exists(Path.GetDirectoryName(conversationJSonPath)))
		{
			Directory.CreateDirectory(Path.GetDirectoryName(conversationJSonPath));
			Debug.Log("Directory NOT Exists - Created");
		}

		File.WriteAllText(conversationJSonPath, conversationJsonString);
	}


	public void SetNewActivationData(ActivationCommand[] commands)
	{
		Manager.instance.activationSettings.activationCommands = commands;
		
		activationJsonString = JsonUtility.ToJson(Manager.instance.activationSettings, true);
		Debug.Log("Activation Settings as jSon:\n" + activationJsonString);

		if (!Directory.Exists(Path.GetDirectoryName(activationJSonPath)))
		{
			Directory.CreateDirectory(Path.GetDirectoryName(activationJSonPath));
			Debug.Log("activation Directory NOT Exists - Created");
		}

		File.WriteAllText(activationJSonPath, activationJsonString);
	}


	

}





[Serializable]
public class ActivationSettings
{
	public ActivationCommand[] activationCommands;
}

[Serializable]
public class ConversationSettings
{
	public ConversationCommand[] conversationCommands;
}




[Serializable]
public class ConversationCommand
{
	public string[] keywords;
	public string[] responses;
	public string[] anims1;
	public string[] anims2;
	public string drinkName;
	public string output;
	public int isDrink;
	//public string drinkId; //Ofek changes
}


[Serializable]
public class ActivationCommand
{
	public string[] responses;
	public string[] anims1;
	public string[] anims2;
}



