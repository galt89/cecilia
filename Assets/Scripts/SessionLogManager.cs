﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class SessionLogManager : MonoBehaviour
{

    public List<string> unknownExpressions;

    public SessionLog sessionLog;
    public bool isSavingLog;
    public List<DrinkLog> drinksLogs = new List<DrinkLog>();
    public string jsonFilePath;


    

   

    public void StartNewSession()
    {
        sessionLog = new SessionLog();
        sessionLog.dateTime = DateTime.Now.ToString();
        string dateTime = DateTime.Now.ToString();
        dateTime = dateTime.Replace('/', '_');
        dateTime = dateTime.Replace(':', '_');
        jsonFilePath = Application.streamingAssetsPath + "/RobobarData/SessionLogs/session_log_" + dateTime + ".json";
       
        drinksLogs = new List<DrinkLog>();
        unknownExpressions = new List<string>();
    }

    public void AddDrink(string currentDrinkName)
    {
        if(drinksLogs == null)
        {
            DebugLogTxt.instance.Log("Session Log - AddDrink - drinksLogs = null!, Starting new log...");
            StartNewSession();
        }

        for (int i = 0; i < drinksLogs.Count; i++)
        {
            if (drinksLogs[i].drinkName.Equals(currentDrinkName))
            {
                drinksLogs[i].amount = drinksLogs[i].amount + 1;
                return;
            }
        }
        drinksLogs.Add(new DrinkLog(currentDrinkName, 1));
    }


    public void AddUnknownExpression(string expression)
    {
        for (int i = 0; i < unknownExpressions.Count; i++)
        {
            if (unknownExpressions[i].Equals(expression))
                return;
        }
        unknownExpressions.Add(expression);
    }



    public void SaveCurrentSession()
    {
        if (isSavingLog) return;

        isSavingLog = true;
        if(sessionLog == null || sessionLog.drinksOrdered == null || sessionLog.unknownExpressions == null || drinksLogs == null || unknownExpressions == null)
        {
            isSavingLog = false;
            return;
        }
        sessionLog.drinksOrdered = drinksLogs.ToArray();
        sessionLog.unknownExpressions = unknownExpressions.ToArray();
        if (sessionLog.drinksOrdered.Length == 0 && sessionLog.unknownExpressions.Length == 0)
        {
            isSavingLog = false;
            return;
        }
            
        string jsonSessionLog = JsonUtility.ToJson(sessionLog, true);
        if (string.IsNullOrEmpty(jsonSessionLog))
        {
            isSavingLog = false;
            return;
        }
            

        if (!File.Exists(jsonFilePath))
        {
            StreamWriter sw = File.CreateText(jsonFilePath);
            sw.Close();
        }

        StartCoroutine(SaveCurrentSessionCo(jsonSessionLog));
    }

    IEnumerator SaveCurrentSessionCo(string jsonSessionLog)
    {
        yield return new WaitForSeconds(0.2f);
        
        if (File.Exists(jsonFilePath))
        {
            File.WriteAllText(jsonFilePath, jsonSessionLog);
        }
        else
        {
            StreamWriter sw = File.CreateText(jsonFilePath);
            yield return new WaitForEndOfFrame();
            sw.Write(jsonFilePath);
            sw.Close();
        }
        sessionLog = null;
        drinksLogs = null;
        unknownExpressions = null;
        yield return new WaitForSeconds(0.2f);
        isSavingLog = false;
    }
}





[System.Serializable]
public class SessionLog
{
    public string dateTime;
    public string[] unknownExpressions;
    public DrinkLog[] drinksOrdered;
    public SessionLog()
    {
        dateTime = "";
        unknownExpressions = new string[0];
        drinksOrdered = new DrinkLog[0];
    }
}

[System.Serializable]
public class DrinkLog
{
    public string drinkName;
    public int amount;
    public DrinkLog(string name, int value)
    {
        drinkName = name;
        amount = value;
    }
    
}
