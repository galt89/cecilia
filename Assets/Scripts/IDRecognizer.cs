﻿using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IDAnalyzer;
using System.IO;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;

public class IDRecognizer : MonoBehaviour
{
    CoreAPI coreapi;
    string sourcePath;
    
    [SerializeField] RawImage rawImage;

    void Start()
    {
        Initialize();
    }

   

    private void Initialize()
    {
        coreapi = new CoreAPI("NXULSi0LVHf12c6DYjwZgJt1q5mhzwho", "EU");
    }


    public void TryTestDrivingLicanse()
    {
        sourcePath = Path.Combine(Application.streamingAssetsPath, "tests/licenseTest.jpg");
        ScanID(sourcePath);
    }

    public void LoadImage(string path, Action<string> OnSuccess = null, Action<string> OnFailed = null)
    {
        sourcePath = path;

        //StartCoroutine(GetTexture(sourcePath));
       
        ScanID(path, OnSuccess, OnFailed);
    }

   

    async void ScanID(string sourcePath, Action<string> OnSuccess = null, Action<string> OnFailed = null)
    {
        try
        {
            JObject result = await coreapi.Scan(sourcePath);
            // Print document holder name
           
            //Debug.Log((string)result.SelectToken("result.firstName"));
            Debug.Log((string)result.SelectToken("result.fullName"));
            //txt.text += "\n" + (string)result.SelectToken("result.fullName");
            string age = (string)result.SelectToken("result.age");
            Debug.Log((string)result.SelectToken("result.age"));
            DebugLogTxt.instance.Log((string)result.SelectToken("result.age"));
            
            Debug.Log((string)result.SelectToken("result.dob"));
            //txt.text += "\n" + (string)result.SelectToken("result.dob");
            Debug.Log((string)result.SelectToken("result.issuerOrg_full"));
            //txt.text += "\n" + (string)result.SelectToken("result.issuerOrg_full");
            // Parse document authentication results
            if (result.ContainsKey("authentication"))
            {
                if ((double)result.SelectToken("authentication.score") > 0.5)
                {
                    Debug.Log("The document uploaded is authentic");
                    DebugLogTxt.instance.Log("The document uploaded is authentic");
                }
                else if ((double)result.SelectToken("authentication.score") > 0.3)
                {
                    Debug.Log("The document uploaded looks little bit suspicious");
                    DebugLogTxt.instance.Log("The document uploaded looks little bit suspicious");
                }
                else
                {
                    Debug.Log("The document uploaded is fake");
                    DebugLogTxt.instance.Log("The document uploaded is fake");
                }
            }
            OnSuccess(age);
        }
        catch (APIException e)
        {
            Debug.Log("Error Code: " + e.ErrorCode);
            DebugLogTxt.instance.Log(e.ErrorCode.ToString());
            Debug.Log("Error Message: " + e.Message);
            DebugLogTxt.instance.Log(e.Message.ToString());
            // View complete list of error codes under API reference: https://developer.idanalyzer.com/
            switch (e.ErrorCode)
            {
                case 1:
                    Debug.Log("Invalid API Key");
                    break;
                case 8:
                    Debug.Log("Out of API quota");
                    break;
                case 9:
                    Debug.Log("Document not recognized");
                    break;
                default:
                    Debug.Log("Other error");
                    break;
            }
            OnFailed("Error Code: " + e.ErrorCode + ", Error Message: " + e.Message);
        }
        catch (ArgumentException e)
        {
            Debug.Log("Argument Error! " + e.Message);
            DebugLogTxt.instance.Log("Argument Error! " + e.Message);
            OnFailed("Argument Error! " + e.Message);
        }
        catch (Exception e)
        {
            Debug.Log("Unexpected Error! " + e.Message);
            DebugLogTxt.instance.Log("Unexpected Error! " + e.Message);
            OnFailed("Unexpected Error! " + e.Message);
        }
    }


    IEnumerator GetTexture(string path) //just for debugging, not in use.
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(path))
        {
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.LogError(uwr.error);
            }
            else
            {
                // Get downloaded asset bundle
                //loadedTexture = DownloadHandlerTexture.GetContent(uwr);
                rawImage.texture = DownloadHandlerTexture.GetContent(uwr);
            }
        }
    }
}
