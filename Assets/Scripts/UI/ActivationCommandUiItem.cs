﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivationCommandUiItem : MonoBehaviour
{
    public GameObject commandUI;
    
    public bool isActive = false;
   
    [HideInInspector]
    public string responseTxt;
   
    public ActivationCommand command = new ActivationCommand();

    SettingsScreenHandler settingsScrn;

    GameObject newResponseElement;
    public GenericResponseUiItem[] responsesItems;
    

    private void Awake()
    {
        settingsScrn = FindObjectOfType<SettingsScreenHandler>();
    }

    public void ActivateNewCommandItem(string[] responses = null, string[] anims1 = null, string[] anims2 = null)
    {
        if(settingsScrn == null)
            settingsScrn = FindObjectOfType<SettingsScreenHandler>();

        if (responses == null || responses.Length == 0)
        {
            Debug.Log("Created empty command item");
            AddCommandItem();
            return;
        }

        command.responses = responses;
        command.anims1 = anims1;
        command.anims2 = anims2;

        isActive = true;

        for (int i = 0; i < responses.Length; i++)
        {
            newResponseElement = Instantiate(settingsScrn.GenericResponseCommandUiItemPrefab, transform);
            GenericResponseUiItem genericResponseUiItem = newResponseElement.GetComponent<GenericResponseUiItem>();
            genericResponseUiItem.SetAnimationsDropDowns(settingsScrn.animationsNames);
            genericResponseUiItem.SetResponse(responses[i], GetAnimIndex(anims1[i]), GetAnimIndex(anims2[i]));
            if (i == 0)
            {
                newResponseElement.GetComponent<GenericResponseUiItem>().removeBtn.gameObject.SetActive(false);
            }
        }
        settingsScrn.RefreshLayoutGroup();
    }




    public void AddCommandItem()
    {
        newResponseElement = Instantiate(settingsScrn.GenericResponseCommandUiItemPrefab, transform);
        GenericResponseUiItem genericResponseUiItem = newResponseElement.GetComponent<GenericResponseUiItem>();
        genericResponseUiItem.SetAnimationsDropDowns(settingsScrn.animationsNames);
       
        isActive = true;
        settingsScrn.RefreshLayoutGroup();
    }



    public void AddGenericResponse()
    {
        newResponseElement = Instantiate(settingsScrn.GenericResponseCommandUiItemPrefab, transform);
        // set animations dropdowns:
        newResponseElement.GetComponent<GenericResponseUiItem>().SetAnimationsDropDowns(settingsScrn.animationsNames);

        settingsScrn.RefreshLayoutGroup();
    }



    public void GatherResponses()
    {
        responsesItems = GetComponentsInChildren<GenericResponseUiItem>();
    }


    public void StartTyping()
    {
        if (settingsScrn == null)
            return;

        settingsScrn.activationSaveBtn.interactable = true;
        settingsScrn.isNeedToSaveActivationsList = true;
        //Debug.Log("Start Typing Activation 2");
    }




    public void SetCommandItem(string[] responses = null, string[] anims1 = null, string[] anims2 = null)
    {
        isActive = true;

        command.responses = responses;
        command.anims1 = anims1;
        command.anims2 = anims2;
       
    }


    int GetAnimIndex(string animName)
    {
        return settingsScrn.animationsNames.IndexOf(animName);
    }


}
