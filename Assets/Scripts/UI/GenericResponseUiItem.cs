﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GenericResponseUiItem : MonoBehaviour
{
    public InputField inpuField;
    public Button removeBtn;
    public Dropdown anims1;
    public Dropdown anims2;
    //public GenericResponse genericResponse;
    public ActivationCommandUiItem activationCommandUiItem;
    public ConversationCommandUiItem conversationCommandUiItem;

    private void Start()
    {
        removeBtn.onClick.AddListener(() => RemoveItem());

        activationCommandUiItem = GetComponentInParent<ActivationCommandUiItem>();
        conversationCommandUiItem = GetComponentInParent<ConversationCommandUiItem>();
    }

    private void OnEnable()
    {
        activationCommandUiItem = GetComponentInParent<ActivationCommandUiItem>();
        conversationCommandUiItem = GetComponentInParent<ConversationCommandUiItem>();
    }

    public void SetAnimationsDropDowns(List<string> animationsNames)
    {
        // set animations dropdowns:
        //Debug.Log("settingsScrn.animationsNames: " + settingsScrn.animationsNames.Count);
        anims1.ClearOptions();
        anims1.AddOptions(animationsNames);
        anims1.value = 0;

        anims2.ClearOptions();
        anims2.AddOptions(animationsNames);
        anims2.value = 0;
    }

    void RemoveItem()
    {
        Destroy(gameObject);
        SetUnsavrdItem();
        Manager.instance.settingsScrnHandler.RefreshLayoutGroup();
    }

   

    public void SetResponse(string response,int anim1Ind, int anim2Ind)
    {
        inpuField.text = response;
        //inpuField.caret
        anims1.value = anim1Ind;
        anims2.value = anim2Ind;
    }


   

    public void SetUnsavrdItem()
    {
        if (activationCommandUiItem != null)
        {
            activationCommandUiItem.StartTyping();
            //Debug.Log("Start Typing Activation");
        }
            
        if (conversationCommandUiItem)
        {
            conversationCommandUiItem.StartTyping();
            //Debug.Log("Start Typing Conversation");
        }
            
    }
}


