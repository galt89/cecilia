﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversationCommandUiItem : MonoBehaviour
{

    public GameObject commandUI;
    public GameObject addBtn;
    public GameObject firstResponse;
    public bool isActive = false;
    public bool isDrink = false;
    public Toggle ActiveCocktail;

    public InputField keywordsTxtField;
    public InputField drinkNameField;
    public InputField outputTxtField;
   // public InputField DrinkIDField;

    [HideInInspector]
    public string keywordsTxt;
    [HideInInspector]
    public string responseTxt;
    [HideInInspector]
    public string outputTxt;

    public ConversationCommand command = new ConversationCommand();

    SettingsScreenHandler settingsScrn;

    GameObject newResponseElement;
    public GenericResponseUiItem[] responsesItems;
    



    private void Awake()
    {
        settingsScrn = FindObjectOfType<SettingsScreenHandler>();
    }




    public void ActivateNewCommandItem(string[] keywords = null, string[] responses = null, string[] anims1 = null, string[] anims2 = null, string drinkName = "", string output = "", string drinkId = "")
    {
        if(settingsScrn == null)
            settingsScrn = FindObjectOfType<SettingsScreenHandler>();

        if (responses == null || responses.Length == 0)
        {
            //Debug.Log("Created empty command item");
            return;
        }

        command.keywords = keywords;
        command.responses = responses;
        command.anims1 = anims1;
        command.anims2 = anims2;
        command.output = output;
        command.drinkName = drinkName;
      //  command.drinkId = drinkId;

        foreach (string keyword in keywords)
        {
            keywordsTxtField.text += "" + keyword + ", ";
        }

        outputTxtField.text = output;
        drinkNameField.text = drinkName;
       // DrinkIDField.text = drinkId;
        addBtn.SetActive(false);
        commandUI.SetActive(true);

        if (!string.IsNullOrEmpty(output) && !string.IsNullOrEmpty(drinkName))
            this.isDrink = true;

        isActive = true;

        settingsScrn.conversationCommandUiItems.Add(this);

        for(int i = 0; i < responses.Length; i++)
        {
            newResponseElement = Instantiate(settingsScrn.GenericResponseCommandUiItemPrefab, transform);
            GenericResponseUiItem genericResponseUiItem = newResponseElement.GetComponent<GenericResponseUiItem>();
            genericResponseUiItem.SetAnimationsDropDowns(settingsScrn.animationsNames);
            genericResponseUiItem.SetResponse(responses[i], GetAnimIndex(anims1[i]), GetAnimIndex(anims2[i]));
            if (i == 0)
            {
                newResponseElement.GetComponent<GenericResponseUiItem>().removeBtn.gameObject.SetActive(false);
            }
        }
        settingsScrn.RefreshLayoutGroup();
    }




    public void AddCommandItem()
    {
        addBtn.SetActive(false);
        commandUI.SetActive(true);
        firstResponse.SetActive(true);
        //settingsScrn = GetComponentInParent<SettingsScreenHandler>();
        firstResponse.GetComponent<GenericResponseUiItem>().SetAnimationsDropDowns(settingsScrn.animationsNames);
        //Debug.Log("settingsScrn.animationsNames[0] = " + settingsScrn.animationsNames[0]);
        settingsScrn.conversationCommandUiItems.Add(this);
        settingsScrn.CreateConversationCommandItem();
        settingsScrn.RefreshLayoutGroup();
    }

    public void RemoveCommandItem()
    {
        settingsScrn.conversationCommandUiItems.Remove(this);
        Destroy(gameObject);
        settingsScrn.RefreshLayoutGroup();
        settingsScrn.conversationSaveBtn.interactable = true;
        settingsScrn.isNeedToSaveConversationList = true;
    }


    public void AddGenericResponse()
    {
        newResponseElement = Instantiate(settingsScrn.GenericResponseCommandUiItemPrefab, transform);
        // set animations dropdowns:
        newResponseElement.GetComponent<GenericResponseUiItem>().SetAnimationsDropDowns(settingsScrn.animationsNames);

        settingsScrn.RefreshLayoutGroup();
    }



    public void gatherResponses()
    {
        responsesItems = GetComponentsInChildren<GenericResponseUiItem>();
    }


    public void StartTyping()
    {
        if (settingsScrn == null)
            return;

        settingsScrn.conversationSaveBtn.interactable = true;
        settingsScrn.isNeedToSaveConversationList = true;
    }




    public void SetCommandItem(string[] keywords = null, string[] responses = null, string[] anims1 = null, string[] anims2 = null, string drinkName = "", string output = "", int isDrink = 0)
    {
        isActive = true;

        command.keywords = keywords;
        command.responses = responses;
        command.anims1 = anims1;
        command.anims2 = anims2;
        command.output = output;
        command.drinkName = drinkName;
        command.isDrink = isDrink;
      //  command.drinkId = drinkId;
        
    }








    int GetAnimIndex(string animName)
    {
        return settingsScrn.animationsNames.IndexOf(animName);
    }
}
