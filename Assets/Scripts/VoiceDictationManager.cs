﻿using Microsoft.CognitiveServices.Speech;
using Microsoft.CognitiveServices.Speech.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class VoiceDictationManager : MonoBehaviour
{
    #region public variables:
    public int timeoutIndex = 0;
    public bool isInitialized;
    public bool isStarting;
    public bool isStopping;
    public bool isStoppingImmediately;
    public bool usePhraseList;
    public Text speechSystemStatusTxt;

    [SerializeField] bool Touch; 
    public int TimeOutCounter = 15000;
   // public GameObject DrinkButtonPrefab;
   // public TouchScreenUIManager touchScreenScript;

    #endregion


    #region private variables:
    [SerializeField] bool _isRecording;
    private object threadLocker = new object();
    SpeechConfig config;
    AudioConfig audioConfig;
    SpeechRecognizer recognizer;
    PhraseListGrammar phraseList;
    CancellationTokenSource cancellation = new CancellationTokenSource();
    string recognized;
    SpeechRecognitionResult result;
    bool isResetEverything;
    [SerializeField] List<string> phrases = new List<string>();
    #endregion




    public bool IsRecording
    {
        get
        {
            return _isRecording;
        }
    }



    private void Start()
    {
        InitSpeechConfig();
    }

    private void Update()
    {
        if (recognizer != null && isInitialized)
        {
            if (Manager.instance.isDebugModeOn)
                speechSystemStatusTxt.text = "DictationRecognizer = " + _isRecording;
        }


    }

    void InitSpeechConfig()
    {
        try
        {
            config = SpeechConfig.FromSubscription("dc97f6a7de9b47cc822625aa29b453ca", "westeurope");
            config.SetServiceProperty("punctuation", "explicit", ServicePropertyChannel.UriQueryParameter);
            config.SetProperty(PropertyId.SpeechServiceConnection_InitialSilenceTimeoutMs, TimeOutCounter.ToString());
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            DebugLogTxt.instance.Log("Speech to Text error: " + e.ToString(), true);
            Manager.instance.errorLogger.AddLog("Speech to Text error: " + e);
        }
    }

    public bool InitSpeechRecognizer()
    {
        if (isInitialized)
        {
            Debug.Log("Speech Recognizer was already initialized...");
            return true;
        }
        isInitialized = false;
        try
        {
            recognizer = new SpeechRecognizer(config);
            phrases.Clear();
            if (usePhraseList)
            {
                //Get all the keywords from all commands:
                for (int i = 0; i < Manager.instance.conversationSettings.conversationCommands.Length; i++)
                {
                    phrases.AddRange(Manager.instance.conversationSettings.conversationCommands[i].keywords);
                }

                phraseList = PhraseListGrammar.FromRecognizer(recognizer);
                for (int i = 0; i < phrases.Count; i++)
                {
                    phraseList.AddPhrase(phrases[i]);
                }
            }
            isInitialized = true;
            Debug.Log("Speech Recognizer initialize success!");
            return true;
        }
        catch (Exception e)
        {
           // Manager.instance.errorLogger.AddLog("Speech Recognizer couldn't be initialized.");
            //Debug.LogError(e);
            isInitialized = false;
            Debug.Log("Speech Recognizer initialize failed!");
            return false;
        }
        //finally
        //{
        //    return;
        //}
    }



    public async void StartRecording(string fromFunction = "")
    {
        if (_isRecording || isStarting) return;

        isResetEverything = false;
        isStarting = true;
        //isVoiceInput = false;

        if (Manager.instance.voiceActivationManager.IsRecording)
        {
            Debug.LogError("trying to start dictation while VoiceActivationManager.IsRecording = true");
            DebugLogTxt.instance.Log("VoiceActivationManager.IsRecording = true while trying to start dictation", true);
            Manager.instance.errorLogger.AddLog("VoiceActivationManager.IsRecording = true while trying to start dictation - " + fromFunction);
            Manager.instance.checkInternet.Check();
            Manager.instance.voiceActivationManager.StopRecording();
            float startTime = Time.time;
            while (Manager.instance.voiceActivationManager.IsRecording)
            {
                await Task.Delay(TimeSpan.FromSeconds(0.1f));
                if ((Time.time - startTime) > 7)
                {
                    DebugLogTxt.instance.Log("Dictation Start Error - VoiceActivationManager IsRecording", true);
                    isResetEverything = true;
                    Manager.instance.errorLogger.AddLog("Dictation Start Error - VoiceActivationManager IsRecording after 7 sec - " + fromFunction);
                    Manager.instance.ResetEverything(true);
                    Manager.instance.checkInternet.Check();
                    return;
                }
            }
        }

        if (!isInitialized)
        {
            InitSpeechRecognizer();
            while (!isInitialized)
                await Task.Delay(TimeSpan.FromSeconds(0.05f));
        }
        Manager.instance.uiManager.micIcon.SetActive(true);
        isStarting = false;

        await StartRecordingTask(fromFunction);

        CheckResults();
        Manager.instance.uiManager.micIcon.SetActive(false);
        if (Manager.instance.settingsScrnOn)
        {
            Manager.instance.settingsScrnHandler.StopRecording();
        }
    }


    async Task StartRecordingTask(string fromFunction)
    {
        lock (threadLocker)
        {
            _isRecording = true;
        }

        result = await recognizer.RecognizeOnceAsync().ConfigureAwait(false);

        lock (threadLocker)
        {
            _isRecording = false;
        }
    }


    void CheckResults()
    {
        if (result.Reason == ResultReason.RecognizedSpeech)
        {
            recognized = result.Text;
            //Debug.Log(recognized);
            processVoiceInput(recognized);
        }
        else if (result.Reason == ResultReason.NoMatch)
        {
            recognized = "NOMATCH: Speech could not be recognized.";
            //Debug.Log(recognized);
            if (!isStopping)
                OnDictationTimeout();
        }
        else if (result.Reason == ResultReason.Canceled)
        {
            var cancellation = CancellationDetails.FromResult(result);
            recognized = $"CANCELED: Reason={cancellation.Reason} ErrorDetails={cancellation.ErrorDetails}";
            if (recognized.Contains("Connection failed"))
            {
                Manager.instance.checkInternet.Check();
            }
            Debug.Log(recognized);
            DebugLogTxt.instance.Log("Voice dictation Error - " + recognized);
            Manager.instance.errorLogger.AddLog("Voice dictation Error: " + recognized);

            if (!isStopping && !Manager.instance.isResetting)
            {
                try
                {
                    OnDictationTimeout();
                }
                catch (Exception e)
                {
                    Debug.Log("Dictation manager - ResultReason.Canceled - error: " + e);
                    DebugLogTxt.instance.Log("Dictation manager - ResultReason.Canceled - error: " + e);
                    Manager.instance.errorLogger.AddLog("Dictation manager - ResultReason.Canceled - error: " + e);
                }
            }

        }

        isStopping = false;
    }



    public async void StopRecording(bool dispose = false, bool fromBtn = false, string fromFunction = "")
    {
        //Debug.Log("**** StopRecording - " + fromFunction);
        if (!_isRecording || isStopping || isStoppingImmediately)
        {
            if (fromBtn && !_isRecording)
            {
                Manager.instance.uiManager.StopRecordingUI();
            }
            return;
        }
        isStopping = true;
        await recognizer.StopContinuousRecognitionAsync();
        Manager.instance.faceRecognitionManager.StopFaceRecognition();

        float startTime = Time.time;
        while (_isRecording)
        {
            await Task.Delay(TimeSpan.FromSeconds(0.1f));
            if ((Time.time - startTime) > 20)
            {
                isStopping = false;
                DebugLogTxt.instance.Log("Dictation Stop Error", true);
                isResetEverything = true;
                Manager.instance.ResetEverything(true);
                Manager.instance.errorLogger.AddLog("Dictation Stop Error after 7 - " + fromFunction);
                Manager.instance.checkInternet.Check();
                return;
            }
        }
        if ((Time.time - startTime) > 2)
        {
            float timeTook = Time.time - startTime;
            DebugLogTxt.instance.Log("Azure Stop Recording took " + timeTook + " seconds.", true);
            if ((Time.time - startTime) > 5)
            {
                Manager.instance.errorLogger.AddLog("Azure StopRecording response took " + timeTook + " seconds.");
            }
        }
        if (fromBtn)
        {
            Manager.instance.uiManager.StopRecordingUI();
            Manager.instance.faceRecognitionManager.ResetFaceRecognition(fromBtn);
        }

        Manager.instance.uiManager.micIcon.SetActive(false);
        timeoutIndex = 0;

        if (dispose)
            DisposeRecording();

    }


    public async void DisposeRecording()
    {
        if (recognizer == null || !isInitialized) return;
        recognizer.Dispose();
        await Task.Delay(TimeSpan.FromSeconds(0.25f));
        recognizer = null;
        isInitialized = false;
    }



    public async void StopImmediately(bool fromBtn, bool restart)
    {
        Debug.Log("StopImmediately - restart = " + restart);
        isStoppingImmediately = true;
        if (_isRecording)
            await recognizer.StopContinuousRecognitionAsync();

        while (_isRecording)
        {
            await Task.Delay(TimeSpan.FromSeconds(0.05f));
        }
        DisposeRecording();
        Manager.instance.uiManager.micIcon.SetActive(false);
        if (fromBtn)
            Manager.instance.uiManager.StopRecordingUI();
        else if (restart)
        {
            while (isInitialized)
            {
                await Task.Delay(TimeSpan.FromSeconds(0.05f));
            }
            StartRecording();
        }
        //else
        //{
        isStoppingImmediately = false;
        //}
    }




    void processVoiceInput(string voiceInput)
    {
        if (Manager.instance.isTrueRecording)
        {
            if (isResetEverything || Manager.instance.commandProcessor.activatingMenuCommand)
                return;

            timeoutIndex = 0;
            Manager.instance.firestoreManager.SendConversationData(voiceInput, false);
            Manager.instance.commandProcessor.ProcessCommand(voiceInput);
        }
        else if (Manager.instance.settingsScrnOn)
        {
            Manager.instance.settingsScrnHandler.settingsInputTxtUI.text = voiceInput;
        }
    }



    void OnDictationTimeout()
    {
        if (Manager.instance.isTrueRecording)
        {
            Manager.instance.faceRecognitionManager.StopFaceRecognition();
            Manager.instance.uiManager.micIcon.SetActive(false);
            if (isResetEverything)
                return;

            if (timeoutIndex == 0)
            {
                timeoutIndex++;
                if (!Touch)
                {
                    Manager.instance.commandProcessor.OnTimeoutFirstTime();
                }
                else
                {
                    Manager.instance.commandProcessor.OnTimeoutFirstTime2();
                }
            }
            else
            {
                Manager.instance.commandProcessor.OnTimeoutSecondTime();
                timeoutIndex = 0;
            }
        }
        else if (Manager.instance.settingsScrnOn)
        {
            Manager.instance.settingsScrnHandler.StopRecording();
        }
    }

    public void CancelTimeout() //ofek changes
    {
        Touch = true;
        timeoutIndex = 0;
        TimeOutCounter = 15000;
        StartCoroutine(TimeoutRepeat());
    }

    IEnumerator TimeoutRepeat() //ofek changes
    {
        yield return new WaitForSeconds(10);
        Touch = false;
    }
   
}