﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.NetworkInformation;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UserLoginManager : MonoBehaviour
{


    /*
     * alon@appearia.com
 pw: Aa123456
    AIzaSyAiPDljg4ZAmIObZVYsafFYl9YYYAi2zFA
    "email":"alon@appearia.com",
    "password": "12345678",
    "returnSecureToken": true

    {
    "email":"alon@appearia.com",
    "password":"Aa123456",
    "returnSecureToken":true}
     * */
    public Manager nanager;
    string API_KEY = "AIzaSyAiPDljg4ZAmIObZVYsafFYl9YYYAi2zFA";
    public GameObject settingsPasswordScreen;
    public InputField emailIF;
    public InputField passwordIF;
    public Button signInBtn;
    public Text msgTxt;
    public Toggle passwordTypeToggle;
    //string _loginDataJson;
    //string _macAddress;
    string _dUID;
    string _idToken;
    static string uidFilePath;
    string ceciliaUidAddress;
    bool isTyping;
    WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
    EventSystem system;


    void Start()
    {
        //Debug.Log(" - - - " + SystemInfo.deviceUniqueIdentifier);
        //Debug.Log(" - - - " + SystemInfo.deviceModel);
        //Debug.Log(" - - - " + SystemInfo.deviceName);
        //Debug.Log(" - - - " + SystemInfo.graphicsDeviceID);
        //Debug.Log(" - - - " + SystemInfo.graphicsDeviceVendorID);
        //Debug.Log(" - - - " + SystemInfo.unsupportedIdentifier);
        uidFilePath = Application.streamingAssetsPath + "/RobobarData/duid.txt";
        nanager.ceciliaID = "not signed in";
        //_macAddress = GetMacAddress();
        _dUID = SystemInfo.deviceUniqueIdentifier;
        Debug.Log("DUID: " + _dUID);
        SaveUIDToFile("Device unique identifier: " + _dUID);
        ceciliaUidAddress = "https://europe-west3-cecilia-304213.cloudfunctions.net/devices/mac/" + _dUID;
#if UNITY_EDITOR
        LoadSavedProperties();
#endif
        LoadSavedEmail();
        settingsPasswordScreen.SetActive(true);

        system = EventSystem.current;

    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.L))
    //    {
    //        Debug.Log("Trying to send login...");
    //        StartCoroutine(GetToken(
    //            "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAiPDljg4ZAmIObZVYsafFYl9YYYAi2zFA",
    //            _loginDataJson));
    //    }
    //}

    public void OnSelectInputField()
    {
        if (isTyping)
            return;
        isTyping = true;
        StartCoroutine(CheckTab());
    }

    IEnumerator CheckTab()
    {
        while (isTyping)
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
                if (next != null)
                {
                    InputField inputfield = next.GetComponent<InputField>();
                    if (inputfield != null) 
                        inputfield.OnPointerClick(new PointerEventData(system));  //if it's an input field, also set the text caret
                    else
                    {
                        Button btn = next.GetComponent<Button>();
                        //btn.OnPointerClick(new PointerEventData(system));
                        if(btn != null) btn.Select();
                    }
                    
                    system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
                }
                else Debug.Log("next nagivation element not found");
            }
            yield return null;
        }
    }

    void LoadSavedProperties()
    {
        string savedEmail = PlayerPrefs.GetString("login_email");
        if (!string.IsNullOrEmpty(savedEmail))
            emailIF.text = savedEmail;
        string savedPassword = PlayerPrefs.GetString("login_password");
        if (!string.IsNullOrEmpty(savedPassword))
            passwordIF.text = savedPassword;
    }

    void LoadSavedEmail()
    {
        string savedEmail = PlayerPrefs.GetString("login_email");
        if (!string.IsNullOrEmpty(savedEmail))
            emailIF.text = savedEmail;
    }

    public void SignIn()
    {
        signInBtn.interactable = false;
        emailIF.interactable = false;
        passwordIF.interactable = false;
        DeactivatePasswordMsg();

        string email = emailIF.text;
        PlayerPrefs.SetString("login_email", email);
        string password = passwordIF.text;
        PlayerPrefs.SetString("login_password", password);

        LoginData loginData = new LoginData(email, password, true);
        string _loginDataJson = JsonUtility.ToJson(loginData);
        Debug.Log(_loginDataJson);

        Debug.Log("Trying to send login...");
        StartCoroutine(GetToken(
            "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAiPDljg4ZAmIObZVYsafFYl9YYYAi2zFA",
            _loginDataJson));
    }

    IEnumerator GetToken(string url, string bodyJsonString)
    {
        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        yield return request.SendWebRequest();
        Debug.Log("Status Code: " + request.responseCode);

        if (request.isNetworkError || request.isHttpError)
        {
            signInBtn.interactable = true;
            emailIF.interactable = true;
            passwordIF.interactable = true;
            msgTxt.text = request.error;
            Debug.Log(request.error);
        }
        else
        {
            Debug.Log("Form upload complete!");
            ServerJsonResponse serverJsonResponse = JsonUtility.FromJson<ServerJsonResponse>(request.downloadHandler.text);
            _idToken = serverJsonResponse.idToken;
            Debug.Log(_idToken);
            StartCoroutine(GetCeciliaID(ceciliaUidAddress));
        }
    }


    IEnumerator GetCeciliaID(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + _idToken);

            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                signInBtn.interactable = true;
                emailIF.interactable = true;
                passwordIF.interactable = true;
                msgTxt.text = "Network Error: " + webRequest.error;
                Debug.Log("Network Error: " + webRequest.error);
            }
            else
            {
                signInBtn.interactable = true;
                emailIF.interactable = true;
                passwordIF.interactable = true;
                string receivedText = webRequest.downloadHandler.text;
                Debug.Log("Received text: " + receivedText);
                if (!string.IsNullOrEmpty(receivedText))
                {
                    Debug.Log("status code: " + webRequest.responseCode);
                    if(webRequest.responseCode == 200)
                    {
                        msgTxt.text = "Success";
                        nanager.ceciliaID = webRequest.downloadHandler.text;
                        CloseLoginScrn();
                    }
                    else
                    {
                        signInBtn.interactable = true;
                        emailIF.interactable = true;
                        passwordIF.interactable = true;
                        msgTxt.text = "Log-in failed - Status code: " + webRequest.responseCode;
                        Debug.Log("Log-in failed - Status code: " + webRequest.responseCode);
                    }
                }
                else
                {
                    signInBtn.interactable = true;
                    emailIF.interactable = true;
                    passwordIF.interactable = true;
                    msgTxt.text = "Empty response";
                    Debug.Log("Empty response");
                }
            }
        }
    }

   

    public static string GetMacAddress()
    {
        string physicalAddress = "";
        NetworkInterface[] nice = NetworkInterface.GetAllNetworkInterfaces();
        foreach (NetworkInterface adaper in nice)
        {
            Debug.Log(adaper.Description);
            if (adaper.Description == "en0")
            {
                physicalAddress = adaper.GetPhysicalAddress().ToString();
                break;
            }
            else
            {
                physicalAddress = adaper.GetPhysicalAddress().ToString();
                if (physicalAddress != "")
                {
                    break;
                };
            }
        }
        return physicalAddress;
    }



    public void ChangePasswordType(bool isLetters)
    {
        if (isLetters)
        {
            passwordIF.contentType = InputField.ContentType.Standard;
        }
        else
        {
            passwordIF.contentType = InputField.ContentType.Password;
        }
        passwordIF.ForceLabelUpdate();
    }

    void DeactivatePasswordMsg()
    {
        msgTxt.text = "";
        //msgTxt.gameObject.SetActive(false);
    }

    public void CloseLoginScrn()
    {
        isTyping = false;
        StopAllCoroutines();
        settingsPasswordScreen.SetActive(false);
        DeactivatePasswordMsg();
        ChangePasswordType(false);
        passwordTypeToggle.isOn = false;
        nanager.loginScrnOn = false;
    }


    void SaveUIDToFile(string dUIDTxt)
    {
        try
        {
            if (!File.Exists(uidFilePath))
            {
                StreamWriter sw = File.CreateText(uidFilePath);
                sw.Close();
            }

            if (File.Exists(uidFilePath))
            {
                File.WriteAllText(uidFilePath, dUIDTxt);
                Debug.Log("DUID was saved to text file. " + dUIDTxt);
            }
            else
            {
                Debug.LogError("File cant be created");
            }
        }
        catch(Exception e)
        {
            Debug.LogError(e);
        }
        
    }

    //IEnumerator GetTokenOld()
    //{

    //    WWWForm form = new WWWForm();
    //    form.AddField("body", loginDataJson);

    //    using (UnityWebRequest www = UnityWebRequest.Post(
    //        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAiPDljg4ZAmIObZVYsafFYl9YYYAi2zFA", form))
    //    {
    //        yield return www.SendWebRequest();

    //        if (www.isNetworkError || www.isHttpError)
    //        {
    //            Debug.Log(www.error);
    //        }
    //        else
    //        {
    //            Debug.Log("Form upload complete!");
    //            StringBuilder sb = new StringBuilder();
    //            foreach (System.Collections.Generic.KeyValuePair<string, string> dict in www.GetResponseHeaders())
    //            {
    //                sb.Append(dict.Key).Append(": \t[").Append(dict.Value).Append("]\n");
    //            }

    //            // Print Headers
    //            Debug.Log(sb.ToString());

    //            // Print Body
    //            Debug.Log(www.downloadHandler.text);
    //        }
    //    }
    //}
}

/*
 * "email":"alon@appearia.com",
    "password": "12345678",
    "returnSecureToken": true
 * */
public class LoginData
{
    public LoginData(string _email, string _password, bool _returnSecureToken)
    {
        email = _email;
        password = _password;
        returnSecureToken = _returnSecureToken;
    }
    public string email;
    public string password;
    public bool returnSecureToken;
}

public class ServerJsonResponse
{
    public string idToken;
}
