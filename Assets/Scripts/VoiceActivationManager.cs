﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;
using UnityEngine.UI;
using System;

public class VoiceActivationManager : MonoBehaviour
{

    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    string[] activationWords = new string[] { "Hello", "Hi", "Hey","He", "Shalom", "Manishma", "How are you", "Ma amazav", "What's up"};
    public bool isStarting;
    bool _isRecording;

    //for debugging:
    public bool recognizerIsRunning;
    public SpeechSystemStatus speechSystemStatus;
    public bool isRecognizingFace;
    public bool isRecognizedUser;
    public bool isCupIn;
    public Text speechSystemStatusTxt;
    
    public bool IsRecording
    {
        get
        {
            if (keywordRecognizer != null)
                _isRecording = keywordRecognizer.IsRunning;
            else
                _isRecording = false;
            return _isRecording;
        }
    }



    void Start()
    {
        CreateActivationKeywords();

        keywords.Add("thanks", () =>
        {
            StartCoroutine(processThanksVoiceInput("thanks"));
        });
        keywords.Add("thank you", () =>
        {
            StartCoroutine(processThanksVoiceInput("thank you"));
        });

        InitRecognizer();
    }


   
    private void LateUpdate()
    {
        //Display different values for debugging:
#if UNITY_EDITOR
        if(keywordRecognizer != null)
            recognizerIsRunning = keywordRecognizer.IsRunning;

        speechSystemStatus = PhraseRecognitionSystem.Status;
        isRecognizingFace = Manager.instance.faceRecognitionManager.isRecognizing;
        isRecognizedUser = Manager.instance.faceRecognitionManager.isWithRecognizedUser;
        isCupIn = Manager.instance.isDebugCupIn || Manager.instance.arduinoHandler.isCupIn;
#endif
        if (Manager.instance.isDebugModeOn)
            speechSystemStatusTxt.text = "keywordRecognizer = " + PhraseRecognitionSystem.Status.ToString();
    }


    void CreateActivationKeywords()
    {
        for(int i = 0; i < activationWords.Length; i++)
        {
            string word = activationWords[i];
            keywords.Add(word , () =>
            {
                StartCoroutine(processActivationVoiceInput(word));
            });
        }
        Debug.Log(keywords.Count + " activation keywords created.");
    }


    public void AddKeywordToList(string word)
    {
        keywords.Add(word, () =>
        {
            Debug.Log(word + " recognized!");
            StartCoroutine(processActivationVoiceInput(word));
        });
    }


    public void InitRecognizer()
    {
        Debug.Log("Initiating Keywords Recognizer.");
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
    }

   

    
    public void StartRecording(bool fromBtn = false, string fromFunction = "")
    {
        if (IsRecording || isStarting)
        {
            if (fromBtn && IsRecording)
            {
                Manager.instance.uiManager.StartRecordingUI();
            }
            return;
        }
        isStarting = true;
        if (Manager.instance.isVoiceActivationEnabled)
            StartCoroutine(WaitForStartRecording(fromBtn, fromFunction));
        else
            StartCoroutine(OnSpeechActivationDisabled(fromBtn, fromFunction));
    }

    IEnumerator WaitForStartRecording(bool fromBtn = false, string fromFunction = "")
    {
        float startTime;
        if (Manager.instance.voiceDictationManager.IsRecording)
        {
            Debug.LogWarning("Trying to start voice activation while VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording);
            Manager.instance.errorLogger.AddLog("Trying to start voice activation while VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording + " - " + fromFunction);
            Manager.instance.voiceDictationManager.StopRecording(false, false, "VoiceActivation - WaitForStartRecording()");
            Manager.instance.checkInternet.Check();
            startTime = Time.time;
            while (Manager.instance.voiceDictationManager.IsRecording)
            {
                yield return null;
                if ((Time.time - startTime) > 7)
                {
                    DebugLogTxt.instance.Log("Voice activation Start Error - VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording, true);
                    Debug.LogError("Voice activation Start Error - VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording + " - " + fromFunction);
                    Manager.instance.errorLogger.AddLog("Voice activation Start Error - VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording + " after 7 sec - " + fromFunction);
                    Manager.instance.ResetEverything(true);
                    Manager.instance.checkInternet.Check();
                    yield break;
                }
            }
        }

        if (PhraseRecognitionSystem.Status == SpeechSystemStatus.Failed)
        {
            DebugLogTxt.instance.Log("Start activation recording failed. " + PhraseRecognitionSystem.Status.ToString(), true);
            Manager.instance.errorLogger.AddLog("Start activation recording failed. " + PhraseRecognitionSystem.Status.ToString() + " - " + fromFunction);
            PhraseRecognitionSystem.Restart();
            yield return new WaitForSeconds(0.25f);
        }
       
        keywordRecognizer.Start();
        startTime = Time.time;
        while (!IsRecording)
        {
            yield return null;
            if (PhraseRecognitionSystem.Status == SpeechSystemStatus.Failed)
            {
                Debug.LogError("PhraseRecognitionSystem.Status == SpeechSystemStatus.Failed");
                DebugLogTxt.instance.Log("Start activation recording failed after restart. " + PhraseRecognitionSystem.Status.ToString(), true);
                Manager.instance.errorLogger.AddLog("Start activation recording failed after restart. " + PhraseRecognitionSystem.Status.ToString() + " - " + fromFunction);
                isStarting = false;
                StartRecording(fromBtn, "VActivationManager - WaitForStartRecording - SpeechSystemStatus.Failed");
                yield break;
            }
            if ((Time.time - startTime) > 7)
            {
                DebugLogTxt.instance.Log("Voice activation Start Error", true);
                Manager.instance.ResetEverything(true);
                Debug.LogError("Voice activation Start Error");
                Manager.instance.errorLogger.AddLog("Voice activation Start Error after 7 sec - " + fromFunction);
                Manager.instance.checkInternet.Check();
                yield break;
            }
        }
        
        if (fromBtn)
        {
            Manager.instance.uiManager.StartRecordingUI();
            Manager.instance.isTrueRecording = true;
           // CommandsProcessor.instance.StartListeningToCupState();
        }

        isStarting = false;
        Manager.instance.faceRecognitionManager.ActivateFaceRecognition();
        Manager.instance.voiceDictationManager.timeoutIndex = 0;
    }


    public bool isStopping;
    public void StopRecording(bool fromBtn = false, string fromFunction = "")
    {
        if (!IsRecording || isStopping)
        {
            if(fromBtn && !IsRecording)
            {
                Manager.instance.uiManager.StopRecordingUI();
            }
            return;
        }

        isStopping = true;
        StartCoroutine(WaitForStopRecording(fromBtn, fromFunction));
    }

    IEnumerator WaitForStopRecording(bool fromBtn = false, string fromFunction = "")
    {
        keywordRecognizer.Stop();
        Manager.instance.faceRecognitionManager.StopFaceRecognition();
        float startTime = Time.time;
        while (IsRecording)
        {
            yield return null;
            if ((Time.time - startTime) > 7)
            {
                DebugLogTxt.instance.Log("Voice activation Stop Error", true);
                Manager.instance.ResetEverything(true);
                Debug.LogError("Voice activation Stop Error");
                Manager.instance.errorLogger.AddLog("Voice activation Stop Error - " + fromFunction);
                Manager.instance.checkInternet.Check();
                yield break;
            }
        }
        
        if (fromBtn)
        {
            Manager.instance.uiManager.StopRecordingUI();
            Manager.instance.faceRecognitionManager.ResetFaceRecognition(fromBtn);
        }
        isStopping = false;
        
        try
        {
            PhraseRecognitionSystem.Shutdown();
        }
        catch(Exception e)
        {
            Debug.LogError(e);
            DebugLogTxt.instance.Log("Voice activation - WaitForStopRecording - " + e.ToString(), true);
            Manager.instance.errorLogger.AddLog("Voice activation - WaitForStopRecording - " + e.ToString() + " - " + fromFunction);
        }
       
    }

    public void DisposeRecording()
    {
        if (keywordRecognizer == null) return;
        keywordRecognizer.Dispose();
        Debug.Log("keywordRecognizer.Dispose()");
    }

    public void StopImmediately()
    {
        if(keywordRecognizer.IsRunning)
            keywordRecognizer.Stop();
    }



    IEnumerator OnSpeechActivationDisabled(bool fromBtn = false, string fromFunction = "")
    {
        float startTime;
        if (Manager.instance.voiceDictationManager.IsRecording)
        {
            Debug.LogWarning("Trying to start OnSpeechActivationDisabled() while VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording);
            DebugLogTxt.instance.Log("Trying to start OnSpeechActivationDisabled() while VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording, true);
            Manager.instance.errorLogger.AddLog("Trying to start OnSpeechActivationDisabled() while VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording + " - " + fromFunction);
            Manager.instance.voiceDictationManager.StopRecording(false, false, "VoiceActivation - OnSpeechActivationDisabled()");
            Manager.instance.checkInternet.Check();
            startTime = Time.time;
            while (Manager.instance.voiceDictationManager.IsRecording)
            {
                yield return null;
                if ((Time.time - startTime) > 7)
                {
                    DebugLogTxt.instance.Log("OnSpeechActivationDisabled() Error - VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording, true);
                    Debug.LogError("OnSpeechActivationDisabled() Error - VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording + " - " + fromFunction);
                    Manager.instance.errorLogger.AddLog("OnSpeechActivationDisabled() Error - VoiceDictationManager.IsRecording = " + Manager.instance.voiceDictationManager.IsRecording + " after 7 sec - " + fromFunction);
                    Manager.instance.ResetEverything(true);
                    Manager.instance.checkInternet.Check();
                    yield break;
                }
            }
        }

        if (fromBtn)
        {
            Manager.instance.uiManager.StartRecordingUI();
            Manager.instance.isTrueRecording = true;
        }

        isStarting = false;
        Manager.instance.faceRecognitionManager.ActivateFaceRecognition();
        Manager.instance.voiceDictationManager.timeoutIndex = 0;
    }



    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        // if the keyword recognized is in our dictionary, call that Action.
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }




    IEnumerator processActivationVoiceInput(string voiceInput)
    {
        if (Manager.instance.isTrueRecording)
        {
            Debug.Log("Activation voice input: " + voiceInput);
            StopRecording();
            while (IsRecording)
                yield return null;
            Manager.instance.commandProcessor.OnHiSayed();
        }
        yield return null;
    }

    IEnumerator processThanksVoiceInput(string voiceInput)
    {
        if (Manager.instance.isTrueRecording && !Manager.instance.switchingCharacters)
        {
            StopRecording();
            Manager.instance.firestoreManager.SendConversationData(voiceInput, false);
            while (IsRecording)
                yield return null;
            Manager.instance.commandProcessor.OnThanksSayed();
        }
        yield return null;
    }
}
