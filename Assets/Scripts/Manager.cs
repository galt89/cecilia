﻿using OpenCVForUnityExample;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Runtime.InteropServices;


public class Manager : MonoBehaviour
{


    public static Manager instance;

    //Access the managers of all features and components from here:
    public SettingsScreenHandler settingsScrnHandler;
    public TextToSpeechHandler textToSpeechHandler;
    public CommandsProcessor commandProcessor;
    public UIManager uiManager;
    public JsonManager jsonManager;
    public TouchScreenUIManager touchScreenUIManager;
    public ArduinoHandler arduinoHandler;
    public VoiceActivationManager voiceActivationManager;
    public VoiceDictationManager voiceDictationManager;
    public MicInput micInput;
    public FpsMonitor fpsMonotor;
    public IDScanManager iDScannerManager;
    public FaceRecognitionManager faceRecognitionManager;
    public AnimationsManager animationsManager;
    public FirestoreManager firestoreManager;
    public CheckInternet checkInternet;
    public ErrorLogger errorLogger;
    public SessionLogManager sessionLogManager;

    //The activation and conversation settings:
    public ActivationSettings activationSettings;
    public ConversationSettings conversationSettings;

    public string ceciliaID;
    public int sessionID;
    //  public string drinkId;

    public bool isResetting;
    public bool isDebugCupIn;

    //This is called every time a cup is placed in.
    public UnityAction OnCupIn;

    bool settingScrnSet = false;

    public bool isFaceRecognitionOn;
    public bool isIDScanEnabled;
    public bool isVoiceActivationEnabled;
    public bool isIDApproved;
    public bool isTrueRecording;
    public bool isRobobarSpeaking;
    public bool settingsScrnOn;
    public bool loginScrnOn = true;
    public bool isDebugModeOn;
    public bool withUser;
    public bool isCheckingID;


    public ActivationCommand cupActivationCommand;
    public ActivationCommand voiceActivationNoCupCommand;
    public ActivationCommand faceFirstTimeCommand;
    public ActivationCommand faceFirstTimeNoCupCommand;
    public ActivationCommand faceSecondTimeCommand;
    public ActivationCommand faceSecondTimeNoCupCommand;
    public ActivationCommand firstTimeoutCommand;
    public ActivationCommand endConversationCommand;
    public ActivationCommand endConversationNoCupCommand;
    public ActivationCommand saidThanksCommand;
    public ActivationCommand notUnderstandCommand;
    public ActivationCommand putCupInCommand;
    public ActivationCommand takeCupOutCommand;
    //public ActivationCommand cupInDetectedCommand;
    public ActivationCommand secondTimeoutCommand;
    public ActivationCommand secondTimeoutNoCupCommand;


    //Character switching:
    public bool switchingCharacters;
    float lastSwitchTime;
    float randomSwitchTime;
    float maxSwitchTime = 30f;
    float minSwitchTime = 15f;






    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(instance.gameObject);
            instance = this;
        }

        activationSettings = new ActivationSettings();
        conversationSettings = new ConversationSettings();

        isDebugModeOn = uiManager.DebugUI.activeSelf;
    }


    private void Start()
    {
        //randomSwitchTime = UnityEngine.Random.Range(minSwitchTime, maxSwitchTime); //define initial character switching time

        checkInternet.Check();
        // drinkId = commandProcessor.currentConversationCommand.drinkId;
        sessionID = PlayerPrefs.GetInt("session_id");
    }



    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.D) && !settingsScrnOn && !loginScrnOn)
        {
            Debug.Log("debug mode toggle");
            isDebugModeOn = !isDebugModeOn;
            uiManager.DebugUI.SetActive(isDebugModeOn);
            fpsMonotor.enabled = isDebugModeOn;
            iDScannerManager.debugRawImage.gameObject.SetActive(isDebugModeOn);
        }

        if (Input.GetKeyDown(KeyCode.C) && !settingsScrnOn && !loginScrnOn)
        {
            if (!isDebugCupIn)
            {
                isDebugCupIn = true;
                OnCupIn.Invoke();
                Debug.Log("Cup state is in");
            }
            else
            {
                isDebugCupIn = false;
                Debug.Log("Cup state is out");
            }
            if (isDebugModeOn)
            {
                arduinoHandler.readInputTxt.text = "cup state = " + isDebugCupIn.ToString();
                arduinoHandler.debugArduInput.text = "cup state = " + isDebugCupIn.ToString();
            }
        }

        if (Input.GetKeyDown(KeyCode.I) && !settingsScrnOn && !loginScrnOn)
        {
            checkInternet.Check();
        }
    }





    public void SetSettingsScreen()
    {
        if (settingScrnSet)
        {
            return;
        }
        uiManager.settingsScrn.SetActive(true);
        settingsScrnHandler.CreateCommandItems();
        settingScrnSet = true;
    }






    public bool CanSwitchCharacters()
    {
        if (Time.time - lastSwitchTime > randomSwitchTime && isTrueRecording)
        {
            lastSwitchTime = Time.time;
            randomSwitchTime = UnityEngine.Random.Range(minSwitchTime, maxSwitchTime);
            return true;
        }

        return false;
    }



    public void ResetEverything(bool endSession = false)
    {
        Debug.Log("ResetEverything");
        if (isResetting)
            return;
        isResetting = endSession;
        if (endSession)
            uiManager.recordingBtn.interactable = false;
        voiceDictationManager.StopImmediately(endSession, false);
        voiceActivationManager.StopImmediately();
        faceRecognitionManager.StopFaceRecognition();
        textToSpeechHandler.StopOnTheMiddle();

        commandProcessor.ResetAll();
        iDScannerManager.ResetEverything();
        sessionLogManager.SaveCurrentSession();
        firestoreManager.SendLogData("stop");

        animationsManager.PlayWaitAnims();
        voiceDictationManager.isStarting = false;
        voiceDictationManager.isStopping = false;
        voiceActivationManager.isStarting = false;
        voiceActivationManager.isStopping = false;

        if (!endSession)
            uiManager.RestartCountDown();
    }





    public void RestartScene()
    {
        if (settingsScrnOn) return;
        //StopSTT();
        Debug.Log("!!!!      -    RestartScene()");

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }



    public void QuitApp()
    {
        Debug.Log("Quitting...");
        if (isTrueRecording || withUser || isRobobarSpeaking)
            ResetEverything(true);
        DebugLogTxt.instance.Log("before Wait and quit");
        StartCoroutine(WaitAndQuit());
    }

    IEnumerator WaitAndQuit()
    {
        DebugLogTxt.instance.Log("Wait and quit");
        while (voiceDictationManager.IsRecording || voiceActivationManager.IsRecording)
        {
            yield return null;
            Debug.Log("Trying to quit but VoiceDictationManager.instance.IsRecording || VoiceActivationManager.instance.IsRecording");
        }
        DebugLogTxt.instance.Log("Wait and quit 1");
        /*    while (sessionLogManager.isSavingLog)
            {
                yield return null;
                Debug.Log("Trying to quit but SessionLogManager.instance.isSavingLog");
            }
          */
        DebugLogTxt.instance.Log("Wait and quit 2");
        while (firestoreManager.isSendingData)
        {
            yield return null;
            Debug.Log("Trying to quit but FirestoreManager.instance.isSendingData");
        }

        yield return new WaitForSeconds(0.2f);
        Application.Quit();
    }


    private void OnApplicationQuit()
    {
        if (isTrueRecording)
            firestoreManager.SendLogData("stop");
    }



}
