﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsManager : MonoBehaviour {

    
    public Animator bartenderAnimator;
    public Animator bartenderMaleAnimator;
    public Animator bartenderFemaleAnimator;

    WaitForEndOfFrame waitEndFrame = new WaitForEndOfFrame();
    WaitForSeconds waitHalfSec = new WaitForSeconds(0.5f);

    public string[] waitingStates;
    public string[] commandStates;

    int randomTimeToWait;
    int waitIndex;
    int randomStateIndex;
    //public bool startWithGirl;

    bool isPlayingWaitAnim = false;
    public enum Character { FEMALE, MALE };
    public Character currentCharacter = Character.FEMALE;






    private void Start()
    {
        //Activate random character:
        //StartWithRandomCharacter();

        //Activate female character:
        currentCharacter = Character.FEMALE;
        bartenderAnimator = bartenderFemaleAnimator;
        Manager.instance.textToSpeechHandler.SwitchCharacters(true);
        PlayWaitAnims();
    }

    void StartWithRandomCharacter()
    {
        int randomStartCharacter = Random.Range(0, 2);
        Debug.Log("Random Start Character = " + randomStartCharacter);
        if (randomStartCharacter == 0) // if female:
        {
            currentCharacter = Character.FEMALE;
            bartenderAnimator = bartenderFemaleAnimator;
            Manager.instance.textToSpeechHandler.SwitchCharacters(true);
            bartenderMaleAnimator.Play("walk out", 0, 1f);
        }
        else if (randomStartCharacter == 1) // if male:
        {
            currentCharacter = Character.MALE;
            bartenderAnimator = bartenderMaleAnimator;
            Manager.instance.textToSpeechHandler.SwitchCharacters(false);
            bartenderFemaleAnimator.Play("walk out", 0, 1f);
        }
    }



    public void SwitchCharacters(bool isFemale)
    {
        Manager.instance.switchingCharacters = true;
        Manager.instance.faceRecognitionManager.swichingCharacters = true;
        if (isFemale)
        {
            currentCharacter = Character.FEMALE;
            bartenderAnimator = bartenderFemaleAnimator;
            bartenderMaleAnimator.CrossFadeInFixedTime("walk out", 0.4f);        }
        else
        {
            currentCharacter = Character.MALE;
            bartenderAnimator = bartenderMaleAnimator;
            bartenderFemaleAnimator.CrossFadeInFixedTime("walk out", 0.4f);
        }
        StartCoroutine(PlayWalkInAnimation());
        Manager.instance.textToSpeechHandler.SwitchCharacters(isFemale);
    }

    IEnumerator PlayWalkInAnimation()
    {
        yield return new WaitForSeconds(2.5f);
        bartenderAnimator.Play("walk in");
        yield return waitEndFrame;
        float animLength = bartenderAnimator.GetCurrentAnimatorStateInfo(0).length;
        yield return new WaitForSeconds(animLength - 2.5f);
        Manager.instance.faceRecognitionManager.swichingCharacters = false;
        yield return new WaitForSeconds(2f);
        Manager.instance.switchingCharacters = false;
        StartCoroutine(PlayWaitAnimsCo());
    }
   


    public void PlayWaitAnims(int delayTime = 0)
    {
        if (isPlayingWaitAnim) return;
        isPlayingWaitAnim = true;
        StartCoroutine(PlayWaitAnimsCo(delayTime));
    }


    public IEnumerator PlayWaitAnimsCo(int delayTime = 0)
    {
        if (Manager.instance.withUser)
        {
            isPlayingWaitAnim = false;
            yield break;
        }

        randomTimeToWait = Random.Range((6+delayTime), (14+delayTime));
        waitIndex = 0;
        while (waitIndex <= randomTimeToWait)
        {
            yield return waitHalfSec;
            waitIndex++;
            if (Manager.instance.withUser)
            {
                isPlayingWaitAnim = false;
                yield break;
            }
        }

        if (Manager.instance.withUser)
        {
            isPlayingWaitAnim = false;
            yield break;
        }
        //if (Manager.instance.CanSwitchCharacters())
        //{
        //    //switch characters:
        //    if (Manager.instance.currentCharacter == Manager.Character.FEMALE)
        //        SwitchCharacters(false);
        //    else
        //        SwitchCharacters(true);
        //}
        //else
        //{
            randomStateIndex = Random.Range(0, waitingStates.Length);
            bartenderAnimator.CrossFadeInFixedTime(waitingStates[randomStateIndex], 0.4f);
            StartCoroutine(WaitForWaitAnimToComplete(waitingStates[randomStateIndex]));
        //}
        
    }




    IEnumerator WaitForWaitAnimToComplete(string name)
    {
        if (Manager.instance.withUser)
        {
            isPlayingWaitAnim = false;
            yield break;
        }
        yield return waitHalfSec;
        while (bartenderAnimator.GetCurrentAnimatorStateInfo(0).IsName(name) && bartenderAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f)
        {
            yield return waitEndFrame;
            if (Manager.instance.withUser)
            {
                isPlayingWaitAnim = false;
                yield break;
            }
        }
        if (Manager.instance.withUser)
        {
            isPlayingWaitAnim = false;
            yield break;
        }
        StartCoroutine(PlayWaitAnimsCo());
    }




    public void PlayAnimation(string stateName)
    {
        isPlayingWaitAnim = false;
        StopAllCoroutines();
        if(stateName == "non")
        {
            Debug.Log("Playing Animation - " + stateName + ", returning..");
            return;
        }
        //Debug.Log("Playing Animation - " + stateName);
        bartenderAnimator.CrossFadeInFixedTime(stateName, 0.4f,0);
 
    }


    public void PlayAnimation(string stateName, float loopTime)
    {
        isPlayingWaitAnim = false;
        StopAllCoroutines();

        bartenderAnimator.CrossFadeInFixedTime(stateName, 0.4f);

        StartCoroutine(WaitAndPutShaker(loopTime-2.3f));
    }


    IEnumerator WaitAndPutShaker(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        PlayAnimation("put_shaker");
    }



   



}
