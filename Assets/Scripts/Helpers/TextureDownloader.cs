﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TextureDownloader : MonoBehaviour
{

    public ModelData[] modelDatas;
    //cecilia_texture.jpg
    string ceciliaTexturePath;

    void Start()
    {
        ceciliaTexturePath = Application.streamingAssetsPath + "/RobobarData/Settings/Textures";
        DownloadTexturesFromStreamingAssets();
    }

    public void DownloadTexturesFromStreamingAssets()
    {
        for(int i = 0; i < modelDatas.Length; i++)
        {
            string texturePath = ceciliaTexturePath + "/" + modelDatas[i].textureName;
            if (!File.Exists(texturePath))
            {
                DebugLogTxt.instance.Log("Texture path not Exists - " + texturePath);
                continue;
            }
            Texture2D newTex = new Texture2D(1, 1);
            try
            {
                byte[] imgData = File.ReadAllBytes(texturePath);
                newTex.LoadImage(imgData);
                modelDatas[i].renderer.sharedMaterial.mainTexture = newTex;
            }
            catch(Exception e)
            {
                DebugLogTxt.instance.Log(e.ToString());
            }
        }
    }
}

[System.Serializable]
public class ModelData
{
    public Renderer renderer;
    public string textureName;
}
