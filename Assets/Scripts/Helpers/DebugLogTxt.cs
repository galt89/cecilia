﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugLogTxt : MonoBehaviour
{
    public static DebugLogTxt instance;
    public Text logTxt;


    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        if (logTxt == null)
        {
            logTxt = GetComponent<Text>();
        }
    }

    public void Log(string text, bool printWhenNotActive = false)
    {
        if (logTxt == null)
            logTxt = GetComponent<Text>();

        if(printWhenNotActive || logTxt.gameObject.activeInHierarchy)
        {
            if (logTxt.text.Length > 3000)
                logTxt.text = "";
            logTxt.text += " " + text + "\n";
        }
            
    }
}
