﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderButton : MonoBehaviour
{
    public UnityEvent onPress;
    

    private void OnMouseDown()
    {
        //Debug.Log("Mouse Down!");
        onPress.Invoke();
    }
}
