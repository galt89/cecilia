﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendShapesMarge : MonoBehaviour {

    public SkinnedMeshRenderer masterMesh;
    public SkinnedMeshRenderer targetMesh;

    public int blendShapeAA;
    public int blendShapeEE;
    public int blendShapeOO;

    public int teethBlendShapeAA;
    public int teethBlendShapeEE;
    public int teethBlendShapeOO;



    void Update () {
        targetMesh.SetBlendShapeWeight(teethBlendShapeAA, masterMesh.GetBlendShapeWeight(blendShapeAA));
        targetMesh.SetBlendShapeWeight(teethBlendShapeEE, masterMesh.GetBlendShapeWeight(blendShapeEE));
        targetMesh.SetBlendShapeWeight(teethBlendShapeOO, masterMesh.GetBlendShapeWeight(blendShapeOO));

        //targetMesh.SetBlendShapeWeight(14, 100f);
    }
}
