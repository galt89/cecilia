﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayConfig : MonoBehaviour
{
    void Start()
    {
        SetDisplays();
    }

    // Display.displays[0] is the primary, default display and is always ON, so start at index 1.
    // Check if additional displays are available and activate each.
    void SetDisplays()
    {
        DebugLogTxt.instance.Log("displays connected: " + Display.displays.Length, true);
        Debug.Log("displays connected: " + Display.displays.Length);
        //Screen.fullScreen = false;
        for (int i = 1; i < Display.displays.Length; i++)
        {
            if (i == 0)
            {
                Display.displays[i].Activate(1920, 1080, 60);
            }
            else
            {
                Display.displays[i].Activate(2160, 3840, 60);
            }
        }
    }
    

    public void ToggleFullScreen()
    {
        Screen.fullScreen = !Screen.fullScreen;
        DebugLogTxt.instance.Log("Screen.fullScreen = " + Screen.fullScreen, true);
    }


}
