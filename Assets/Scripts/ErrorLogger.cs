﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;

public class ErrorLogger : MonoBehaviour
{

    static string path;
    static string checkPath = "Assets/Scenes/tests/Encription/log.erl";
    static string checkDecodedPath = "Assets/Scenes/tests/Encription/decodedLog.txt";
    static string text = "--- Error Log ---\n\n";
    

    private void Awake()
    {
       
        string dateTime = DateTime.Now.ToString();
        dateTime = dateTime.Replace('/', '_');
        dateTime = dateTime.Replace(':', '_');
        path = Application.streamingAssetsPath + "/RobobarData/ErrorLogs/log_" + dateTime + ".erl";
    }

   

    public void AddLog(string log)
    {
        Manager.instance.firestoreManager.SendLogData("error", log);
        text += Time.realtimeSinceStartup + " - " + log + "\n-----\n";
        try
        {
            SaveToLog(text);
        }
        catch(Exception e)
        {
            Debug.LogError(e);
            DebugLogTxt.instance.Log("Error logger - AddLog - " + e.ToString(), true);
        }
        
    }


    void SaveToLog(string text)
    {
        if (!File.Exists(path))
        {
            StreamWriter sw = File.CreateText(path);
            sw.Close();
        }

        if (File.Exists(path))
        {
            File.WriteAllText(path, text);
        }
        else
        {
            Debug.LogError("File cant be created");
        }
    }

    void SaveAndEncript(string text)
    {
        LogData lData = new LogData();
        lData.data = Encoding.UTF8.GetBytes(text);
        string bJSon = JsonUtility.ToJson(lData);

        if (!File.Exists(path))
        {
            StreamWriter sw = File.CreateText(path);
            sw.Close();
        }

        if (File.Exists(path))
        {
            File.WriteAllText(path, bJSon);
        }
        else
        {
            Debug.LogError("File cant be created");
        }
    }


    public void ReadLogFile()
    {
        string readjSon = File.ReadAllText(checkPath);
        //Debug.Log(readjSon);
        LogData readLData = JsonUtility.FromJson<LogData>(readjSon);
        string decodedData = Encoding.UTF8.GetString(readLData.data);
        Debug.Log(decodedData);
        if (!File.Exists(checkDecodedPath))
        {
            StreamWriter sw = File.CreateText(checkDecodedPath);
            sw.Close();
        }

        //yield return new WaitForEndOfFrame();
        if (File.Exists(checkDecodedPath))
        {
            File.WriteAllText(checkDecodedPath, decodedData);
        }
    }
}




[System.Serializable]
public class LogData
{
    public byte[] data;
}
