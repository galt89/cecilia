﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TouchScreenUIManager : MonoBehaviour
{
    public Manager manager;
    
    bool isActive;
    string localImagesPath;
    List<Texture2D> localImages = new List<Texture2D>();

    public GameObject DrinksMenuParent;
    public GameObject drinkBtnPrefab;
    public Transform drinkBtnsParent;
    public GameObject waitScrnParent;
    public GameObject pleasePutCupParent;
    public GameObject pleaseTakeDrinkParent;
    public GameObject pleaseTakeCupParent;
    public GameObject drinkScreen;
    public Image drinkImage;
    public Image drinkDescription;
    public Button drinkBtn;
    public GameObject topPannel;
    public bool menushow; //OfekChanges

    public struct DrinkUIItem
    {
        public string dName;
        public ConversationCommand dCommand;
        public Texture2D dPic;
        public Texture2D dLong;
        public Texture2D dShort;
    }

    List<GameObject> drinksBtns = new List<GameObject>();

    public Image mainPageBG;
    public Image putCupBG;
    public Image pleaseWaitBG;
    public Image takeAfterDrinkBG;
    public Image takeCupBG;
    public Image drinkOpenBigPageBG;
    public Image mainPageCecilia; //ofek

    Texture2D mainPageBGtx;
    Texture2D drinkOpenBigPageBGtx;
    Texture2D CeciliaBackgroundBGtx; //ofek


    private void Awake() //png from file
    {
        localImagesPath = Application.streamingAssetsPath + "/RobobarData/Settings/DrinksMenu/";

        DirectoryInfo dir = new DirectoryInfo(localImagesPath);
        FileInfo[] infos = dir.GetFiles();
        for(int i = 0; i < infos.Length; i++)
        {
            if(infos[i].Name.Contains(".png") && !infos[i].Name.Contains(".meta"))
            {
                if (infos[i].Name.Contains("_"))
                {
                    //Debug.Log("Geting image: " + infos[i].FullName);
                    Texture2D newTex = new Texture2D(1, 1);
                    byte[] imgData = File.ReadAllBytes(infos[i].FullName);
                    newTex.LoadImage(imgData);
                    newTex.name = infos[i].Name.Remove(infos[i].Name.IndexOf('.'));
                    localImages.Add(newTex);
                }
                else if (infos[i].Name.Contains("CocktailsMainPageBackground"))
                {
                    mainPageBGtx = new Texture2D(1, 1);
                    byte[] imgData = File.ReadAllBytes(infos[i].FullName);
                    mainPageBGtx.LoadImage(imgData);
                }
                else if (infos[i].Name.Contains("CocktailOpenBigPageBackground"))
                {
                    drinkOpenBigPageBGtx = new Texture2D(1, 1);
                    byte[] imgData = File.ReadAllBytes(infos[i].FullName);
                    drinkOpenBigPageBGtx.LoadImage(imgData);
                }
                else if (infos[i].Name.Contains("CeciliaBackground"))
                {
                    CeciliaBackgroundBGtx = new Texture2D(1, 1);
                    byte[] imgData = File.ReadAllBytes(infos[i].FullName);
                    CeciliaBackgroundBGtx.LoadImage(imgData);
                }
            }
        }
    }



    private void Start()
    {
        topPannel.SetActive(false);

        //Assign backgrounds images from loaded images
        mainPageBG.sprite = GetNewSprite(mainPageBGtx);
        putCupBG.sprite = GetNewSprite(mainPageBGtx);
        pleaseWaitBG.sprite = GetNewSprite(mainPageBGtx);
        takeAfterDrinkBG.sprite = GetNewSprite(mainPageBGtx);
        takeCupBG.sprite = GetNewSprite(mainPageBGtx);
        drinkOpenBigPageBG.sprite = GetNewSprite(drinkOpenBigPageBGtx);
        mainPageCecilia.sprite = GetNewSprite(CeciliaBackgroundBGtx);
    }

    private void Update() //Ofek changes
    {
        if(pleasePutCupParent.activeSelf == false)
        {
            menushow = true;
        }
        else
        {
            menushow = false;
        }
    }

    public void ActivateDrinksMenu()
    {
        if (isActive)
            return;

        DrinksMenuParent.SetActive(true);
        isActive = true;
       
        ShowPutCupScreen();
    }

    public void DeactivateDrinksMenu()
    {
        isActive = false;
        HideDrinksUI();
    }

    public void ShowWaitScreen()
    {
        pleasePutCupParent.SetActive(false);
        pleaseTakeDrinkParent.SetActive(false);
        pleaseTakeCupParent.SetActive(false);
        waitScrnParent.SetActive(true);
        drinkScreen.SetActive(false);
    }

    public void ShowPutCupScreen()
    {
        pleaseTakeDrinkParent.SetActive(false);
        waitScrnParent.SetActive(false);
        drinkScreen.SetActive(false);
        pleaseTakeCupParent.SetActive(false);
        pleasePutCupParent.SetActive(true);
    }

    public void ShowTakeYourDrinkScreen()
    {
        drinkScreen.SetActive(false);
        pleasePutCupParent.SetActive(false);
        waitScrnParent.SetActive(false);
        pleaseTakeCupParent.SetActive(false);
        pleaseTakeDrinkParent.SetActive(true);
    }

    public void ShowTakeYourCupScreen()
    {
        drinkScreen.SetActive(false);
        pleasePutCupParent.SetActive(false);
        waitScrnParent.SetActive(false);
        pleaseTakeDrinkParent.SetActive(false);
        pleaseTakeCupParent.SetActive(true);
    }


    public void ShowDrinksMenu()
    {
        pleaseTakeDrinkParent.SetActive(false);
        pleaseTakeCupParent.SetActive(false);
        waitScrnParent.SetActive(false);
        pleasePutCupParent.SetActive(false);
    }

    public void HideDrinksUI()
    {
        pleaseTakeDrinkParent.SetActive(false);
        pleaseTakeCupParent.SetActive(false);
        waitScrnParent.SetActive(false);
        drinkScreen.SetActive(false);
        pleasePutCupParent.SetActive(false);
        DrinksMenuParent.SetActive(false);
    }


    public void OnScreenTap()
    {
        manager.commandProcessor.OnCupOrTouchDetected();
    }


    public void StartSetDrinksBtns()
    {
        if(drinksBtns.Count > 0)
        {
            foreach(GameObject btn in drinksBtns)
            {
                Destroy(btn);
            }
            drinksBtns.Clear();
        }
        if(manager.conversationSettings != null)
        {
            if(manager.conversationSettings.conversationCommands != null)
            {
                foreach(ConversationCommand command in manager.conversationSettings.conversationCommands)
                {
                    if (command.isDrink == 1)
                    {
                        DrinkUIItem drinkUIItem = new DrinkUIItem();
                        int seccessIndex = 0;
                        for (int y = 0; y < localImages.Count; y++)
                        {
                            if (localImages[y].name == (command.drinkName + "_pic"))
                            {
                                
                                drinkUIItem.dPic = localImages[y];
                                seccessIndex++;
                            }
                            if (localImages[y].name == (command.drinkName + "_short"))
                            {
                                
                                drinkUIItem.dShort = localImages[y];
                                seccessIndex++;
                            }
                            if (localImages[y].name == (command.drinkName + "_long"))
                            {
                                
                                drinkUIItem.dLong = localImages[y];
                                seccessIndex++;
                            }
                        }
                        if (seccessIndex >= 3)
                        {
                            drinkUIItem.dCommand = command;
                            drinkUIItem.dName = command.drinkName;
                            CreateDrinkBtn(drinkUIItem);
                        }
                    }
                }
            }
        }
    }



    public void CreateDrinkBtn(DrinkUIItem drinkUIItem)
    {
        GameObject newDrinkBtn = Instantiate(drinkBtnPrefab, drinkBtnsParent);
        newDrinkBtn.GetComponent<Button>().onClick.AddListener(delegate { OpenDrinkScreen(drinkUIItem); });
        drinksBtns.Add(newDrinkBtn);
        newDrinkBtn.transform.Find("drink_pic").GetComponent<Image>().sprite = GetNewSprite(drinkUIItem.dPic);
        newDrinkBtn.transform.Find("drink_short").GetComponent<Image>().sprite = GetNewSprite(drinkUIItem.dShort);
    }



    public static Sprite GetNewSprite(Texture2D SpriteTexture, float PixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.Tight)
    {
        Sprite NewSprite = Sprite.Create(SpriteTexture, new Rect(0, 0, SpriteTexture.width, SpriteTexture.height), new Vector2(0, 0), PixelsPerUnit, 0, spriteType);
        return NewSprite;
    }



    void OpenDrinkScreen(DrinkUIItem drinkUIItem)
    {
        drinkBtn.onClick.RemoveAllListeners();
        drinkBtn.onClick.AddListener(delegate { OnDrinkSellected(drinkUIItem.dCommand); });
        drinkImage.sprite = GetNewSprite(drinkUIItem.dPic);
        drinkDescription.sprite = GetNewSprite(drinkUIItem.dLong);
        drinkScreen.SetActive(true);
    }



    void OnDrinkSellected(ConversationCommand command)
    {
        drinkBtn.interactable = false;
        StartCoroutine(manager.commandProcessor.ActivateNewCommand(command));
    }


    public void EnableDrinkBtn()
    {
        drinkBtn.interactable = true;
    }


    // Top bar handler (probably should be in its own class...):
    #region "Top bar toggle"

    bool hiddenPannelCounterStarted;
    int hiddenBtnRCounter;
    int hiddenBtnLCounter;
    float hiddenCounterTimeout = 4;
    WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();

    public void HiddenBtnR()
    {
        hiddenBtnRCounter++;
        if (!hiddenPannelCounterStarted)
            StartCoroutine(CheckHiddenCounter());
    }
    
    public void HiddenBtnL()
    {
        if (hiddenPannelCounterStarted && hiddenBtnRCounter >= 2) // Ofek change >= 2 instead == 2
            hiddenBtnLCounter++;
        else
            hiddenBtnRCounter = hiddenBtnLCounter = 0;
    }

    IEnumerator CheckHiddenCounter()
    {
        hiddenPannelCounterStarted = true;
        float startTime = Time.time;
        string msg = "top panel code failed.";
        while (Time.time - startTime < hiddenCounterTimeout)
        {
            yield return waitForEndOfFrame;
            if (hiddenBtnRCounter >= 2 && hiddenBtnLCounter >= 2)
            {
                //Show the pannel!
                topPannel.SetActive(true);
                msg = "top panel code succeeded!";
                break;
            } 
        }
        Debug.Log(msg);
        yield return new WaitForSeconds(1);
        int pressIndex = 0;
        startTime = Time.time;
        while (topPannel.activeSelf == true)
        {
#if UNITY_EDITOR
            yield return null;
            if (pressIndex < 2)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    pressIndex++;
                    Debug.Log("pressIndex = " + pressIndex);
                    startTime = Time.time;
                }
                
                if (pressIndex > 0 && Time.time - startTime >= 1)
                {
                    pressIndex = 0;
                }
                if (Time.time - startTime > 10)
                {
                    pressIndex = 0;
                    topPannel.SetActive(false);
                    break;
                }
            }
            else
            {
                pressIndex = 0;
                topPannel.SetActive(false);
                break;
            }
#else
            yield return waitForEndOfFrame;
            Vector3 relativeMousePos = Display.RelativeMouseAt(Input.mousePosition);
            if (relativeMousePos.z == 0) //if on the touch display...
            {
                if (relativeMousePos.y < Screen.height * 0.8)
                {
                    topPannel.SetActive(false);
                }
            }
#endif
        }
        hiddenBtnRCounter = hiddenBtnLCounter = 0;
        hiddenPannelCounterStarted = false;
    }

    #endregion

}



