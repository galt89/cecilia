﻿using Kignis;
using OpenCVForUnityExample;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FaceRecognitionManager : MonoBehaviour
{

    KFaceForUnity kface;
   
    [System.Serializable]
    public struct Person
    {
        public int id;
        public string name;
        public FaceRecognitionResultWithTex2D faceResoult;
        public ConversationCommand lastCommand;
        public float lastTime;
    }
    public List<Person> persons = new List<Person>();
    public Person currentPerson;
    Person newPerson;
    int currentPersonId;
    public FaceRecognitionResultWithTex2D lastResult;
    DnnObjectDetectionWebCamTextureCustom faceDetectionScript;
    WaitForSeconds waitSnapshotTime = new WaitForSeconds(1.5f);
    Texture2D receavedTexture;
    public RawImage detectedFaceImage;
    Texture2D currenttexure;
    FaceRecognitionResultWithTex2D frrTex;
    bool isKFaceInitialized;
    public bool isRecognizing;
    public bool isWithRecognizedUser;
    public bool isTryingToRegisterADrink;
    public bool swichingCharacters;





    void Start()
    {
        kface = new KFaceForUnity();;
        faceDetectionScript = GetComponent<DnnObjectDetectionWebCamTextureCustom>();
        InitKFace();
    }

    void InitKFace()
    {
        try
        {
            bool ret = kface.InitModel(Application.dataPath + "/StreamingAssets/Models");
            isKFaceInitialized = ret;
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            DebugLogTxt.instance.Log("InitKFace() - " + e.ToString(), true);
            Manager.instance.errorLogger.AddLog("InitKFace() - " + e.ToString());
        }
        finally
        {
            Debug.Log("KFaceForUnity InitModel:" + isKFaceInitialized);
            DebugLogTxt.instance.Log("KFaceForUnity InitModel:" + isKFaceInitialized, true);
            if(isRecognizing)
                TryGetSnapshot();
        }
    }


    public void ActivateFaceRecognition()
    {
        Debug.Log("ActivateFaceRecognition()");
        if (!Manager.instance.isFaceRecognitionOn || !faceDetectionScript.isWebcamInitialized)
        {
            Debug.Log("ActivateFaceRecognition() - isFaceRecognitionOn = " + Manager.instance.isFaceRecognitionOn +
                ", isWebcamInitialized = " + faceDetectionScript.isWebcamInitialized + ". returning...");
            return;
        }
        Debug.Log("ActivateFaceRecognition() - voiceActivationManager.IsRecording = " + Manager.instance.voiceActivationManager.IsRecording +
                ", withUser = " + Manager.instance.withUser);
        if (Manager.instance.voiceActivationManager.IsRecording && !Manager.instance.withUser && Manager.instance.isTrueRecording)
        {
            isRecognizing = true;
            isTryingToRegisterADrink = false;
            if (isKFaceInitialized)
                TryGetSnapshot();
            else
                Invoke("InitKFace", 1);
        }
    }

    public void StopFaceRecognition()
    {
        isRecognizing = false;
    }

    public void ResetFaceRecognition(bool fromBtn = false)
    {
        //check if needed...
        if (!Manager.instance.isFaceRecognitionOn || !faceDetectionScript.isWebcamInitialized) return;

        isWithRecognizedUser = false;
        isRecognizing = false;
        isTryingToRegisterADrink = false;
        receavedTexture = null;
        detectedFaceImage.texture = null;
        if (fromBtn)
        {
            currentPerson = new Person();
            persons.Clear();
        }
        StopAllCoroutines();
        Resources.UnloadUnusedAssets();
        GC.Collect();
    }



    void TryGetSnapshot()
    {
        if ((!isRecognizing && !isTryingToRegisterADrink) || !Manager.instance.isTrueRecording) return;
        StartCoroutine(TryGetSnapshotCo());
    }

    
    IEnumerator TryGetSnapshotCo()
    {
        yield return waitSnapshotTime;
        if (swichingCharacters)
        {
            if (!isTryingToRegisterADrink)
                TryGetSnapshot();
            yield break;
        }

        if(isRecognizing || isTryingToRegisterADrink)
            faceDetectionScript.TakeSnapShot(OnSnapshotSeccess, OnSnapshotFailed);
    }

    void OnSnapshotSeccess()
    {
        if (!isRecognizing && !isTryingToRegisterADrink) return;
        if (Manager.instance.switchingCharacters)
        {
            TryGetSnapshot();
            return;
        }
        receavedTexture = faceDetectionScript.newFaceImage;
        detectedFaceImage.texture = receavedTexture;
        SearchThePerson(receavedTexture);
    }

    void OnSnapshotFailed()
    {
        if (!isRecognizing && !isTryingToRegisterADrink) return;

        if(!isTryingToRegisterADrink)
            TryGetSnapshot();
    }


   
    protected void SearchThePerson(Texture2D currentImage)
    {
        if (!isRecognizing && !isTryingToRegisterADrink) return;

        isWithRecognizedUser = false;
        
        currenttexure = currentImage;

        // Get face features, which is actually 128 floating point values storing in MobileFaceNetFeatures struct
        frrTex = kface.GetFeatures(currenttexure, KFaceForUnity.Draw.BOTH, KFaceForUnity.ImageFlip.Vert);
        
        lastResult = frrTex;

        if (frrTex.frr.netNum > 0) //If the quality of the face features is good enoght
        {
            isWithRecognizedUser = true;
            FaceLibSearchResult flsr;

            for (int i = 0; i < persons.Count; i++)
            {
                // Compare it with all the other images in the face library
                flsr = CompareWithLibFaces(frrTex.frr.facenetfs[0], persons[i].faceResoult.frr.facenetfs[0], i);

                // Face cosine similarity must be greater than 0.4,
                // the larger the value, the more similar
                if (flsr.personID != "" && flsr.sim > 0.4)
                {
                    //personFound = true;
                    currentPersonId = i;
                    currentPerson = persons[i];
                    if (isTryingToRegisterADrink)
                    {
                        RegisterADrinkToAUser(drinkToRegister, commandToRegister);
                        Debug.Log("isTryingToRegisterADrink() - person found: " + flsr.personID + ", file name: " + flsr.filename);
                        Resources.UnloadUnusedAssets();
                        GC.Collect();
                        return;
                    }
                    else
                    {
                        if (Time.time - currentPerson.lastTime < 60)
                        {
                            DebugLogTxt.instance.Log("found same person: " + flsr.personID + ", file name: " + flsr.filename);
                            TryGetSnapshot();
                            Resources.UnloadUnusedAssets();
                            GC.Collect();
                            return;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(persons[i].name) || persons[i].name == "drink")
                            {
                                Manager.instance.commandProcessor.OnNewFaceRecognized();
                            }
                            else
                            {
                                Manager.instance.commandProcessor.OnFemiliarFaceRecognized(persons[i].name, persons[i].lastCommand);
                            }
                            Debug.Log("person found: " + flsr.personID + ", file name: " + flsr.filename);
                            //lastPerson = currentPerson;
                            currentPerson.lastTime = Time.time;
                            persons[i] = currentPerson;
                            Resources.UnloadUnusedAssets();
                            GC.Collect();
                            return;
                        }
                    }
                }
            }

            
            newPerson.id = persons.Count;
            newPerson.name = "drink";
            newPerson.lastCommand = new ConversationCommand();
            newPerson.faceResoult = frrTex;
            newPerson.lastTime = Time.time;
            currentPerson = newPerson;
            AddCurrentPerson();
            if (isTryingToRegisterADrink)
            {
                RegisterADrinkToAUser(drinkToRegister, commandToRegister);
               
                Resources.UnloadUnusedAssets();
                GC.Collect();
                return;
            }
            else
            {
                Manager.instance.commandProcessor.OnNewFaceRecognized();
               
                Resources.UnloadUnusedAssets();
                GC.Collect();
                return;
            }
            
        }
        else
        {
            DebugLogTxt.instance.Log("the face can't be recognized...");
            if (!isTryingToRegisterADrink)
                TryGetSnapshot();

            Resources.UnloadUnusedAssets();
            GC.Collect();
        }

        
    }

    public void AddCurrentPerson()
    {
        currentPersonId = persons.Count;
        persons.Add(currentPerson);
    }


    FaceLibSearchResult CompareWithLibFaces(MobileFaceNetFeatures fs, MobileFaceNetFeatures fsl, int currentResult)
    {
        FaceLibSearchResult flsr;
        double maxSim = -1.0;
        double tmpSim = -1.0;
        string maxSimPersonID = "";
        string maxSimFilename = "";

        tmpSim = kface.GetSimilarity(fs, fsl);
        if (tmpSim > maxSim)
        {
            maxSim = tmpSim;

            if (currentResult < persons.Count)
            {
                maxSimPersonID = persons[currentResult].id.ToString();
                maxSimFilename = persons[currentResult].name;
            }
            else
            {
                maxSimPersonID = currentResult.ToString();
                maxSimFilename = "drink";
            }
        }

        flsr.personID = maxSimPersonID;
        flsr.filename = maxSimFilename;
        flsr.sim = maxSim;

        return flsr;
    }


    string drinkToRegister;
    ConversationCommand commandToRegister = new ConversationCommand();
    public void RegisterADrinkToAUser(string drinkName, ConversationCommand currentCommand)
    {
        if (!faceDetectionScript.isWebcamInitialized) return;
        drinkToRegister = drinkName;
        commandToRegister = currentCommand;
        if (isWithRecognizedUser)
        {
            currentPerson.name = drinkToRegister;
            currentPerson.lastCommand = commandToRegister;
            currentPerson.lastTime = Time.time;
            persons[currentPersonId] = currentPerson;
            isTryingToRegisterADrink = false;
        }
        else
        {
            isTryingToRegisterADrink = true;
            TryGetSnapshot();
        }
    }




    //void PrintResults()
    //{
    //    Debug.Log("facenetfs.Length = " + lastResult.frr.facenetfs.Length);
    //    if (lastResult.frr.facenetfs.Length > 0)
    //    {
    //        Debug.Log("facenetfs[0] = " + lastResult.frr.facenetfs[0].ToString());
    //        Debug.Log("feature.Length = " + lastResult.frr.facenetfs[0].feature.Length);
    //    }
    //    Debug.Log("rectNum = " + lastResult.frr.rectNum);
    //    Debug.Log("rects = " + lastResult.frr.rects.Length);
    //    Debug.Log("errCode = " + lastResult.frr.errCode);
    //}



   

}
