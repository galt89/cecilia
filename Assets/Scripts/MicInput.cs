﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MicInput : MonoBehaviour{

	public Slider MicrophoneSensivity;
	public static float MicLoudness;
	private string _device;
	private AudioClip _clipRecord;
	private int _sampleWindow = 128;
	[SerializeField] private bool _isInitialized = false;
	float levelMax;
	float[] waveData;
	int micPosition;
	float wavePeak;
	WaitForSeconds waitCheckMicrophoneTime = new WaitForSeconds(0.5f);
	public int micDivicesCount;
	int prevMicDivicesCount;

    private void Start()
    {
		micDivicesCount = prevMicDivicesCount = Microphone.devices.Length;
		StartCoroutine(CheckMicrophone());
	}


    public void InitMic()
	{
		_isInitialized = false;
		_device = null;
		_clipRecord = null;
		//if (!_isInitialized && _device == null) {
			if (micDivicesCount == 0)
            {
                Debug.Log("Microphone.devices.Length = 0");
                Manager.instance.settingsScrnHandler.settingsInputTxtUI.text = Manager.instance.uiManager.barmanTextUI.text = "Microphone is not connected - Restart recording";
				DebugLogTxt.instance.Log("Microphone is not connected.", true);
				Manager.instance.errorLogger.AddLog("No microphone device detected.");
				return;
            }
            waveData = new float[_sampleWindow];
            _device = Microphone.devices [0];
			_clipRecord = Microphone.Start (_device, true, 999, 44100);
			Debug.Log ("Init microphone - " + _clipRecord);
			_isInitialized = true;
		//}
	}

	IEnumerator CheckMicrophone()
    {
        while (true)
        {
			yield return waitCheckMicrophoneTime;
			micDivicesCount = Microphone.devices.Length;
			//Debug.Log("micDivicesCount: " + micDivicesCount);
			//if (!_isInitialized)
			//	yield break;
			if (micDivicesCount < prevMicDivicesCount)
			{
				//Microphone is not connected.
				Debug.Log("Microphone has disconnected");
				DebugLogTxt.instance.Log("Microphone has disconnected");
				Manager.instance.settingsScrnHandler.settingsInputTxtUI.text = Manager.instance.uiManager.barmanTextUI.text = "Microphone was disconnected";
				Manager.instance.uiManager.ShowMicrophoneIssuesTxt();
				Manager.instance.errorLogger.AddLog("Microphone device disconected.");
				if(micDivicesCount == 0)
                {
                    Manager.instance.ResetEverything(true);
                    //StopMicrophone(true);
                }
                else
                {
					if (Manager.instance.voiceDictationManager.IsRecording)
					{
						Manager.instance.voiceDictationManager.StopImmediately(false, false);
					}
					else
					{
						Manager.instance.voiceDictationManager.DisposeRecording();
					}
				}
			}
			else if(micDivicesCount > prevMicDivicesCount)
            {
				Debug.Log("Microphone reconnected");
				DebugLogTxt.instance.Log("Microphone has reconnected");
				//StopMicrophone(true);
			}
			prevMicDivicesCount = micDivicesCount;
		}
	}

	public void StopMicrophone(bool restart = false)
	{
        //StopAllCoroutines();
        if (_isInitialized)
        {
			_isInitialized = false;
			_device = null;
			_clipRecord = null;
			try
            {
				Microphone.End(_device);
			}
			catch(Exception e)
            {
				Debug.LogError(e);
				DebugLogTxt.instance.Log(e.ToString(), true);
            }
            finally
            {
                if (restart)
                {
					InitMic();
				}
            }
        }
        else
        {
			if (restart)
			{
				InitMic();
			}
		}
	}

	

	public float LevelMax()
	{
		
		if (micDivicesCount == 0 || !_isInitialized || _device == null)
		{
			return 0;
		}

		levelMax = 0;

		micPosition = Microphone.GetPosition (_device) - (_sampleWindow + 1);
		if(micPosition < 0)
			return 0;

		try
		{
			_clipRecord.GetData(waveData, micPosition);
			if(_clipRecord.loadState == AudioDataLoadState.Failed)
			{
				DebugLogTxt.instance.Log("Microphone input failure!, Reset session." + _clipRecord.loadState, true);
				Manager.instance.errorLogger.AddLog("Microphone input failure!, Reset session.");
				Manager.instance.ResetEverything(true);
				return 0;
			}
		}
		catch (Exception e)
		{
			DebugLogTxt.instance.Log("Microphone input failure!, Reset session." + e, true);
			Manager.instance.errorLogger.AddLog("Microphone input failure!, Reset session.");
			Manager.instance.ResetEverything(true);
			return 0;
        }   

		for (int i = 0; i < _sampleWindow; ++i) {
			wavePeak = waveData [i] * waveData [i];
			if (levelMax < wavePeak) {
				levelMax = wavePeak;
			}
		}
	
		if (levelMax > 0) {
			return (levelMax * 3);
		} else {
			return 0;
		}
	}


}