﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class IDScanManager : MonoBehaviour
{
    public IDRecognizer iDRecognizer;
    private string dataJsonPath;
    private string ScanSavePath;
    public IDScannerSettings iDScannerSettings;
    public Toggle serviceEnabledToggle;
    public Dropdown scannerDevicesDropdown;
    string currentDeviceName;
    WebCamTexture deviceWebcamTexture;
    public int allowedAge = 18;
    public InputField allowedAgeIF;
    public string arduinoActivateCommand;
    public InputField arduinoActivateCommandIF;
    public string arduinoDeactivateCommand;
    public InputField arduinoDeactivateCommandIF;
    public string showIDSentence;
    public bool showIDSentenceFuilure;
    public InputField showIDSentenceIF;
    public string scanSucceededSentence;
    public InputField scanSucceededSentenceIF;
    public string failureSentence;
    public InputField failureSentenceIF;
    public string underAgeSentence;
    public InputField underAgeSentenceIF;
    public string properAgeSentence;
    public InputField properAgeSentenceIF;
    public string onCancelSentence;
    public InputField onCancelAentenceIF;
    public GameObject iDScanScreenUI;
    public RawImage debugRawImage;
    public Button takePictureBtn;
    public Button cancerlBtn;
    bool scanStarted;
    bool lightIsOn;
    bool scannerIsOn;
    public AudioClip AudioConfirm; // ofek change

    private void Awake()
    {
        dataJsonPath = Application.streamingAssetsPath + "/RobobarData/Settings/idScannerSettings.json";
        ScanSavePath = Application.streamingAssetsPath + "/RobobarData/idScan.jpg";
        LoadSavedData();
    }

    void LoadSavedData()
    {
        string savedSettings = null;
        //if (Directory.Exists(Path.GetDirectoryName(dataJsonPath)))
        if(File.Exists(dataJsonPath))
            savedSettings = File.ReadAllText(dataJsonPath);

        if (!string.IsNullOrEmpty(savedSettings))
            iDScannerSettings = JsonUtility.FromJson<IDScannerSettings>(savedSettings);
        else //create defult
        {
            iDScannerSettings = new IDScannerSettings();
        }
    }

   
    void SaveSettingsData()
    {
        string settingsData = JsonUtility.ToJson(iDScannerSettings, true);
        //Debug.Log("ID scanner Settings as jSon:\n" + settingsData);

        if (!Directory.Exists(Path.GetDirectoryName(dataJsonPath)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(dataJsonPath));
            Debug.Log("Directory NOT Exists - Created");
        }

        File.WriteAllText(dataJsonPath, settingsData);
    }

    void Start()
    {
        takePictureBtn.gameObject.SetActive(false);
        debugRawImage.gameObject.SetActive(false);
        if (!Directory.Exists(Path.GetDirectoryName(dataJsonPath)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(dataJsonPath));
            Debug.Log("json directory NOT Exists - Created");
        }

        if (!Directory.Exists(Path.GetDirectoryName(ScanSavePath)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(ScanSavePath));
            Debug.Log("picture directory NOT Exists - Created");
        }

        iDScanScreenUI.SetActive(false);
        SetUIItems();
        GetScannerDevices();
    }

    void SetUIItems()
    {
        Manager.instance.isIDScanEnabled = serviceEnabledToggle.isOn = iDScannerSettings.isServiceEnabled;
        allowedAge = iDScannerSettings.allowedAge;
        allowedAgeIF.text = iDScannerSettings.allowedAge.ToString();
        arduinoActivateCommand = arduinoActivateCommandIF.text = iDScannerSettings.arduActivateCommand;
        arduinoDeactivateCommand = arduinoDeactivateCommandIF.text = iDScannerSettings.arduDeactivateCommand;
        showIDSentence = showIDSentenceIF.text = iDScannerSettings.showIDSentence;
        scanSucceededSentence = scanSucceededSentenceIF.text = iDScannerSettings.scanSucceededSentence;
        failureSentence = failureSentenceIF.text = iDScannerSettings.scanFailureSentence;
        underAgeSentence = underAgeSentenceIF.text = iDScannerSettings.underAgeSentence;
        properAgeSentence = properAgeSentenceIF.text = iDScannerSettings.properAgeSentence;
        onCancelSentence = onCancelAentenceIF.text = iDScannerSettings.onCancelAentence;
    }

    #region "Setting screen functions"
    // Setting screen function:
    public void SetAllowedAge(string age)
    {
        allowedAge = int.Parse(age);
        iDScannerSettings.allowedAge = int.Parse(age);
        SaveSettingsData();
    }
    public void SetServiceEnabledToggle(bool isEnabled)
    {
        Manager.instance.isIDScanEnabled = isEnabled;
        iDScannerSettings.isServiceEnabled = isEnabled;
        SaveSettingsData();
    }
    public void SetActivateLightCommand(string command)
    {
        arduinoActivateCommand = command;
        iDScannerSettings.arduActivateCommand = command;
        SaveSettingsData();
    }
    public void SetDeactivateLightCommand(string command)
    {
        arduinoDeactivateCommand = command;
        iDScannerSettings.arduDeactivateCommand = command;
        SaveSettingsData();
    }
    public void SetShowIDSentence(string command)
    {
        showIDSentence = command;
        iDScannerSettings.showIDSentence = command;
        SaveSettingsData();
    }

    public void SetScanSucceededSentence(string command)
    {
        scanSucceededSentence = command;
        iDScannerSettings.scanSucceededSentence = command;
        SaveSettingsData();
    }

    public void SetScanFailureSentence(string command)
    {
        failureSentence = command;
        iDScannerSettings.scanFailureSentence = command;
        SaveSettingsData();
    }

    public void SetUnderAgeSentence(string command)
    {
        underAgeSentence = command;
        iDScannerSettings.underAgeSentence = command;
        SaveSettingsData();
    }

    public void SetProperAgeSentence(string command)
    {
        properAgeSentence = command;
        iDScannerSettings.properAgeSentence = command;
        SaveSettingsData();
    }

    public void SetOnCancelSentence(string command)
    {
        onCancelSentence = command;
        iDScannerSettings.onCancelAentence = command;
        SaveSettingsData();
    }

    #endregion


    #region "Scanner Handling"
    void GetScannerDevices()
    {
        var devices = WebCamTexture.devices;
        List<string> scannerDevicesNames = new List<string>();
        for (int i = 0; i < devices.Length; i++)
        {
            scannerDevicesNames.Add(devices[i].name);
        }
        if (scannerDevicesNames.Count > 0)
        {
            scannerDevicesDropdown.AddOptions(scannerDevicesNames);
        }
        //check saved name:
        string savedDevice = iDScannerSettings.savedDeviceName;
        if (string.IsNullOrEmpty(savedDevice))
        {
            if (scannerDevicesNames.Count > 0)
            {
                scannerDevicesDropdown.value = 0;
                currentDeviceName = scannerDevicesNames[0];
                deviceWebcamTexture = new WebCamTexture(currentDeviceName);
                iDScannerSettings.savedDeviceName = currentDeviceName;
            }
            return;
        }
           
        for (int i = 0; i < scannerDevicesNames.Count; i++)
        {
            if (scannerDevicesNames[i] == savedDevice)
            {
                scannerDevicesDropdown.value = i;
                currentDeviceName = savedDevice;
                deviceWebcamTexture = new WebCamTexture(currentDeviceName);
            }
        }
        if (string.IsNullOrEmpty(currentDeviceName))
        {
            if(scannerDevicesNames.Count > 0)
            {
                scannerDevicesDropdown.value = 0;
                currentDeviceName = scannerDevicesNames[0];
                deviceWebcamTexture = new WebCamTexture(currentDeviceName);
                iDScannerSettings.savedDeviceName = currentDeviceName;
            }
        }
    }

    public void ChooseScannerDevice()
    {
        currentDeviceName = scannerDevicesDropdown.options[scannerDevicesDropdown.value].text;
        deviceWebcamTexture = new WebCamTexture(currentDeviceName);
        iDScannerSettings.savedDeviceName = currentDeviceName;
    }

    public void StartScanner() //The camera open
    {
        if (deviceWebcamTexture == null)
            ChooseScannerDevice();
        if (deviceWebcamTexture != null)
        {
            deviceWebcamTexture.Play();
            if (debugRawImage != null)
                debugRawImage.texture = deviceWebcamTexture;
            takePictureBtn.gameObject.SetActive(true);
        }
        else
        {
            Debug.LogError("deviceWebcamTexture = null");
        }
    }

    public void TakePicture()
    {
        scanStarted = true;
        StartCoroutine(TakeScannerPicture());
    }
    IEnumerator TakeScannerPicture()
    {
        Texture2D snap = new Texture2D(deviceWebcamTexture.width, deviceWebcamTexture.height);
        try
        {
            snap.SetPixels(deviceWebcamTexture.GetPixels());
            snap.Apply();
        }
        catch(Exception e)
        {
            DebugLogTxt.instance.Log("Line 273 - " + e.ToString(), true);
            Manager.instance.errorLogger.AddLog("Line 273 - " + e.ToString());
            StopScanner();
            yield break;
        }
        
        yield return new WaitForEndOfFrame();
        if (debugRawImage != null)
            debugRawImage.texture = snap;

        if (!Directory.Exists(Path.GetDirectoryName(ScanSavePath)))
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(ScanSavePath));
                DebugLogTxt.instance.Log("Line 286 - picture directory NOT Exists - Created", true);
                Debug.Log("picture directory NOT Exists - Created");
            }
            catch(Exception e)
            {
                DebugLogTxt.instance.Log("Line 292 - " + e.ToString(), true);
                Manager.instance.errorLogger.AddLog("Line 292 - " + e.ToString());
                Debug.Log(e.ToString());
                StopScanner();
                yield break;
            }
            yield return new WaitForEndOfFrame();
        }
        try
        {
            File.WriteAllBytes(ScanSavePath, snap.EncodeToJPG());
        }
        catch(Exception e)
        {
            DebugLogTxt.instance.Log("Line 305 - " + e.ToString(), true);
            Manager.instance.errorLogger.AddLog("Line 305 - " + e.ToString());
            Debug.Log(e.ToString());
            StopScanner();
            yield break;
        }
        DebugLogTxt.instance.Log("Scan file created in path: " + ScanSavePath, true);
        yield return new WaitForFixedUpdate();
        yield return new WaitForEndOfFrame();
        StopScanner();
        SendDeactivateLightToArduino();
        GetUserAge(ScanSavePath);
    }


    public void StopScanner()
    {
        if (deviceWebcamTexture != null && deviceWebcamTexture.isPlaying)
            deviceWebcamTexture.Stop();
    }

    #endregion

 

    public async void OnPleaseShowID()
    {
        Manager.instance.isCheckingID = true;
        scanStarted = false;
        // stop recording...
        if (Manager.instance.voiceDictationManager.IsRecording)
        {
            Manager.instance.voiceDictationManager.StopRecording(false, false, "OnPleaseShowID()");
            while (Manager.instance.voiceDictationManager.IsRecording)
                await Task.Delay(50);
        }
        else if (Manager.instance.voiceActivationManager.IsRecording)
        {
            Manager.instance.voiceActivationManager.StopRecording(false, "OnPleaseShowID()");
            while (Manager.instance.voiceActivationManager.IsRecording)
                await Task.Delay(50);
        }
        // show id scan popup...
        iDScanScreenUI.SetActive(true); //Scan ID pop up show!
        
        // say show ID sentence...
        if (!string.IsNullOrEmpty(showIDSentence) && !showIDSentenceFuilure)
        {
            Manager.instance.textToSpeechHandler.Speak(showIDSentence);
            Manager.instance.uiManager.ShowBarmanTxt(showIDSentence);
        }
            

        // activate light...
        SendActivateLightToArduino();

        // activate scanner (camera)...
        StartScanner();

        int timeCounter = 0;
        while(!scanStarted)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            timeCounter++;
            if (timeCounter >= 60)
            {
                OnUserCanceled();
                break;
            }
        }
    }


    public void GetUserAge(string picturePath) // get from IDAnalyzer...
    {
        DebugLogTxt.instance.Log("GetUserAge(string) - picturePath: " + picturePath, true);
        cancerlBtn.interactable = false;
        takePictureBtn.gameObject.SetActive(false);
        iDRecognizer.LoadImage(picturePath, OnIDScanSucceeded, OnIDScanFailed);
    }

    void OnIDScanSucceeded(string msg)
    {
        cancerlBtn.interactable = true;
        ClearImageData();
        Debug.Log("result msg = " + msg);
        if(string.IsNullOrEmpty(msg) || msg == "Null")
        {
            OnIDScanFailed(msg);
            return;
        }
        //try to parse the string to int...
        int age = -1;
        try
        {
            age = int.Parse(msg);
        }
        catch(Exception e)
        {
            DebugLogTxt.instance.Log("Age value is not valid: " + e, true);
            Debug.LogError("Age value is not valid: " + e);
        }
        finally
        {
            if(msg == null || age <= 0) //fail
            {
                OnIDScanFailed(msg);
            }
            else if(age > 0) //success
            {
                OnIDReadSucceeded(age);
            }
        }
    }

    async void OnIDReadSucceeded(int age)
    {
        // say scan success...
        if (!string.IsNullOrEmpty(scanSucceededSentence))
        {
            Manager.instance.textToSpeechHandler.Speak(scanSucceededSentence);
            Manager.instance.uiManager.ShowBarmanTxt(scanSucceededSentence);
        }

        await Task.Delay(50);
        while (Manager.instance.isRobobarSpeaking)
            await Task.Delay(50);
       
        // check the if proper age...
        bool isProperAge = IsUserAgeAllowed(age);

        if (isProperAge)
            OnUserApproved();
        else
            OnUserUnderAge();
    }
    async void OnIDScanFailed(string msg)
    {
        cancerlBtn.interactable = true;
        ClearImageData();
        //say scan failior sentence
        if (!string.IsNullOrEmpty(failureSentence))
        {
            Manager.instance.textToSpeechHandler.Speak(failureSentence);
            Manager.instance.uiManager.ShowBarmanTxt(failureSentence);
            while (Manager.instance.isRobobarSpeaking)
                await Task.Delay(50);
        }
        else
        {
            await Task.Delay(TimeSpan.FromSeconds(2));
        }    
        // show error msg...
        Debug.Log(msg);
        DebugLogTxt.instance.Log(msg, true);
        showIDSentenceFuilure = true;
        OnPleaseShowID();
        StartCoroutine(TimeIDsentence());
    }

   IEnumerator TimeIDsentence()
    {
        yield return new WaitForSeconds(3);
        showIDSentenceFuilure = false;
    }


    
    public async void OnUserUnderAge()
    {
        //say use under age
        ClearImageData();
        if (!string.IsNullOrEmpty(underAgeSentence))
        {
            Manager.instance.textToSpeechHandler.Speak(underAgeSentence);
            Manager.instance.uiManager.ShowBarmanTxt(underAgeSentence);
            
            while (Manager.instance.isRobobarSpeaking)
                await Task.Delay(50);
        }
        else
        {
            await Task.Delay(TimeSpan.FromSeconds(2));
        }

        OnPleaseShowID();
    }

    public async void OnUserApproved()
    {
        ClearImageData();
        GetComponent<AudioSource>().PlayOneShot(AudioConfirm); //Ofek change
        //say user approved
        if (!string.IsNullOrEmpty(properAgeSentence))
        {
            Manager.instance.textToSpeechHandler.Speak(properAgeSentence);
            Manager.instance.uiManager.ShowBarmanTxt(properAgeSentence);
            Manager.instance.isCheckingID = false;
            Manager.instance.isIDApproved = true;
            await Task.Delay(TimeSpan.FromSeconds(2));
        }
        else
        {
            await Task.Delay(TimeSpan.FromSeconds(2));
            if (Manager.instance.voiceActivationManager.IsRecording)
            {
                Manager.instance.voiceActivationManager.StopRecording(false, "OnUserApproved()");
                while (Manager.instance.voiceActivationManager.IsRecording)
                    await Task.Delay(50);
            }
            Manager.instance.voiceDictationManager.StartRecording("OnUserApproved() - no proper Age Sentence.");
            Manager.instance.isCheckingID = false;
            Manager.instance.isIDApproved = true;
        }
        Manager.instance.touchScreenUIManager.ShowDrinksMenu();
        scanStarted = false;
        iDScanScreenUI.SetActive(false);
    }

    public async void OnUserCanceled()
    {
        scanStarted = false;
        StopScanner();
        //say user cancel
        if (!string.IsNullOrEmpty(onCancelSentence))
        {
            Manager.instance.textToSpeechHandler.Speak(onCancelSentence);
            Manager.instance.uiManager.ShowBarmanTxt(onCancelSentence);
            Manager.instance.withUser = false;
            Manager.instance.isCheckingID = false;
            Manager.instance.isIDApproved = false;
        }
        else
        {
            Manager.instance.withUser = false;
            Manager.instance.isCheckingID = false;
            Manager.instance.isIDApproved = false;
            if (Manager.instance.voiceDictationManager.IsRecording)
            {
                Manager.instance.voiceDictationManager.StopRecording(false, false, "OnUserUnderAge()");
                while (Manager.instance.voiceDictationManager.IsRecording)
                    await Task.Delay(50);
            }
            Manager.instance.voiceActivationManager.StartRecording(false, "OnUserUnderAge() - no under Age Sentence.");
            Manager.instance.animationsManager.PlayWaitAnims();
        }

        Manager.instance.touchScreenUIManager.ShowPutCupScreen();
        SendDeactivateLightToArduino();
        iDScanScreenUI.SetActive(false);
    }



    
    public void SendActivateLightToArduino()
    {
        if (!string.IsNullOrEmpty(arduinoActivateCommand))
        {
            Manager.instance.arduinoHandler.SendToArdu(arduinoActivateCommand);
            lightIsOn = true;
        }  
    }


    public void SendDeactivateLightToArduino()
    {
        if (!string.IsNullOrEmpty(arduinoDeactivateCommand))
        {
            Manager.instance.arduinoHandler.SendToArdu(arduinoDeactivateCommand);
            lightIsOn = false;
        }
    }




    public bool IsUserAgeAllowed(string userBDate)
    {
        bool isAgeAllowed = false;
        //calculate user age...
        return isAgeAllowed;
    }

    public bool IsUserAgeAllowed(int userAge)
    {
        bool isAgeAllowed;
        if (userAge >= allowedAge)
            isAgeAllowed = true;
        else
            isAgeAllowed = false;
        return isAgeAllowed;
    }


    public void ClearImageData()
    {
        if (Directory.Exists(Path.GetDirectoryName(ScanSavePath)))
        {
            Texture2D snap = new Texture2D(16,16);
            File.WriteAllBytes(ScanSavePath, snap.EncodeToJPG());
        }
    }



    public void ResetEverything()
    {
        scanStarted = false;
        StopScanner();
        Manager.instance.withUser = false;
        Manager.instance.isCheckingID = false;
        Manager.instance.isIDApproved = false;
        if (lightIsOn)
            SendDeactivateLightToArduino();
        iDScanScreenUI.SetActive(false);
    }
}

[System.Serializable]
public class IDScannerSettings
{
    public bool isServiceEnabled;
    public int allowedAge = 18;
    public string arduActivateCommand = "";
    public string arduDeactivateCommand = "";
    public string savedDeviceName = "";
    public string showIDSentence = "";
    public string scanSucceededSentence = "";
    public string scanFailureSentence = "";
    public string underAgeSentence = "";
    public string properAgeSentence = "";
    public string onCancelAentence = "";

    public IDScannerSettings()
    {

    }
}
