﻿using OpenCVForUnityExample;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {


    public GameObject settingsScrn;
	public Button settingsBtn;
    public Button recordingBtn;
    public InputField portField;
	public Text barmanTextUI;
	public Text inputTxtUI;
    public GameObject micIcon;
    public GameObject DebugUI;
    public GameObject popupParent;
    public Text popupTxt;
    public Button popupCloseBtn;
    public Button popupYesBtn;
    public Button popupNoBtn;
    public Text recordBtnText;
    public Text restartText;
    public GameObject networkConnectionIssuesTxt;
    public GameObject microphoneIssuesTxt;


    bool isStarted = false;

    WaitForSeconds wait1Sec = new WaitForSeconds(1);
    int waitBarmanTxtIndex = 0;
    IEnumerator barmanTxtEnumerator;

    int waitUserTxtIndex = 0;
    IEnumerator usertxtEnumerator;

   


    void Awake(){
        HideNetworkConnectionIssuesTxt();
	}


    public void OpenSettingsScrn(){
		settingsScrn.SetActive (true);
        //Manager.instance.settingsScrnHandler.settingsPasswordScreen.SetActive(true);
		Manager.instance.settingsScrnOn = true;
        Manager.instance.micInput.InitMic();
        Manager.instance.checkInternet.Check();
    }


    // The main function that starts and stops recording:
	public void StartStopToggle(){
        recordingBtn.interactable = false;
        isStarted = !isStarted;
        if (isStarted)
        {
            StartCoroutine(StartRecording());
        }
        else
        {
            StartCoroutine(StopRecording());
        }
	}


    IEnumerator StartRecording()
    {
        //if no internet show message and stop session, else make the drink.
        Manager.instance.checkInternet.Check();
        yield return new WaitForSeconds(0.1f);
        while (Manager.instance.checkInternet.isCheckingNetwork)
            yield return null;

        if (Manager.instance.checkInternet.isNetworktError)
        {
            Manager.instance.uiManager.barmanTextUI.text = "Please check your internet connection.";
            DebugLogTxt.instance.Log("Please check your internet connection.", true);
            Manager.instance.errorLogger.AddLog("Cecilia start error - Please check internet connection.");
            isStarted = false;
            recordingBtn.interactable = true;
            yield break;
        }
        if (!Manager.instance.arduinoHandler.portSet)
        {
            Manager.instance.uiManager.barmanTextUI.text = "Please set port name";
            DebugLogTxt.instance.Log("Cecilia start error - Please set port name.", true);
            Manager.instance.errorLogger.AddLog("Cecilia start error - Please set port name.");
            isStarted = false;
            recordingBtn.interactable = true;
            yield break;
        }
        if (string.IsNullOrEmpty(Manager.instance.ceciliaID))
        {
            Manager.instance.uiManager.barmanTextUI.text = "Please enter Cecilia ID in the settings screen.";
            isStarted = false;
            recordingBtn.interactable = true;
            yield break;
        }

        Manager.instance.micInput.InitMic();
        yield return new WaitForSeconds(0.1f);
        if (Manager.instance.micInput.micDivicesCount == 0)
        {
            Manager.instance.uiManager.barmanTextUI.text = "Please connect a microphone.";
            isStarted = false;
            recordingBtn.interactable = true;
            yield break;
        }

        yield return Manager.instance.voiceDictationManager.InitSpeechRecognizer();
       
        Debug.Log("Speech Recognizer initialized...");
        if (!Manager.instance.voiceDictationManager.isInitialized)
        {
            DebugLogTxt.instance.Log("speech service can not be initialized.", true);
            Manager.instance.errorLogger.AddLog("Cecilia start error - Speech service can not be initialized.");
            isStarted = false;
            recordingBtn.interactable = true;
            yield break;
        }

        Manager.instance.voiceActivationManager.StartRecording(true, "UI manager - RecordingToggle");
        Manager.instance.firestoreManager.SendLogData("start");
    }

    IEnumerator StopRecording()
    {
        Manager.instance.isTrueRecording = false;
        Manager.instance.withUser = false;
        Manager.instance.isIDApproved = false;
        Manager.instance.isCheckingID = false;

        Manager.instance.textToSpeechHandler.StopOnTheMiddle();
        while (Manager.instance.textToSpeechHandler.isGettingSpeech)
        {
            yield return null;
        }

        if (!Manager.instance.voiceActivationManager.IsRecording && !Manager.instance.voiceDictationManager.IsRecording)
        {
            StopRecordingUI();
        }
        else
        {
            if (Manager.instance.voiceActivationManager.IsRecording)
            {
                Manager.instance.voiceActivationManager.StopRecording(true, "StopRecording()");
                while (Manager.instance.voiceActivationManager.IsRecording)
                    yield return null;
            }

            if (Manager.instance.voiceDictationManager.IsRecording)
            {
                Manager.instance.voiceDictationManager.StopRecording(true, true, "StopRecording()");
                while (Manager.instance.voiceDictationManager.IsRecording)
                    yield return null;
            }
        }

        Manager.instance.firestoreManager.SendLogData("stop");
    }

    //After recording started successfully, set the UI accordingly: (called from the voice activation manager)
    public void StartRecordingUI(){
        Manager.instance.touchScreenUIManager.ActivateDrinksMenu();
        Manager.instance.sessionLogManager.StartNewSession();
        HideNetworkConnectionIssuesTxt();
        recordBtnText.text = "Stop";
        settingsBtn.interactable = false;
		portField.interactable = false;
		barmanTextUI.text = "Hi, I'm Cecilia.";
        isStarted = true;
        Manager.instance.arduinoHandler.StartReadFromArduino();
        recordingBtn.interactable = true;
        Manager.instance.checkInternet.StartListenningToInternet();
    }

	public void StopRecordingUI(){
        micIcon.SetActive(false);
        Manager.instance.micInput.StopMicrophone();
        Manager.instance.commandProcessor.ResetAll();
        Manager.instance.faceRecognitionManager.ResetFaceRecognition(true);
        recordBtnText.text = "Start";
		settingsBtn.interactable = true;
		portField.interactable = true;
		inputTxtUI.text = "";
        Manager.instance.isTrueRecording = false;
        Manager.instance.withUser = false;
        Manager.instance.arduinoHandler.StopReadFromArduino();
        //Manager.instance.sessionLogManager.SaveCurrentSession();
        Manager.instance.checkInternet.StopListenningToInternet();
        isStarted = false;
        recordingBtn.interactable = true;
        Manager.instance.isResetting = false;
    }



    public void AutoStartRecording()
    {
        if (isStarted) return;

        isStarted = true;

        StartCoroutine(StartRecording());
    }


    // Show the content of the bartender's speaking:
    public void ShowBarmanTxt(string txt, int time = 7, bool pernament = false)
    {
        if (barmanTxtEnumerator != null)
            StopCoroutine(barmanTxtEnumerator);
        barmanTxtEnumerator = ShowSpeakingTxtCo(txt, time, pernament);
        StartCoroutine(barmanTxtEnumerator);
    }

    IEnumerator ShowSpeakingTxtCo(string txt, int time = 7, bool pernament = false)
    {
        waitBarmanTxtIndex = 0;
        barmanTextUI.text = txt;
        while (waitBarmanTxtIndex < time)
        {
            yield return wait1Sec;
            waitBarmanTxtIndex++;
        }
        if (!pernament)
            barmanTextUI.text = "";
    }

    // The user's speech input - the UI is not active anymore so doesn't do anything:
    public void ShowUserTxt(string txt, int time = 10)
    {
        if (inputTxtUI != null && inputTxtUI.gameObject.activeSelf)
        {
            if (usertxtEnumerator != null)
                StopCoroutine(usertxtEnumerator);
            usertxtEnumerator = ShowUserTxtCo(txt, time);
            StartCoroutine(usertxtEnumerator);
        }
    }

    IEnumerator ShowUserTxtCo(string txt, int time = 10)
    {
        waitUserTxtIndex = 0;
        inputTxtUI.text = txt;
        while (waitUserTxtIndex < time)
        {
            yield return wait1Sec;
            waitUserTxtIndex++;
        }

        inputTxtUI.text = "";
    }



    public void ShowPopup(string msg, UnityAction yesAction = null)
    {
        popupParent.SetActive(true);
        popupTxt.text = msg;
        if(yesAction == null)
        {
            popupYesBtn.gameObject.SetActive(false);
            popupNoBtn.gameObject.SetActive(false);
            popupCloseBtn.gameObject.SetActive(true);
        }
        else
        {
            popupYesBtn.gameObject.SetActive(true);
            popupNoBtn.gameObject.SetActive(true);
            popupCloseBtn.gameObject.SetActive(false);
            popupYesBtn.onClick.RemoveAllListeners();
            popupYesBtn.onClick.AddListener(yesAction);
            popupYesBtn.onClick.AddListener(() => popupParent.SetActive(false));
        }
    }



    //If a critical error is accorded and auto restart recording is required:
    public void RestartCountDown()
    {
        StartCoroutine(RestartCountDownCo());
    }

    IEnumerator RestartCountDownCo()
    {
        int countDown = 5;
        restartText.text = "Restarting in " + countDown;
        restartText.gameObject.SetActive(true);
        while (countDown > 0)
        {
            yield return wait1Sec;
            if (isStarted)
            {
                restartText.gameObject.SetActive(false);
                yield break;
            }
                
            countDown--;
            restartText.text = "Restarting in " + countDown;
        }
        restartText.gameObject.SetActive(false);
        AutoStartRecording();
    }


    public void ShowNetworkConnectionIssuesTxt()
    {
        networkConnectionIssuesTxt.SetActive(true);
        Invoke("HideNetworkConnectionIssuesTxt", 20);
    }

    public void HideNetworkConnectionIssuesTxt()
    {
        networkConnectionIssuesTxt.SetActive(false);
    }

    public void ShowMicrophoneIssuesTxt()
    {
        microphoneIssuesTxt.SetActive(true);
        Invoke("HideMicrophoneIssuesTxt", 10);
    }

    public void HideMicrophoneIssuesTxt()
    {
        microphoneIssuesTxt.SetActive(false);
    }

   
}
