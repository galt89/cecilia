﻿using Firebase;
using Firebase.Extensions;
using Firebase.Firestore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;


public class FirestoreManager : MonoBehaviour
{

    public Manager manager;
    FirebaseFirestore db;
    FirebaseApp app;
    public bool firebaseInitialized;
    List<string> errorsQue = new List<string>();
    public bool isSendingData;
   // public string DrinkId;
    
  



    async void Start()
    {
       // DrinkId = manager.drinkId;
        await Task.Delay(TimeSpan.FromSeconds(1));

        firebaseInitialized = await InitFirebase();

        if (!firebaseInitialized)
        {
            Debug.LogError("Firebase not initialized");
            return;
        }
        
    }


    async Task<bool> InitFirebase()
    {
        db = FirebaseFirestore.DefaultInstance;
        bool initialized = false;
        try
        {
            await Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;

                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    app = Firebase.FirebaseApp.DefaultInstance;
                    initialized = true;
                    Debug.Log("Firebase initialized.");
                }
                else
                {
                    UnityEngine.Debug.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                }
            });
        }
        catch(Exception e)
        {
            Debug.LogError(e);
        }
       
        return initialized;
    }



    /*
        cecilia_id: "cecilia001"
        conversation_text: "I love you"
        is_bartender: "FALSE"
        session_id2: "1005"
        timestamp: "17/06/2021 12:26:48"
        * */
    public async void SendConversationData(string text, bool isBartender)
    {
        if (!firebaseInitialized) return;
        //if (!manager.withUser) return;
        isSendingData = true;
        Dictionary<string, object> conversationDataDic = new Dictionary<string, object>();
        conversationDataDic.Add("cecilia_id", manager.ceciliaID);
        conversationDataDic.Add("conversation_text", text);
        conversationDataDic.Add("is_bartender", isBartender);
        conversationDataDic.Add("session_id", manager.sessionID);
        conversationDataDic.Add("timestamp", DateTime.Now);
        


        try
        {
            DocumentReference docRef = db.Collection("cecilia_conversation").Document();
            await docRef.SetAsync(conversationDataDic).ContinueWithOnMainThread(task => {
                isSendingData = false;
            });
        }
        catch(Exception e)
        {
            Debug.LogError(e);
        }
        finally
        {
            isSendingData = false;
        }
    }


    /*
     *      "session_id": "1001",
            "cecilia_id": "cecilia001",
            "is_touchscreen": "FALSE",
            "drink_name": "Red Alert",
            "timestamp": "17/06/2021 9:28:30"
     * */
    
    public async void SendDrinkData(bool isTouchScreen, string drinkName, string pumpsInfo)
    {
        if (!firebaseInitialized) return;
        isSendingData = true;
        Dictionary<string, object> drinkDataDic = new Dictionary<string, object>();
        drinkDataDic.Add("session_id", manager.sessionID);
        drinkDataDic.Add("cecilia_id", manager.ceciliaID);
        drinkDataDic.Add("is_touchscreen", isTouchScreen);
        drinkDataDic.Add("drink_name", drinkName);
        drinkDataDic.Add("timestamp", DateTime.Now);
       // drinkDataDic.Add("drink_id", drinkId);
        drinkDataDic.Add("pumps_info", pumpsInfo);
        try
        {
            DocumentReference docRef = db.Collection("cecilia_drinks").Document();
            await docRef.SetAsync(drinkDataDic).ContinueWithOnMainThread(task => {
                isSendingData = false;
            });
        }
        catch(Exception e)
        {
            Debug.LogError(e);
        }
        finally
        {
            isSendingData = false;
        }
    }



    /*
     action: "error"
    cecilia_id: "cecilia001"
    error_code: "522"
    error_desc: "No microphone device detected"
    timestamp: "21/06/2021 8:22:45"
     * */
    public async void SendLogData(string action, string errorDesc = "")
    {
        //check if internet available, if not, add log to que and return:
        if (manager.checkInternet.isNetworktError)
        {
            errorsQue.Add("Offline " + action + " - Occurred at " + DateTime.Now + " - " + errorDesc);
            return;
        }
        if (!firebaseInitialized) return;
        isSendingData = true;
        Dictionary<string, object> logDataDic = new Dictionary<string, object>();

        logDataDic.Add("action", action);

        logDataDic.Add("cecilia_id", Manager.instance.ceciliaID);

        //if(!string.IsNullOrEmpty(errorCode))
        //    logDataDic.Add("error_code", errorCode);

        if (!string.IsNullOrEmpty(errorDesc))
            logDataDic.Add("error_desc", errorDesc);

        logDataDic.Add("timestamp", DateTime.Now);

        logDataDic.Add("IP", manager.checkInternet.publicIpv4);

        // Location (not in use):
        //string currentLocation;
        //if (LocationManager.instance.status == "active")
        //    currentLocation = LocationManager.instance.Latitude + "," + LocationManager.instance.Longitude;
        //else
        //    currentLocation = LocationManager.instance.status;
        //logDataDic.Add("Current Location", currentLocation);

        try
        {
            DocumentReference docRef = db.Collection("cecilia_log").Document();
            await docRef.SetAsync(logDataDic).ContinueWithOnMainThread(task => {
                isSendingData = false;
            });
        }
        catch(Exception e)
        {
            Debug.LogError(e);
        }
        finally
        {
            if (isSendingData)
            {
                Debug.Log("something went wrong with sending the data...");
            }
            isSendingData = false;
        }
    }

    public async void SendOfflineLogs()
    {
        for(int i = 0; i< errorsQue.Count; i++)
        {
            await Task.Delay(100);
            SendLogData("error", errorsQue[i]);
        }
        await Task.Delay(1000);
        errorsQue.Clear();
    }
}
