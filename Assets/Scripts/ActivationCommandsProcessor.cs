﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public partial class CommandsProcessor : MonoBehaviour
{

    public async void OnHiSayed()
    {
        processingCommand = true;
        manager.faceRecognitionManager.ResetFaceRecognition();
       
        manager.sessionID++;
        manager.withUser = true;

        while (manager.switchingCharacters)
            await Task.Delay(TimeSpan.FromSeconds(0.05f));

      
        if (manager.isIDScanEnabled) {
            UpdateActivationResponseData(manager.cupActivationCommand);
            manager.textToSpeechHandler.Speak(currentCommandResponse1);
            manager.animationsManager.PlayAnimation(currentResponesAmin1);
            manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
        }
        else
        {
            PlayerPrefs.SetInt("session_id", manager.sessionID);
            manager.touchScreenUIManager.ShowDrinksMenu();
            UpdateActivationResponseData(manager.voiceActivationNoCupCommand);
            manager.textToSpeechHandler.Speak(currentCommandResponse1);
            manager.animationsManager.PlayAnimation(currentResponesAmin1);
            manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
            manager.touchScreenUIManager.ShowDrinksMenu();
        }
           // manager.touchScreenUIManager.ShowDrinksMenu();
       
    }


    public async void OnNewFaceRecognized()
    {
        processingCommand = true;
        manager.voiceActivationManager.StopRecording();

        manager.sessionID++;
        manager.withUser = true;
        UpdateActivationResponseData(Manager.instance.faceFirstTimeCommand);
        manager.textToSpeechHandler.Speak(currentCommandResponse1);
        manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
        manager.animationsManager.PlayAnimation(currentResponesAmin1);
        if (!manager.isIDScanEnabled)
            manager.touchScreenUIManager.ShowDrinksMenu();
        PlayerPrefs.SetInt("session_id", manager.sessionID);
    }


    public void OnFemiliarFaceRecognized(string drinkName, ConversationCommand previosCommand)
    {
        if (previosCommand == null)
            Debug.Log("OnFemiliarFaceRecognized() - previos Command is null...");
        else
            Debug.Log("OnFemiliarFaceRecognized() - " + drinkName + ", " + previosCommand.output);
        processingCommand = true;
        manager.voiceActivationManager.StopRecording();

        manager.sessionID++;
        manager.withUser = true;
        userPreviosDrink = drinkName;
        if (string.IsNullOrEmpty(userPreviosDrink) || userPreviosDrink == "drink" || previosCommand == null)
        {
            UpdateActivationResponseData(manager.faceFirstTimeCommand);
            userPreviosDrink = "drink";
            manager.textToSpeechHandler.Speak(currentCommandResponse1);
            manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
        }
        else
        {
            UpdateActivationResponseData(manager.faceSecondTimeCommand);
            userPreviosCommand = previosCommand;
            isWaitingForDrinkQuestion = true;
            currentCommandResponse1 = currentCommandResponse1.Replace("*drink", userPreviosDrink);
            manager.textToSpeechHandler.Speak(currentCommandResponse1);
            manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
        }
        manager.animationsManager.PlayAnimation(currentResponesAmin1);
        if (!manager.isIDScanEnabled)
            manager.touchScreenUIManager.ShowDrinksMenu();
        PlayerPrefs.SetInt("session_id", manager.sessionID);
    }


    public void OnTimeoutFirstTime()
    {
            processingCommand = true;
            UpdateActivationResponseData(manager.firstTimeoutCommand);
            manager.textToSpeechHandler.Speak(currentCommandResponse1);
            manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
            manager.animationsManager.PlayAnimation(currentResponesAmin1);   
    }
    public void OnTimeoutFirstTime2() //Ofek changes
    {
        processingCommand = true; 

        //UpdateActivationResponseData(manager.firstTimeoutCommand);
        manager.textToSpeechHandler.Speak(currentCommandResponse1);
        manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
        manager.animationsManager.PlayAnimation(currentResponesAmin1);
    }


    public async void OnTimeoutSecondTime()
    {
        processingCommand = true;

        UpdateActivationResponseData(manager.secondTimeoutCommand);
        manager.textToSpeechHandler.Speak(currentCommandResponse1);
        manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
        manager.animationsManager.PlayAnimation(currentResponesAmin1);
        manager.touchScreenUIManager.ShowTakeYourCupScreen();

        await Task.Delay(TimeSpan.FromSeconds(0.1f));
        manager.withUser = false;
        manager.isIDApproved = false;
        manager.isCheckingID = false;
        manager.faceRecognitionManager.ResetFaceRecognition();

        await Task.Delay(TimeSpan.FromSeconds(4.9f));
        manager.touchScreenUIManager.ShowPutCupScreen();
    }


    public async void OnDrinkReady()
    {
        processingCommand = true;
        
        UpdateActivationResponseData(manager.endConversationCommand);
        manager.textToSpeechHandler.Speak(currentCommandResponse1);
        manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
        manager.animationsManager.PlayAnimation(currentResponesAmin1);

        manager.touchScreenUIManager.ShowTakeYourDrinkScreen();

        await Task.Delay(TimeSpan.FromSeconds(0.1f));
        manager.withUser = false;
        manager.isIDApproved = false;
        manager.isCheckingID = false;
        manager.faceRecognitionManager.ResetFaceRecognition();

        await Task.Delay(TimeSpan.FromSeconds(4.9f));
        manager.touchScreenUIManager.ShowPutCupScreen();
    }





    public void OnThanksSayed()
    {
        processingCommand = true;
       
        if (!manager.switchingCharacters)
        {
            UpdateActivationResponseData(manager.saidThanksCommand);
            manager.textToSpeechHandler.Speak(currentCommandResponse1);
            manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
            manager.animationsManager.PlayAnimation(currentResponesAmin1);
        }
       
        manager.withUser = false;
    }


    public async void OnCupOrTouchDetected()
    {
        if (processingCommand || manager.withUser)
            return;
        processingCommand = true;
        if (manager.voiceActivationManager.IsRecording)
        {
            manager.voiceActivationManager.StopRecording();
            while (manager.voiceActivationManager.IsRecording)
            {
                await Task.Delay(TimeSpan.FromSeconds(0.1f));
            }
        }

        manager.faceRecognitionManager.ResetFaceRecognition();
        manager.sessionID++;
        manager.withUser = true;
        
        while(manager.switchingCharacters)
            await Task.Delay(TimeSpan.FromSeconds(0.05f));

        if (manager.isIDScanEnabled)
        {
            UpdateActivationResponseData(manager.cupActivationCommand);
            manager.textToSpeechHandler.Speak(currentCommandResponse1);
            manager.animationsManager.PlayAnimation(currentResponesAmin1);
            manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
        }
        else
        {
            manager.touchScreenUIManager.ShowDrinksMenu();
            UpdateActivationResponseData(manager.voiceActivationNoCupCommand);
            manager.textToSpeechHandler.Speak(currentCommandResponse1);
            manager.animationsManager.PlayAnimation(currentResponesAmin1);
            manager.uiManager.ShowBarmanTxt(currentCommandResponse1);
        }

       
           // manager.touchScreenUIManager.ShowDrinksMenu();

      //  UpdateActivationResponseData(manager.voiceActivationNoCupCommand);
        PlayerPrefs.SetInt("session_id", manager.sessionID);
    }


}
