﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using SpeechLib;
using System.IO;
using System;
using CrazyMinnow.SALSA;
using UnityEngine.Networking;
using UnityEngine.UI;
using Microsoft.CognitiveServices.Speech;
using System.Threading.Tasks;

public class TextToSpeechHandler : MonoBehaviour {

    
    private SpeechConfig speechConfig;
    private SpeechSynthesizer currentSynthesizer;
    private SpeechSynthesizer synthesizerFemale;
    private SpeechSynthesizer synthesizerMale;

    public Dropdown maleVoicesDropdown;
    public Dropdown femaleVoicesDropdown;

	WaitForFixedUpdate waitFixedUpdate = new WaitForFixedUpdate();
    WaitForSeconds waitOneSec = new WaitForSeconds(1);
    public InputField voicesInputField;
	List<string> voicesOptions = new List<string>();
	string[] splittedVoiceDiscription;
	string[] voiceDiscriptionSeparator = new string[]{","};
	int selectedVoiceIndex;
    int selectedMaleVoiceIndex;
    int selectedFemaleVoiceIndex;

    public Salsa3D salsa;
    public Salsa3D maleSalsa;
    public Salsa3D femaleSalsa;

    public bool isGettingSpeech;



    void Start()
    {
        InitSpeech();
        string savedVoicesInputField = PlayerPrefs.GetString("saved_voices");
        if (string.IsNullOrEmpty(savedVoicesInputField))
        {
            savedVoicesInputField = "en-US-JennyNeural,en-US-GuyNeural";
        }
        voicesInputField.text = savedVoicesInputField;
        UpdateDropDowns();

    }

    public void UpdateVoicesList()
    {
        PlayerPrefs.SetString("saved_voices", voicesInputField.text);
        UpdateDropDowns();
    }

    void UpdateDropDowns()
    {
        
        splittedVoiceDiscription = voicesInputField.text.Split(voiceDiscriptionSeparator, System.StringSplitOptions.RemoveEmptyEntries);
        voicesOptions.Clear();
        foreach (string item in splittedVoiceDiscription)
        {
            voicesOptions.Add(item);
        }
        maleVoicesDropdown.ClearOptions();
        maleVoicesDropdown.AddOptions(voicesOptions);
        femaleVoicesDropdown.ClearOptions();
        femaleVoicesDropdown.AddOptions(voicesOptions);

        //male:
        maleVoicesDropdown.onValueChanged.RemoveAllListeners();
        selectedMaleVoiceIndex = PlayerPrefs.GetInt("male_voice");
        maleVoicesDropdown.value = selectedMaleVoiceIndex;
        maleVoicesDropdown.onValueChanged.AddListener(delegate { ChooseVoiceMale(); });
        //female:
        femaleVoicesDropdown.onValueChanged.RemoveAllListeners();
        selectedFemaleVoiceIndex = PlayerPrefs.GetInt("female_voice");
        femaleVoicesDropdown.value = selectedFemaleVoiceIndex;
        femaleVoicesDropdown.onValueChanged.AddListener(delegate { ChooseVoiceFemale(); });

        SetSpeechVoices();
    }


    public void ChooseVoiceMale()
    {
        Debug.Log("ChooseVoiceMale()");
        selectedMaleVoiceIndex = maleVoicesDropdown.value;
        PlayerPrefs.SetInt("male_voice", selectedMaleVoiceIndex);

        speechConfig.SpeechSynthesisVoiceName = maleVoicesDropdown.options[maleVoicesDropdown.value].text;
        Debug.Log("male Salsa voice is " + maleVoicesDropdown.options[maleVoicesDropdown.value].text);
        synthesizerMale = new SpeechSynthesizer(speechConfig, null);

        if (!_isFemale)
        {
            currentSynthesizer = synthesizerMale;
        }
    }

    public void ChooseVoiceFemale()
    {
        Debug.Log("ChooseVoiceFemal()");
        selectedFemaleVoiceIndex = femaleVoicesDropdown.value;
        PlayerPrefs.SetInt("female_voice", selectedFemaleVoiceIndex);

        speechConfig.SpeechSynthesisVoiceName = femaleVoicesDropdown.options[femaleVoicesDropdown.value].text;
        Debug.Log("female Salsa voice is " + femaleVoicesDropdown.options[femaleVoicesDropdown.value].text);
        synthesizerFemale = new SpeechSynthesizer(speechConfig, null);

        if (_isFemale)
        {
            currentSynthesizer = synthesizerFemale;
        }
    }

    public void InitSpeech()
    {
        try
        {
            speechConfig = SpeechConfig.FromSubscription("dc97f6a7de9b47cc822625aa29b453ca", "westeurope");
            speechConfig.SetSpeechSynthesisOutputFormat(SpeechSynthesisOutputFormat.Raw24Khz16BitMonoPcm);
        }
        catch(Exception e)
        {
            Debug.LogError(e);
            DebugLogTxt.instance.Log("Text to Speech error: " + e.ToString(), true);
            Manager.instance.errorLogger.AddLog("Text to Speech error: " + e);
        }
        //SetSpeechVoices();
    }

    public void SetSpeechVoices()
    {
        speechConfig.SpeechSynthesisVoiceName = femaleVoicesDropdown.options[femaleVoicesDropdown.value].text;
        //Debug.Log("female Salsa voice is " + femaleVoicesDropdown.options[femaleVoicesDropdown.value].text);
        synthesizerFemale = new SpeechSynthesizer(speechConfig, null);

        speechConfig.SpeechSynthesisVoiceName = maleVoicesDropdown.options[maleVoicesDropdown.value].text;
        //Debug.Log("male Salsa voice is " + maleVoicesDropdown.options[maleVoicesDropdown.value].text);
        synthesizerMale = new SpeechSynthesizer(speechConfig, null);
    }

    bool _isFemale;
    public void SwitchCharacters(bool isFemale)
    {
        if (isFemale)
        {
            currentSynthesizer = synthesizerFemale;
            salsa = femaleSalsa;
        } 
        else
        {
            currentSynthesizer = synthesizerMale;
            salsa = maleSalsa;
        }
        _isFemale = isFemale;
    }





    async public void Speak(string text)
    {
        if (Manager.instance.isRobobarSpeaking)
        {
            Manager.instance.isRobobarSpeaking = false;
            StopAllCoroutines();
        }
       
        StopOnTheMiddle();
        Manager.instance.isRobobarSpeaking = true;

        isGettingSpeech = true;
        //StartCoroutine(MeasureTime());
        await VoiceToFile(text);
    }




    async Task VoiceToFile(string txt)
    {
        SpeechSynthesisResult result = await currentSynthesizer.SpeakTextAsync(txt);
        
        isGettingSpeech = false;
        // Checks result.
        if (result.Reason == ResultReason.SynthesizingAudioCompleted)
        {
            // Native playback is not supported on Unity yet (currently only supported on Windows/Linux Desktop).
            // Use the Unity API to play audio here as a short term solution.
            // Native playback support will be added in the future release.
            var sampleCount = result.AudioData.Length / 2;
            var audioData = new float[sampleCount];
            for (var i = 0; i < sampleCount; ++i)
            {
                audioData[i] = (short)(result.AudioData[i * 2 + 1] << 8 | result.AudioData[i * 2]) / 32768.0F;
            }

            // The output audio format is 16K 16bit mono
            var audioClip = AudioClip.Create("SynthesizedAudio", sampleCount, 1, 24000, false);
            audioClip.SetData(audioData, 0);

            if (Manager.instance.isRobobarSpeaking)
                StartCoroutine(GetAudioClipCo(audioClip));
        }
        else if (result.Reason == ResultReason.Canceled)
        {
            var cancellation = SpeechSynthesisCancellationDetails.FromResult(result);
            string newMessage = $"CANCELED:\nReason=[{cancellation.Reason}]\nErrorDetails=[{cancellation.ErrorDetails}]\nDid you update the subscription info?";
            Debug.Log(newMessage);
            DebugLogTxt.instance.Log("TextToSpeech - VoiceToFile() - " + newMessage, true);
            Manager.instance.errorLogger.AddLog("TextToSpeech - VoiceToFile() - " + newMessage);
            Manager.instance.ResetEverything(true);
            Manager.instance.checkInternet.Check();
        }

        Manager.instance.firestoreManager.SendConversationData(txt, true);
    }

    IEnumerator MeasureTime()
    {
        float startTime = Time.time;
        while (isGettingSpeech)
        {
            yield return null;
            if(Time.time - startTime > 4f)
            {
                Manager.instance.touchScreenUIManager.DeactivateDrinksMenu();
                Debug.Log("Text to speech takes too much time");
                DebugLogTxt.instance.Log("Text to speech took too much time and was canceled.");
                Manager.instance.errorLogger.AddLog("Text to speech took too much time and was canceled.");
                StopOnTheMiddle();
                Manager.instance.uiManager.ShowNetworkConnectionIssuesTxt();
                yield break;
            }
        }
    }

    IEnumerator GetAudioClipCo(AudioClip speechClip)
    {
        if (speechClip != null)
        {
            try
            {
                salsa.SetAudioClip(speechClip);
                salsa.Play();
            }
            catch (Exception e)
            {
                Debug.Log("salsa error - " + e);
                DebugLogTxt.instance.Log("salsa error - " + e.ToString(), true);
            }
        }

        if (speechClip != null)
        {
            float timeToWait = speechClip.length - 0.5f;
            if (timeToWait > 0)
                yield return new WaitForSecondsRealtime(timeToWait);
        }

        if (!Manager.instance.isRobobarSpeaking)
            yield break;

        Manager.instance.isRobobarSpeaking = false;

        if (Manager.instance.isTrueRecording)
            Manager.instance.commandProcessor.OnDoneSpeaking();
    }


    public async void StopOnTheMiddle()
    {
        if (isGettingSpeech)
        {
            if(currentSynthesizer != null)
                await currentSynthesizer.StopSpeakingAsync();
        }
        if(salsa != null)
        {
            if (salsa.isTalking)
                salsa.Stop();
        }
        Manager.instance.isRobobarSpeaking = false;
    }


    // Test speech voices:

    //IEnumerator speechTestCoroutine;
    public AudioSource testAudioSource;
    public InputField testSpeechTxt;

    public void SpeechTest(bool isWoman)
    {
        if (isWoman)
        {
            currentSynthesizer = synthesizerFemale;
        }
        else
        {
            currentSynthesizer = synthesizerMale;
        }

        SpeechTest();

        //if (speechTestCoroutine != null)
        //    StopCoroutine(speechTestCoroutine);
        //speechTestCoroutine = SpeechTestCo();
        //StartCoroutine(speechTestCoroutine);
    }


    void SpeechTest()
    {
        // Starts speech synthesis, and returns after a single utterance is synthesized.
        using (var result = currentSynthesizer.SpeakTextAsync(testSpeechTxt.text).Result)
        {
            // Checks result.
            if (result.Reason == ResultReason.SynthesizingAudioCompleted)
            {
                // Native playback is not supported on Unity yet (currently only supported on Windows/Linux Desktop).
                // Use the Unity API to play audio here as a short term solution.
                // Native playback support will be added in the future release.
                var sampleCount = result.AudioData.Length / 2;
                var audioData = new float[sampleCount];
                for (var i = 0; i < sampleCount; ++i)
                {
                    audioData[i] = (short)(result.AudioData[i * 2 + 1] << 8 | result.AudioData[i * 2]) / 32768.0F;
                }

                // The output audio format is 16K 16bit mono
                //var audioClip = AudioClip.Create("SynthesizedAudio", sampleCount, 1, 16000, false);
                var audioClip = AudioClip.Create("SynthesizedAudio", sampleCount, 1, 24000, false);
                audioClip.SetData(audioData, 0);
                testAudioSource.clip = audioClip;
                testAudioSource.Play();

                Debug.Log("Speech test synthesis succeeded!");
            }
            else if (result.Reason == ResultReason.Canceled)
            {
                var cancellation = SpeechSynthesisCancellationDetails.FromResult(result);
                string newMessage = $"CANCELED:\nReason=[{cancellation.Reason}]\nErrorDetails=[{cancellation.ErrorDetails}]\nDid you update the subscription info?";
                Debug.Log(newMessage);
            }
        }
    }




    //async Task TTest(string txt)
    //{
    //    var result = await currentSynthesizer.SpeakTextAsync(txt);
    //}
    //void VoiceToFile(string txt)
    //{
    //    //StartCoroutine(GetAudioClipCo(testAudioClip));
    //    //return;
    //    // Starts speech synthesis, and returns after a single utterance is synthesized.
    //    using (var result = currentSynthesizer.SpeakTextAsync(txt).Result)
    //    {
    //        // Checks result.
    //        if (result.Reason == ResultReason.SynthesizingAudioCompleted)
    //        {
    //            // Native playback is not supported on Unity yet (currently only supported on Windows/Linux Desktop).
    //            // Use the Unity API to play audio here as a short term solution.
    //            // Native playback support will be added in the future release.
    //            var sampleCount = result.AudioData.Length / 2;
    //            var audioData = new float[sampleCount];
    //            for (var i = 0; i < sampleCount; ++i)
    //            {
    //                audioData[i] = (short)(result.AudioData[i * 2 + 1] << 8 | result.AudioData[i * 2]) / 32768.0F;
    //            }

    //            // The output audio format is 16K 16bit mono
    //            //var audioClip = AudioClip.Create("SynthesizedAudio", sampleCount, 1, 16000, false);
    //            var audioClip = AudioClip.Create("SynthesizedAudio", sampleCount, 1, 24000, false);
    //            audioClip.SetData(audioData, 0);
    //            //audioSource.clip = audioClip;
    //            //audioSource.Play();
    //            if (Manager.instance.isRobobarSpeaking)
    //                StartCoroutine(GetAudioClipCo(audioClip));

    //            Debug.Log("Speech synthesis succeeded!");
    //        }
    //        else if (result.Reason == ResultReason.Canceled)
    //        {
    //            var cancellation = SpeechSynthesisCancellationDetails.FromResult(result);
    //            string newMessage = $"CANCELED:\nReason=[{cancellation.Reason}]\nErrorDetails=[{cancellation.ErrorDetails}]\nDid you update the subscription info?";
    //            Debug.Log(newMessage);
    //        }
    //    }
    //}

}
