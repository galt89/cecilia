﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;
using UnityEngine.Networking;

public class CheckInternet : MonoBehaviour
{
   
    public string localIpv4;
    public string publicIpv4;
    WaitForSeconds waitSec = new WaitForSeconds(1);
    string googleIPAddress = "216.58.201.228";

    string ipfyIP = "109.186.215.13";
    //https://api64.ipify.org?format=json
    //https://api.ipify.org?format=json
    string ipfyAddress = "https://api64.ipify.org?format=json";

    public bool isCheckingNetwork;
    bool isSendingRequest;
    public bool isNetworktError;
    WaitForSeconds waitCheckTime = new WaitForSeconds(0.025f);
    int maxCheckTime = 20;
    int currentCheckTime;
    public bool isCheckingStatus;

    


    private void Start()
    {
        currentCheckTime = maxCheckTime;
    }

    public void Check()
    {
        if (!isCheckingNetwork)
        {
            isCheckingNetwork = true;
            isNetworktError = false;
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                isNetworktError = true;
                DebugLogTxt.instance.Log("Connection Error - NetworkReachability.NotReachable");
                Debug.Log("Connection Error - NetworkReachability.NotReachable");
                Manager.instance.errorLogger.AddLog("Network error: NetworkReachability.NotReachable");
                Manager.instance.uiManager.ShowNetworkConnectionIssuesTxt();
                Manager.instance.uiManager.barmanTextUI.text = "Please check your internet connection.";
                Manager.instance.ResetEverything(true);
                isCheckingNetwork = false;
                return;
            }
            currentCheckTime = maxCheckTime;
            StartCoroutine(CheckInternetSpeed());
            CheckIP();
        }
    }


    IEnumerator CheckInternetSpeed()
    {
        isNetworktError = false;
        //Debug.Log("checking internet..");
        DebugLogTxt.instance.Log("checking internet..");
        isSendingRequest = true;
        StartCoroutine(CheckResults());
        UnityWebRequest request = new UnityWebRequest(googleIPAddress);
        yield return request.SendWebRequest();

        if (request.error != null)
        {
            isNetworktError = true;
            DebugLogTxt.instance.Log("Connection Error - " + request.error, true);
            Debug.Log("Connection Error - " + request.error);
            Manager.instance.errorLogger.AddLog("Network: Connection Error - " + request.error);
            Manager.instance.uiManager.ShowNetworkConnectionIssuesTxt();
            Manager.instance.uiManager.barmanTextUI.text = "Please check your internet connection.";
            Manager.instance.ResetEverything(true);
        }
        isSendingRequest = false;
    }


    
    IEnumerator CheckResults()
    {
        float requestStartTime = Time.time;
        while (isSendingRequest)
        {
            yield return waitCheckTime;
            if (Time.time - requestStartTime > 5)
            {
                isNetworktError = true;
                DebugLogTxt.instance.Log("Connection Error - request timeout - check your connection.");
                Debug.Log("Connection Error - request timeout - check your connection.");
                Manager.instance.errorLogger.AddLog("Network: Connection Error - request timeout - check your connection.");
                Manager.instance.uiManager.ShowNetworkConnectionIssuesTxt();
                Manager.instance.uiManager.barmanTextUI.text = "Please check your internet connection.";
                Manager.instance.ResetEverything(true);
                isCheckingNetwork = false;
                isSendingRequest = false;
                //StopAllCoroutines();
                yield break;
            }
        }
        float requestTime = Time.time - requestStartTime;

        if (!isNetworktError)
        {
            DebugLogTxt.instance.Log("Connection check - request time: " + requestTime + " checking ping...");
            Debug.Log("Connection check - request time: " + requestTime + " checking ping...");

            UnityEngine.Ping ping = new UnityEngine.Ping(googleIPAddress);
            float waitTime = 0;
            while (ping.isDone == false && waitTime < 10)
            {
                yield return null;
                waitTime += Time.deltaTime;
            }

            if (ping.isDone)
            {
                DebugLogTxt.instance.Log("CheckInternet: internet available. ping = " + ping.time, true);
                Debug.Log("CheckInternet: internet available. ping = " + ping.time);
                //send all offline errors:
                Manager.instance.firestoreManager.SendOfflineLogs();
            }
            else
            {
                isNetworktError = true;
                DebugLogTxt.instance.Log("CheckInternet: internet NOT available. ping = " + ping.time, true);
                Debug.Log("CheckInternet: internet NOT available. ping = " + ping.time);
                Manager.instance.errorLogger.AddLog("Network: not available - ping = " + ping.time);
                Manager.instance.uiManager.ShowNetworkConnectionIssuesTxt();
                Manager.instance.uiManager.barmanTextUI.text = "Please check your internet connection.";
                Manager.instance.ResetEverything(true);
            }
        }

        isCheckingNetwork = false;
    }


    IEnumerator CheckInternetCoroutine()
    {
        while (isCheckingStatus)
        {
            while (Manager.instance.withUser)
            {
                currentCheckTime = maxCheckTime;
                yield return waitSec;
            }
                
            while (currentCheckTime > 0)
            {
                yield return waitSec;
                currentCheckTime--;
            }
            yield return waitSec;

            if (Manager.instance.isTrueRecording && !Manager.instance.withUser)
                Check();
        }
    }


    public void CheckIP()
    {
        localIpv4 = IPManager.GetIP(ADDRESSFAM.IPv4);
        StartCoroutine(GetIPAddress());
    }


   

    IEnumerator GetIPAddress()
    {
        //"http://checkip.dyndns.org"
        UnityWebRequest request = UnityWebRequest.Get(ipfyAddress);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError(request.error);
            publicIpv4 = request.error;
        }
        else
        {
            string result = request.downloadHandler.text;

            IPResult iPResult;
            iPResult = JsonUtility.FromJson<IPResult>(result);

            //Debug.Log("Public IP: " + iPResult.ip);
            publicIpv4 = iPResult.ip;
        }
    }


    public void StartListenningToInternet()
    {
        isCheckingStatus = true;
        StartCoroutine(CheckInternetCoroutine());
    }

    public void StopListenningToInternet()
    {
        isCheckingStatus = false;
    }

}

public class IPResult
{
    public string ip;
}
