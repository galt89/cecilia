﻿using OpenCVForUnity.UnityUtils.Helper;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class SettingsScreenHandler : MonoBehaviour {

	
	public ActivationCommandUiItem[] activationCommandUiItems;

	public GameObject settingsScreen;

	//The UI elements prefabs for creating the menu...
	public GameObject ActivationCommandUiItemPrefab;
	public GameObject ConversationCommandUiItemPrefab;
	public GameObject GenericResponseCommandUiItemPrefab;

	//Activation settings variables:
	public GameObject activationCommandsWindow;
	public Transform activationCommandsParent;
	RectTransform activationCommandsParentRect;
	public Button activationSaveBtn;
	public bool isNeedToSaveActivationsList;

	//Conversation settings variables:
	public GameObject conversationCommandsWindow;
	public Transform conversationCommandsParent;
	RectTransform conversationCommandsParentRect;
	public Button conversationSaveBtn;
	public bool isNeedToSaveConversationList;

	public List<ConversationCommandUiItem> conversationCommandUiItems;
	
	//The speech input text for testing the speech service from the settings screen:
	public Text settingsInputTxtUI;

    public List<string> animationsNames = new List<string>();

	//Toggleable features:
	public Toggle phraseListToggle;
	public Toggle faceRecognitionToggle;
	public Toggle voiceActivationToggle;

	//public InputField ceciliaIDInputField; //not needed anymore

	public GameObject IDScannerSettingsScrn;

	string[] keywordsSeperator = new string[] { ", ", ",", " ,", " , " };

	//Speech service test:
	public Text recordBtnText;
	WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
	bool isTestRecording;


	private void Awake()
	{
		activationCommandsParentRect = activationCommandsParent.GetComponent<RectTransform>();
		conversationCommandsParentRect = conversationCommandsParent.GetComponent<RectTransform>();
	}

    private void Start()
    {
		if(PlayerPrefs.GetInt("phrase_list_on") == 1)
        {
			phraseListToggle.isOn = true;
			Manager.instance.voiceDictationManager.usePhraseList = true;
        }
        else
        {
			phraseListToggle.isOn = false;
			Manager.instance.voiceDictationManager.usePhraseList = false;
		}

		GetCameraDevices();

		//ceciliaIDInputField.text = PlayerPrefs.GetString("cecilia_id");// not needed anymore 

		faceRecognitionToggle.isOn = PlayerPrefs.GetInt("face_recognition_toggle") == 1 ? true : false;
		voiceActivationToggle.isOn = PlayerPrefs.GetInt("voice_activation_toggle") == 1 ? true : false;
	}


    public void CreateCommandItems () {

        animationsNames.Add("non");
        
		foreach (string animName in Manager.instance.animationsManager.commandStates)
		{
			animationsNames.Add(animName);
		}
		CreateActivationCommandItems();
		CreateConversationCommandItems();

		activationSaveBtn.interactable = false;
		isNeedToSaveActivationsList = false;
		conversationSaveBtn.interactable = false;
		isNeedToSaveConversationList = false;

		Manager.instance.uiManager.settingsScrn.SetActive(false);

		Manager.instance.touchScreenUIManager.StartSetDrinksBtns();
	}

	



	// Activation Commands menu:
	public void CreateActivationCommandItems()
	{
		//Debug.Log("Attempting to create activation command items");
		if (Manager.instance.activationSettings == null)
		{
			Manager.instance.activationSettings = new ActivationSettings();
			Manager.instance.activationSettings.activationCommands = new ActivationCommand[0];
		}
		
		ActivationSettings curActivationSettings = Manager.instance.activationSettings;
		for (int i = 0; i < activationCommandUiItems.Length; i++)
		{
			activationCommandUiItems[i].GatherResponses();
			if(activationCommandUiItems[i].responsesItems != null)
            {
				foreach (GenericResponseUiItem gnritm in activationCommandUiItems[i].responsesItems)
				{
					Destroy(gnritm.gameObject);
				}
				activationCommandUiItems[i].responsesItems = null;
			}
			
			if (i < curActivationSettings.activationCommands.Length)
			{
				activationCommandUiItems[i].ActivateNewCommandItem(
					curActivationSettings.activationCommands[i].responses,
					curActivationSettings.activationCommands[i].anims1,
					curActivationSettings.activationCommands[i].anims2);
			}
			else
			{
				activationCommandUiItems[i].ActivateNewCommandItem();
			}
		}

		Manager.instance.cupActivationCommand = activationCommandUiItems[0].command;
		Manager.instance.voiceActivationNoCupCommand = activationCommandUiItems[1].command;
		Manager.instance.faceFirstTimeCommand = activationCommandUiItems[2].command;
		Manager.instance.faceFirstTimeNoCupCommand = activationCommandUiItems[3].command;
		Manager.instance.faceSecondTimeCommand = activationCommandUiItems[4].command;
		Manager.instance.faceSecondTimeNoCupCommand = activationCommandUiItems[5].command;
		Manager.instance.firstTimeoutCommand = activationCommandUiItems[6].command;
		Manager.instance.endConversationCommand = activationCommandUiItems[7].command;
		Manager.instance.endConversationNoCupCommand = activationCommandUiItems[8].command;
		Manager.instance.saidThanksCommand = activationCommandUiItems[9].command;
		Manager.instance.notUnderstandCommand = activationCommandUiItems[10].command;
		Manager.instance.putCupInCommand = activationCommandUiItems[11].command;
		Manager.instance.takeCupOutCommand = activationCommandUiItems[12].command;
		Manager.instance.secondTimeoutCommand = activationCommandUiItems[13].command;
		Manager.instance.secondTimeoutNoCupCommand = activationCommandUiItems[14].command;

		activationSaveBtn.interactable = false;
		isNeedToSaveActivationsList = false;

		CloseActivationCommandWindow();
	}

	
	public void SaveActivationSettings()
	{
		List<ActivationCommand> availableActivationCommands = new List<ActivationCommand>();
		List<string> currentResponsesList = new List<string>();
		List<string> currentAnims1List = new List<string>();
		List<string> currentAnims2List = new List<string>();
		
		for (int i = 0; i < activationCommandUiItems.Length; i++)
		{
			activationCommandUiItems[i].GatherResponses();
			if (activationCommandUiItems[i].responsesItems != null && activationCommandUiItems[i].responsesItems.Length > 0)
			{
				currentResponsesList.Clear();
				currentAnims1List.Clear();
				currentAnims2List.Clear();
				foreach (GenericResponseUiItem response in activationCommandUiItems[i].responsesItems)
				{
					currentResponsesList.Add(response.inpuField.text.Trim());
					currentAnims1List.Add(response.anims1.options[response.anims1.value].text);
					currentAnims2List.Add(response.anims2.options[response.anims2.value].text);
				}

				activationCommandUiItems[i].SetCommandItem(currentResponsesList.ToArray(), currentAnims1List.ToArray(), currentAnims2List.ToArray());
			}

			availableActivationCommands.Add(activationCommandUiItems[i].command);
		}

		Manager.instance.jsonManager.SetNewActivationData(availableActivationCommands.ToArray());

		activationSaveBtn.interactable = false;
		isNeedToSaveActivationsList = false;
	}


	public void OpenActivationCommandWindow()
	{
		activationCommandsWindow.SetActive(true);
	}

	public void CloseActivationCommandWindow()
	{
        if(isNeedToSaveActivationsList){
			Manager.instance.uiManager.ShowPopup("Close without saving?", CreateActivationCommandItems);
		}
        else
        {
			activationCommandsWindow.SetActive(false);
		}
	}



	// Conversation cammands window:
	public void CreateConversationCommandItems()
	{
		//Debug.Log("Attempting to create conversation command items");
		foreach (ConversationCommandUiItem cmd in conversationCommandUiItems)
        {
			cmd.gatherResponses();
            if (cmd.responsesItems != null && cmd.responsesItems.Length > 0)
            {
				foreach (GenericResponseUiItem grui in cmd.responsesItems)
					Destroy(grui.gameObject);
            }
            cmd.responsesItems = null;
        }
		foreach (Transform conUIItem in conversationCommandsParent)
        {
			Destroy(conUIItem.gameObject);
		}
		conversationCommandUiItems.Clear();

		if (Manager.instance.conversationSettings == null)
			Manager.instance.conversationSettings = new ConversationSettings();
		if (Manager.instance.conversationSettings.conversationCommands != null && Manager.instance.conversationSettings.conversationCommands.Length > 0)
		{
			foreach (ConversationCommand command in Manager.instance.conversationSettings.conversationCommands)
			{
				CreateConversationCommandItem(command.keywords, command.responses, command.anims1, command.anims2, command.drinkName, command.output, command.isDrink);
			}
		}

		CreateConversationCommandItem();

		conversationSaveBtn.interactable = false;
		isNeedToSaveConversationList = false;

		CloseConversationCommandWindow();
	}

	public void CreateConversationCommandItem(string[] keywords = null, string[] responses = null, string[] anims1 = null, string[] anims2 = null, string drinkName = "", string output = "", int isDrink = 0)
	{
		GameObject newConversationCommand = (GameObject)Instantiate(ConversationCommandUiItemPrefab, conversationCommandsParent);
		ConversationCommandUiItem currentConversationCommandUiItem = newConversationCommand.GetComponent<ConversationCommandUiItem>();
		currentConversationCommandUiItem.ActivateNewCommandItem(keywords, responses, anims1, anims2, drinkName, output);
	}



	public void SaveConversationSettings()
	{
		List<ConversationCommand> availableConversationCommands = new List<ConversationCommand>();
		string[] currentCommandKeywords;
		List<string> currentResponsesList = new List<string>();
		List<string> currentAnims1List = new List<string>();
		List<string> currentAnims2List = new List<string>();
		string currentCommandOutput;
		string currentCommandDrinkName;
		//string currectCommandDrinkID;
		int currentCommandIsDrink;
		List<ConversationCommandUiItem> conversationCommandUiItemsToRemove = new List<ConversationCommandUiItem>();
		
		foreach (ConversationCommandUiItem cmnd in conversationCommandUiItems)
		{
			currentCommandKeywords = cmnd.keywordsTxtField.text.Split(keywordsSeperator, System.StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < currentCommandKeywords.Length; i++)
            {
                currentCommandKeywords[i] = currentCommandKeywords[i].Trim();
            }
            currentResponsesList.Clear();
			currentAnims1List.Clear();
			currentAnims2List.Clear();
			cmnd.gatherResponses();
			if (cmnd.responsesItems != null && cmnd.responsesItems.Length > 0)
			{
				foreach (GenericResponseUiItem response in cmnd.responsesItems)
				{
					currentResponsesList.Add(response.inpuField.text.Trim());
					currentAnims1List.Add(response.anims1.options[response.anims1.value].text);
					currentAnims2List.Add(response.anims2.options[response.anims2.value].text);
				}
			}

			currentCommandOutput = cmnd.outputTxtField.text;
			currentCommandDrinkName = cmnd.drinkNameField.text;
		//	currectCommandDrinkID = cmnd.DrinkIDField.text;

			if (!string.IsNullOrEmpty(currentCommandOutput) && !string.IsNullOrEmpty(currentCommandDrinkName))
			{
				currentCommandIsDrink = 1;
			}
			else
			{
				currentCommandIsDrink = 0;
			}


			if ((currentCommandKeywords == null || currentCommandKeywords.Length == 0)&&(cmnd.responsesItems == null || cmnd.responsesItems.Length == 0))
			{
				conversationCommandUiItemsToRemove.Add(cmnd);
			}
			else
			{
				cmnd.SetCommandItem(currentCommandKeywords, currentResponsesList.ToArray(), currentAnims1List.ToArray(), currentAnims2List.ToArray(), currentCommandDrinkName, currentCommandOutput, currentCommandIsDrink);
				availableConversationCommands.Add(cmnd.command);
			}
		}

		for (int i = 0; i < conversationCommandUiItemsToRemove.Count; i++)
		{
			conversationCommandUiItemsToRemove[i].RemoveCommandItem();
		}

		Manager.instance.jsonManager.SetNewConversationData(availableConversationCommands.ToArray());

		conversationSaveBtn.interactable = false;
		isNeedToSaveConversationList = false;

		Manager.instance.touchScreenUIManager.StartSetDrinksBtns();

		Manager.instance.voiceDictationManager.DisposeRecording();
	}


	
	public void OpenConversationCommandWindow()
	{
		conversationCommandsWindow.SetActive(true);
	}

	public void CloseConversationCommandWindow()
	{
		if (isNeedToSaveConversationList)
		{
			Manager.instance.uiManager.ShowPopup("Close without saving?", CreateConversationCommandItems);
        }
        else
        {
			conversationCommandsWindow.SetActive(false);
		}
	}




	public void RefreshLayoutGroup()
	{
		StartCoroutine(RefreshLayoutGroupCo());
	}

	IEnumerator RefreshLayoutGroupCo()
    {
		yield return new WaitForEndOfFrame();
		
		if (activationCommandsParent.gameObject.activeSelf)
		{
			if (activationCommandsParent.GetComponentsInChildren<RectTransform>().Length > 0)
            {
				LayoutRebuilder.ForceRebuildLayoutImmediate(activationCommandsParentRect);
				activationCommandsParent.GetComponent<VerticalLayoutGroup>().SetLayoutVertical();
			}
		}
		if (conversationCommandsParent.gameObject.activeSelf)
		{
			if (conversationCommandsParent.GetComponentsInChildren<RectTransform>().Length > 0)
            {
				LayoutRebuilder.ForceRebuildLayoutImmediate(conversationCommandsParentRect);
				conversationCommandsParent.GetComponent<VerticalLayoutGroup>().SetLayoutVertical();
			}
		}
	}


	// Speech service test:

	public void RecordingToggle(){
        Manager.instance.isTrueRecording = false;
        isTestRecording = !isTestRecording;
        if (isTestRecording)
        {
			Manager.instance.voiceDictationManager.StartRecording("RecordingToggle() - TestRecording");
			StartCoroutine(WaitForStartRecording());
        } 
        else
        {
			Manager.instance.voiceDictationManager.StopRecording(false, false, "RecordingToggle() - TestRecording");
            StartCoroutine(WaitForStopRecording());
        }   
	}

	IEnumerator WaitForStartRecording(){
        while (!Manager.instance.voiceDictationManager.IsRecording)
        {
            yield return waitForEndOfFrame;
        }
        recordBtnText.text = "Stop Recording";
        settingsInputTxtUI.text = "Recording inputs";
	}

    IEnumerator WaitForStopRecording()
    {
        while (Manager.instance.voiceDictationManager.IsRecording)
        {
            yield return waitForEndOfFrame;
        }
        recordBtnText.text = "Start Recording";
    }


    public void StopRecording(){
		//VoiceDictationManager.instance.StopRecording();
		Manager.instance.voiceDictationManager.StopRecording(false, false, "Settings - StopRecording()");
		isTestRecording = false;
        StartCoroutine(WaitForStopRecording());
	}



	public void CloseSettingsScrn(){
		StopRecording ();
		settingsScreen.SetActive (false);
		Manager.instance.settingsScrnOn = false;
	}



	public Dropdown cameraDevicesDropdown;
	void GetCameraDevices()
    {
		var devices = WebCamTexture.devices;
		List<string> cameraDevicesNames = new List<string>();
		for (int i = 0; i < devices.Length; i++){
			cameraDevicesNames.Add(devices[i].name);
		}
		if(cameraDevicesNames.Count > 0)
        {
			cameraDevicesDropdown.AddOptions(cameraDevicesNames);
		}
	}


	public WebCamTextureToMatHelper webCamTextureToMatHelper;
	public void ChooseCameraDevice()
    {
		webCamTextureToMatHelper.requestedDeviceName = cameraDevicesDropdown.value.ToString();
	}


	public void PhraseListToggle(bool toggle)
    {
		if (toggle)
        {
			Manager.instance.voiceDictationManager.usePhraseList = true;
			PlayerPrefs.SetInt("phrase_list_on", 1);
		}
        else
        {
			Manager.instance.voiceDictationManager.usePhraseList = false;
			PlayerPrefs.SetInt("phrase_list_on", 0);
		}
		Manager.instance.voiceDictationManager.DisposeRecording();
	}




	public void SetFaceRecognitionToggle(bool isOn)
	{
		Manager.instance.isFaceRecognitionOn = isOn;
		PlayerPrefs.SetInt("face_recognition_toggle", isOn ? 1 : 0);
	}

	public void SetVoiceActivationToggle(bool isOn)
	{
		Manager.instance.isVoiceActivationEnabled = isOn;
		PlayerPrefs.SetInt("voice_activation_toggle", isOn ? 1 : 0);
	}

	public void OpenIDSettingsScrn()
	{
		IDScannerSettingsScrn.SetActive(true);
	}

	public void CloseIDSettingsScrn()
	{
		IDScannerSettingsScrn.SetActive(false);
	}


	
}
