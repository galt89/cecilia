﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicIndicationHandler : MonoBehaviour {

	//RectTransform rectTransform;

	public MicInput micInput;

	bool _isOn = false;

	Vector2 indicationScale;

    public float micLevel;

    public float micScaleMultiplier = 100;

    public float SpeakLoudToMic;

    


    //	void Awake () {
    //		rectTransform = GetComponent<RectTransform> ();
    //	}

    void OnEnable()
	{
		_isOn = true;
	}

	void OnDisable()
	{
		_isOn = false;
	}

    void Update() //ofek change
    {
       /* if(micLevel >= micInput.MicrophoneSensivity.value)
        {
            MicSoundFilter();
        }*/
    }
	
	
	void FixedUpdate () {
		if (_isOn) {
            micLevel = micInput.LevelMax();
            if(micLevel == -1)
            {
                Manager.instance.settingsScrnHandler.settingsInputTxtUI.text = Manager.instance.uiManager.barmanTextUI.text = "Microphone is not initialized - Restart recording";
            }
            else
            {
                //micLevel = micLevel * 100;
                indicationScale.x = 1 + micLevel * micScaleMultiplier;
                indicationScale.y = 1 + micLevel * micScaleMultiplier;
                //transform.localScale = indicationScale;
                transform.localScale = Vector2.Lerp(transform.localScale, indicationScale, 1);
            }
		}
	}

 /*   void MicSoundFilter() //ofekchange
    {
        SpeakLoudToMic = micLevel;
    }
 */
}
