﻿#if !(PLATFORM_LUMIN && !UNITY_EDITOR)

using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ObjdetectModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.UnityUtils;
using OpenCVForUnity.UnityUtils.Helper;
using UnityEngine.Events;
using UnityEngine.UI;

namespace OpenCVForUnityExample
{
    /// <summary>
    /// Face Detection WebCamTexture Example
    /// An example of detecting human face in a image of WebCamTexture using the CascadeClassifier class.
    /// http://docs.opencv.org/3.2.0/db/d28/tutorial_cascade_classifier.html
    /// </summary>
    [RequireComponent (typeof(WebCamTextureToMatHelper))]
    public class FaceDetectionWebCamTextureExample : MonoBehaviour
    {
        /// <summary>
        /// The gray mat.
        /// </summary>
        Mat grayMat;

        /// <summary>
        /// The texture.
        /// </summary>
        Texture2D texture;

        /// <summary>
        /// The cascade.
        /// </summary>
        CascadeClassifier cascade;

        /// <summary>
        /// The faces.
        /// </summary>
        MatOfRect faces;

        /// <summary>
        /// The webcam texture to mat helper.
        /// </summary>
        WebCamTextureToMatHelper webCamTextureToMatHelper;

        /// <summary>
        /// The FPS monitor.
        /// </summary>
        FpsMonitor fpsMonitor;

        /// <summary>
        /// LBP_CASCADE_FILENAME
        /// </summary>
        protected static readonly string LBP_CASCADE_FILENAME = "lbpcascade_frontalface.xml";

#if UNITY_WEBGL && !UNITY_EDITOR
        IEnumerator getFilePath_Coroutine;
#endif

        WaitForSeconds waitCameraTime = new WaitForSeconds(0.1f);
        WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
        //bool displayVideo;
        public RawImage videoRawImage;

        // Use this for initialization
        void Start ()
        {
            fpsMonitor = GetComponent<FpsMonitor> ();

            webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper> ();

            #if UNITY_WEBGL && !UNITY_EDITOR
            getFilePath_Coroutine = Utils.getFilePathAsync (LBP_CASCADE_FILENAME, (result) => {
                getFilePath_Coroutine = null;

                cascade = new CascadeClassifier ();
                cascade.load (result);
                if (cascade.empty ()) {
                    Debug.LogError ("cascade file is not loaded. Please copy from “OpenCVForUnity/StreamingAssets/” to “Assets/StreamingAssets/” folder. ");
                }

                webCamTextureToMatHelper.Initialize ();
            });
            StartCoroutine (getFilePath_Coroutine);
            #else
            cascade = new CascadeClassifier ();
            cascade.load (Utils.getFilePath (LBP_CASCADE_FILENAME));
//            cascade.load (Utils.getFilePath ("haarcascade_frontalface_alt.xml"));
            if (cascade.empty ()) {
                Debug.LogError ("cascade file is not loaded. Please copy from “OpenCVForUnity/StreamingAssets/” to “Assets/StreamingAssets/” folder. ");
            }

            #if UNITY_ANDROID && !UNITY_EDITOR
            // Avoids the front camera low light issue that occurs in only some Android devices (e.g. Google Pixel, Pixel2).
            webCamTextureToMatHelper.avoidAndroidFrontCameraLowLightIssue = true;
            #endif
            webCamTextureToMatHelper.Initialize ();

            #endif
        }

        /// <summary>
        /// Raises the web cam texture to mat helper initialized event.
        /// </summary>
        public void OnWebCamTextureToMatHelperInitialized ()
        {
            Debug.Log ("OnWebCamTextureToMatHelperInitialized");
            
            Mat webCamTextureMat = webCamTextureToMatHelper.GetMat ();

            texture = new Texture2D (webCamTextureMat.cols (), webCamTextureMat.rows (), TextureFormat.RGBA32, false);
            Utils.fastMatToTexture2D(webCamTextureMat, texture);

            //if (gameObject.GetComponent<Renderer>() != null && gameObject.GetComponent<Renderer>().enabled)
            //{
            //    gameObject.GetComponent<Renderer>().material.mainTexture = texture;
            //    gameObject.transform.localScale = new Vector3(webCamTextureMat.cols(), webCamTextureMat.rows(), 1);
                
            //    float width = webCamTextureMat.width();
            //    float height = webCamTextureMat.height();

            //    float widthScale = (float)Screen.width / width;
            //    float heightScale = (float)Screen.height / height;
            //    if (widthScale < heightScale)
            //    {
            //        Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
            //    }
            //    else
            //    {
            //        Camera.main.orthographicSize = height / 2;
            //    }
            //    displayVideo = true;
            //}
            //else
            //{
            //    webCamTextureToMatHelper.Pause();
            //}

            webCamTextureToMatHelper.Pause();
            if (fpsMonitor != null) {
                fpsMonitor.Add ("width", webCamTextureMat.width ().ToString ());
                fpsMonitor.Add ("height", webCamTextureMat.height ().ToString ());
                fpsMonitor.Add ("orientation", Screen.orientation.ToString ());
            }
            DebugLogTxt.instance.Log("width: " + webCamTextureMat.width().ToString() + ", height: " + webCamTextureMat.height().ToString(),true);
            grayMat = new Mat (webCamTextureMat.rows (), webCamTextureMat.cols (), CvType.CV_8UC1);

            faces = new MatOfRect ();

            videoRawImage.texture = texture;
        }

        /// <summary>
        /// Raises the web cam texture to mat helper disposed event.
        /// </summary>
        public void OnWebCamTextureToMatHelperDisposed ()
        {
            Debug.Log ("OnWebCamTextureToMatHelperDisposed");

            if (grayMat != null)
                grayMat.Dispose ();

            if (texture != null) {
                Texture2D.Destroy (texture);
                texture = null;
            }

            if (faces != null)
                faces.Dispose ();
        }

        /// <summary>
        /// Raises the web cam texture to mat helper error occurred event.
        /// </summary>
        /// <param name="errorCode">Error code.</param>
        public void OnWebCamTextureToMatHelperErrorOccurred (WebCamTextureToMatHelper.ErrorCode errorCode)
        {
            Debug.Log ("OnWebCamTextureToMatHelperErrorOccurred " + errorCode);
        }

        // Update is called once per frame
        //void Update ()
        //{
        //    if (displayVideo)
        //    {
        //        if (webCamTextureToMatHelper.IsPlaying() && webCamTextureToMatHelper.DidUpdateThisFrame())
        //        {

        //            Mat rgbaMat = webCamTextureToMatHelper.GetMat();

        //            Imgproc.cvtColor(rgbaMat, grayMat, Imgproc.COLOR_RGBA2GRAY);
        //            Imgproc.equalizeHist(grayMat, grayMat);


        //            if (cascade != null)
        //                cascade.detectMultiScale(grayMat, faces, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
        //                    new Size(grayMat.cols() * 0.2, grayMat.rows() * 0.2), new Size());


        //            OpenCVForUnity.CoreModule.Rect[] rects = faces.toArray();
        //            for (int i = 0; i < rects.Length; i++)
        //            {
        //                //              Debug.Log ("detect faces " + rects [i]);

        //                Imgproc.rectangle(rgbaMat, new Point(rects[i].x, rects[i].y), new Point(rects[i].x + rects[i].width, rects[i].y + rects[i].height), new Scalar(255, 0, 0, 255), 2);
        //            }

        //            //                Imgproc.putText (rgbaMat, "W:" + rgbaMat.width () + " H:" + rgbaMat.height () + " SO:" + Screen.orientation, new Point (5, rgbaMat.rows () - 10), Imgproc.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar (255, 255, 255, 255), 2, Imgproc.LINE_AA, false);

        //            Utils.fastMatToTexture2D(rgbaMat, texture);
        //        }
        //    }
        //}



        public void TakeSnapShot(UnityAction onSnapshotSeccess, UnityAction onSnapshotFailed)
        {
            StartCoroutine(TakeSnapShotCo(onSnapshotSeccess, onSnapshotFailed));
        }

        int numOfTrys = 0;
        
        IEnumerator TakeSnapShotCo(UnityAction onSnapshotSeccess, UnityAction onSnapshotFailed)
        {
            webCamTextureToMatHelper.Play();
            yield return waitCameraTime;
            float startTime = Time.time;
            while (!webCamTextureToMatHelper.IsPlaying() || !webCamTextureToMatHelper.DidUpdateThisFrame())
            {
                //DebugLogTxt.instance.Log("Camera isn't ready - waiting... IsPlaying() = " + webCamTextureToMatHelper.IsPlaying() + ", DidUpdateThisFrame() = " + webCamTextureToMatHelper.DidUpdateThisFrame());
                //yield return waitCameraTime;
                if ((Time.time - startTime) > 3)
                {
                    webCamTextureToMatHelper.Pause();
                    onSnapshotFailed.Invoke();
                    Debug.LogError("webCamTextureToMatHelper.IsPlaying() = false");
                    DebugLogTxt.instance.Log("webCamTextureToMatHelper.IsPlaying() = false",true);
                    yield break;
                }
                yield return null;
            }
            //yield return waitForEndOfFrame;
            
            //if (webCamTextureToMatHelper.IsPlaying())
            //{
            OpenCVForUnity.CoreModule.Rect[] rects = new OpenCVForUnity.CoreModule.Rect[1];
            Mat rgbaMat = new Mat();
            for (int i = 0; i < 10; i++)
            {
                rgbaMat = webCamTextureToMatHelper.GetMat();
                Imgproc.cvtColor(rgbaMat, grayMat, Imgproc.COLOR_RGBA2GRAY);
                Imgproc.equalizeHist(grayMat, grayMat);

                if (cascade != null)
                    cascade.detectMultiScale(grayMat, faces, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
                        new Size(grayMat.cols() * 0.2, grayMat.rows() * 0.2), new Size());

                rects = faces.toArray();
                numOfTrys = i + 1;
                if (rects.Length > 0)
                    break;

                yield return waitCameraTime;
            }
            //for (int y = 0; y < rects.Length; y++)
            //{
            //    Imgproc.rectangle(rgbaMat, new Point(rects[y].x, rects[y].y), new Point(rects[y].x + rects[y].width, rects[y].y + rects[y].height), new Scalar(255, 0, 0, 255), 2);
            //}
            Utils.fastMatToTexture2D(rgbaMat, texture);
            //yield return waitForEndOfFrame;
            //yield return waitCameraTime;
            //}

            if (rects.Length > 0)
            {
                
                GetFaceTexture(rects, onSnapshotSeccess, onSnapshotFailed);
            }    
                else
                {
                    //webCamTextureToMatHelper.Pause();
                    onSnapshotFailed.Invoke();
                    //Debug.Log("No faces detected...");
                    DebugLogTxt.instance.Log("No faces detected...");
                }
                webCamTextureToMatHelper.Pause();
            //}
            //else
            //{
            //    webCamTextureToMatHelper.Pause();
            //    onSnapshotFailed.Invoke();
            //    Debug.LogError("webCamTextureToMatHelper.IsPlaying() = false");
            //    //DebugLogTxt.instance.Log("webCamTextureToMatHelper.IsPlaying() = false");
            //}
        }


        public int minimumFaceSize = 150;
        Color[] pix;
        int newPosX;
        int newPosY;
        int newScaleX;
        int newScaleY;
        int requestedHeight;
        public Texture2D newFaceImage;
        void GetFaceTexture(OpenCVForUnity.CoreModule.Rect[] rects, UnityAction onSnapshotSeccess, UnityAction onSnapshotFailed)
        {
            requestedHeight = texture.height;
            foreach (OpenCVForUnity.CoreModule.Rect imageRect in rects)
            {
                if (imageRect.width >= minimumFaceSize)
                {
                    newScaleX = imageRect.width;
                    newScaleY = imageRect.height;

                    newPosX = imageRect.x;
                    newPosY = (requestedHeight - imageRect.y) - newScaleY;

                    pix = texture.GetPixels(newPosX, newPosY, newScaleX, newScaleY);

                    newFaceImage = new Texture2D(newScaleX, newScaleY, TextureFormat.RGB24, false);
                    newFaceImage.SetPixels(pix);
                    newFaceImage.Apply();
                    if (webCamTextureToMatHelper.IsPlaying())
                        webCamTextureToMatHelper.Pause();
                    //Debug.Log("on Snapshot Seccess!");
                    DebugLogTxt.instance.Log("on Snapshot Seccess! size: " + newScaleX + ", " + numOfTrys);
                    onSnapshotSeccess.Invoke();
                    return;
                }
            }
            if(webCamTextureToMatHelper.IsPlaying())
                webCamTextureToMatHelper.Pause();
            onSnapshotFailed.Invoke();
            Debug.Log("No faces in the right size");
            DebugLogTxt.instance.Log("face is too far");
        }



        /// <summary>
        /// Raises the destroy event.
        /// </summary>
        void OnDestroy ()
        {
            webCamTextureToMatHelper.Dispose ();

            if (cascade != null)
                cascade.Dispose ();

            #if UNITY_WEBGL && !UNITY_EDITOR
            if (getFilePath_Coroutine != null) {
                StopCoroutine (getFilePath_Coroutine);
                ((IDisposable)getFilePath_Coroutine).Dispose ();
            }
            #endif
        }

        /// <summary>
        /// Raises the back button click event.
        /// </summary>
        public void OnBackButtonClick ()
        {
            SceneManager.LoadScene ("OpenCVForUnityExample");
        }

        /// <summary>
        /// Raises the play button click event.
        /// </summary>
        public void OnPlayButtonClick ()
        {
            webCamTextureToMatHelper.Play ();
        }

        /// <summary>
        /// Raises the pause button click event.
        /// </summary>
        public void OnPauseButtonClick ()
        {
            webCamTextureToMatHelper.Pause ();
        }

        /// <summary>
        /// Raises the stop button click event.
        /// </summary>
        public void OnStopButtonClick ()
        {
            webCamTextureToMatHelper.Stop ();
        }

        /// <summary>
        /// Raises the change camera button click event.
        /// </summary>
        public void OnChangeCameraButtonClick ()
        {
            webCamTextureToMatHelper.requestedIsFrontFacing = !webCamTextureToMatHelper.IsFrontFacing ();
        }
    }
}

#endif