using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMouseClick : MonoBehaviour
{
    public VoiceDictationManager VoiceDicationScript;

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0 || Input.GetMouseButton(0))
        {
            VoiceDicationScript.CancelTimeout();
        }
    }
}
