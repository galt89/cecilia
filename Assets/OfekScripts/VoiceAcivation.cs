﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Windows.Speech;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.WebCam;
using System.IO;

public class VoiceAcivation : MonoBehaviour
{
    public GameObject uiManager;
    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    public string[] CommandsKeys;

    // Start is called before the first frame update
    void Start()
    {
        Commands();
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
    }

    // Update is called once per frame
    void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        // if the keyword recognized is in our dictionary, call that Action.
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    void Commands() // All the commands
    {
        for (int i = 0; i < CommandsKeys.Length; i++)
        {
            keywords.Add(CommandsKeys[i], Hello);
            
        }
    }

    void Hello()
    {
        uiManager.GetComponent<TouchScreenUIManager>().OnScreenTap();
        //Call the function that start the session
    }
}
