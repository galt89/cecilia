﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.Threading;
using UnityEngine.UI;

public class Pumps : MonoBehaviour
{
   // SerialPort sp = new SerialPort("COM3", 9600);
    public GameObject PumpScreen;
    public InputField Input;
    public ArduinoHandler arduinoHandler;

    // Start is called before the first frame update
    void Start()
    {
       
    }
   
    // Update is called once per frame
    public void PumpsScreenActive()
    {
        PumpScreen.SetActive(true); //Unity will open the window pumps settings
    }

    public void PumpsScreenDisActive()
    {
        PumpScreen.SetActive(false); //Unity will close the window pumps settings
    }
  
    //All pumps number + liters
    public void Pump1()
    {
       Manager.instance.arduinoHandler.SendToArdu("1:" + Input.text);
    }
    public void Pump2()
    {
        Manager.instance.arduinoHandler.SendToArdu("2:" + Input.text);

    }
    public void Pump3()
    {
        Manager.instance.arduinoHandler.SendToArdu("3:" + Input.text);
    }
    public void Pump4()
    {
        Manager.instance.arduinoHandler.SendToArdu("4:" + Input.text);
    }
    public void Pump5()
    {
        Manager.instance.arduinoHandler.SendToArdu("5:" + Input.text);
    }
    public void Pump6()
    {
        Manager.instance.arduinoHandler.SendToArdu("6:" + Input.text);
    }
    public void Pump7()
    {
        Manager.instance.arduinoHandler.SendToArdu("7:" + Input.text);
    }
    public void Pump8()
    {
        Manager.instance.arduinoHandler.SendToArdu("8:" + Input.text);
    }
    public void Pump9()
    {
        Manager.instance.arduinoHandler.SendToArdu("9:" + Input.text);
    }
    public void Pump10()
    {
        Manager.instance.arduinoHandler.SendToArdu("10:" + Input.text);
    }
    public void Pump11()
    {
        Manager.instance.arduinoHandler.SendToArdu("11:" + Input.text);
    }
    public void Pump12()
    {
        Manager.instance.arduinoHandler.SendToArdu("12:" + Input.text);
    }
}
