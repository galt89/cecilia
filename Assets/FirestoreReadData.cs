using Firebase;
using Firebase.Extensions;
using Firebase.Firestore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class FirestoreReadData : MonoBehaviour
{

    FirebaseFirestore db;
    FirebaseApp app;
    public bool firebaseInitialized;
    [SerializeField]
    int counter;
    [SerializeField]
    bool ContainersChecked;

    [SerializeField]
    List<string> AlltheNumbersContainers = new List<string>();

    [SerializeField]
    public List<GameObject> DrinksChilldrenMenu = new List<GameObject>();
 
    [Serializable]
    public class AllContainers {
        public int n = 2;
        public int[] con0;
        public int[] con1;
        public int[] con2;
        public int[] con3;
        public int[] con4;
        public int[] con5;
        public int[] con6;
        public int[] con7;
        public int[] con8;
        public int[] con9;
        public int[] con10;
        public int[] con11;
    }

    public Manager manager;
 
    [SerializeField]
    public List<string> AllContainersSoldOut = new List<string>();

    public AllContainers containersList;
   
    async void Start()
    {
        
       // AllCocktailsOutput = manager.conversationSettings;
        containersList.con0 = new int[containersList.n];
        containersList.con1 = new int[containersList.n];
        containersList.con2 = new int[containersList.n];
        containersList.con3 = new int[containersList.n];
        containersList.con4 = new int[containersList.n];
        containersList.con5 = new int[containersList.n];
        containersList.con6 = new int[containersList.n];
        containersList.con7 = new int[containersList.n];
        containersList.con8 = new int[containersList.n];
        containersList.con9 = new int[containersList.n];
        containersList.con10 = new int[containersList.n];
        containersList.con11 = new int[containersList.n];


      //  GetDocumentObject();
      //GetDocumentProperty("containers");
    }
 



    async Task<bool> InitFirebase()
    {
        db = FirebaseFirestore.DefaultInstance;
        bool initialized = false;
        try
        {
            await Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;

                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    app = Firebase.FirebaseApp.DefaultInstance;
                    initialized = true;
                    Debug.Log("Firebase initialized.");
                }
                else
                {
                    UnityEngine.Debug.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                }
            });
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }

        return initialized;
    }

    public void buttonStartGetData() // When the user click on the start session button
    {
        ContainersChecked = false;
        GetDocumentObject();
    }




    async void GetDocumentObject()
    {

        foreach (GameObject DrinkButtonObj in FindObjectsOfType(typeof(GameObject)) as GameObject[])
        {
            if (DrinkButtonObj.name == "Drink Button(Clone)")
            {
                DrinksChilldrenMenu.Add(DrinkButtonObj);
                
               /* for (i = 0; i < DrinksChilldrenMenu.Count; i++)
                {
                    DrinksChilldrenMenu[i].GetComponent<Button>().interactable = false;
                    DrinksChilldrenMenu[i].GetComponent<Image>().color = new Color32(255, 255, 255, 107); // change the color image to grey     
                } */
            }
        }
        DrinksChilldrenMenu.Reverse();
        await Task.Delay(TimeSpan.FromSeconds(1));

        firebaseInitialized = await InitFirebase();

        if (!firebaseInitialized)
        {
            Debug.LogError("Firebase not initialized");
            return;
        }
        //START READ DATA FROM FIREBASE
        DocumentReference docRef = db.Collection("cecilia_devices").Document(manager.ceciliaID);
        docRef.Listen(snapshot =>
        {
            if (snapshot.Exists)
            {
                Debug.Log($"Document data for document: {snapshot.Id}");
                Dictionary<string, object> devices = snapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in devices)
                {
                    Debug.Log($"pair: {pair.Key} , {pair.Value} , {pair.Value.GetType()}");
                }
                //Convert the document snapshot to the matched class like this:
                CeciliaDevice ceciliaDevice = snapshot.ConvertTo<CeciliaDevice>();

                for (int i = 0; i < ceciliaDevice.containers.Length; i++)
                {
                    Debug.Log($"*** containers.Length = {ceciliaDevice.containers.Length}\n" +
                        $"containers[].container_number = {ceciliaDevice.containers[i].container_number}\n" +
                        $"containers[].currentVolume = {ceciliaDevice.containers[i].currentVolume}\n" +
                        $"containers[].name = {ceciliaDevice.containers[i].name}\n" +
                        $"containers[].redLine = {ceciliaDevice.containers[i].redLine}");
                    if (i == 0)
                    {
                        containersList.con0[0] = ceciliaDevice.containers[0].currentVolume;
                        containersList.con0[1] = ceciliaDevice.containers[0].redLine;
                    }
                    if (i == 1)
                    {
                        containersList.con1[0] = ceciliaDevice.containers[1].currentVolume;
                        containersList.con1[1] = ceciliaDevice.containers[1].redLine;
                    }
                    if (i == 2)
                    {
                        containersList.con2[0] = ceciliaDevice.containers[2].currentVolume;
                        containersList.con2[1] = ceciliaDevice.containers[2].redLine;
                    }
                    if (i == 3)
                    {
                        containersList.con3[0] = ceciliaDevice.containers[3].currentVolume;
                        containersList.con3[1] = ceciliaDevice.containers[3].redLine;
                    }
                    if (i == 4)
                    {
                        containersList.con4[0] = ceciliaDevice.containers[4].currentVolume;
                        containersList.con4[1] = ceciliaDevice.containers[4].redLine;
                    }
                    if (i == 5)
                    {
                        containersList.con5[0] = ceciliaDevice.containers[5].currentVolume;
                        containersList.con5[1] = ceciliaDevice.containers[5].redLine;
                    }
                    if (i == 6)
                    {
                        containersList.con6[0] = ceciliaDevice.containers[6].currentVolume;
                        containersList.con6[1] = ceciliaDevice.containers[6].redLine;
                    }
                    if (i == 7)
                    {
                        containersList.con7[0] = ceciliaDevice.containers[7].currentVolume;
                        containersList.con7[1] = ceciliaDevice.containers[7].redLine;
                    }
                    if (i == 8)
                    {
                        containersList.con8[0] = ceciliaDevice.containers[8].currentVolume;
                        containersList.con8[1] = ceciliaDevice.containers[8].redLine;
                    }
                    if (i == 9)
                    {
                        containersList.con9[0] = ceciliaDevice.containers[9].currentVolume;
                        containersList.con9[1] = ceciliaDevice.containers[9].redLine;
                    }
                    if (i == 10)
                    {
                        containersList.con10[0] = ceciliaDevice.containers[10].currentVolume;
                        containersList.con10[1] = ceciliaDevice.containers[10].redLine;
                    }
                    if (i == 11)
                    {
                        containersList.con11[0] = ceciliaDevice.containers[11].currentVolume;
                        containersList.con11[1] = ceciliaDevice.containers[11].redLine;
                        ContainersChecked = true;

                    }
                    if (manager.touchScreenUIManager.DrinksMenuParent.activeSelf)
                    {
                        if (ceciliaDevice.containers[i].currentVolume <= ceciliaDevice.containers[i].redLine)
                        {
                            AllContainersSoldOut.Add(i.ToString());
                            if (ContainersChecked)
                            {
                                DrinksButtonSoldOut();
                            }
                        }
                        else
                        {
                            EnableDrinkButtons();
                        }
                    }
                }
                
            }
            else
            {
                Debug.Log("Document {0} does not exist! " + snapshot.Id);
            }
        });
    }

    public void DrinksButtonSoldOut() //ofek change
    {
        for (int i = 0; i < manager.conversationSettings.conversationCommands.Length; i++)
        {
           
            if (manager.conversationSettings.conversationCommands[i].output.Length == 0)
            {
                Debug.Log("DO NOTHING");
                break;
            }
            else
            {

                string[] SplitString = manager.conversationSettings.conversationCommands[i].output.Split('&', ':');
                Debug.Log(SplitString.Length);
                for (int k = 0; k < SplitString.Length; k++)
                {
                    if (k % 2 == 0) //Check if k is even 
                    {

                        AlltheNumbersContainers.Add(SplitString[k]);
                        for (int j = 0; j < AllContainersSoldOut.Count; j++)
                        {
                            if (SplitString[k].Equals(AllContainersSoldOut[j]))
                            {
                                DrinksChilldrenMenu[i].GetComponent<Button>().interactable = false;
                                DrinksChilldrenMenu[i].GetComponent<Image>().color = new Color32(255, 255, 255, 107); // change the color image to grey // disable drinks
                            }
                        }
                    }
                }



                Debug.Log("DISABLE CONTAINER" + i);
            }
        }
    }

    public void EnableDrinkButtons() //**********
    {
        for (int i = 0; i < manager.conversationSettings.conversationCommands.Length; i++)
        {
            if (manager.conversationSettings.conversationCommands[i].output.Length != 0)
            {
                DrinksChilldrenMenu[i].GetComponent<Button>().interactable = true;
                DrinksChilldrenMenu[i].GetComponent<Image>().color = new Color32(255, 255, 255, 225); // change the color image to normal // enable drinks
            }
            else
            {
                break;
            }
        }
    }
     



        async void GetDocumentProperty(string propertyName)
        {
            DocumentReference docRef = db.Collection("cecilia_devices").Document(manager.ceciliaID);
            docRef.Listen(snapshot =>
            {
                if (snapshot.Exists)
                {
                    Debug.Log($"Document data for document: {snapshot.Id}");
                    Dictionary<string, object> devices = snapshot.ToDictionary();
                    foreach (KeyValuePair<string, object> pair in devices)
                    {
                        Debug.Log($"pair: {pair.Key} , {pair.Value} , {pair.Value.GetType()}");
                        if (pair.Key == propertyName)
                        {
                            //Debug.Log($"containers.Length = {containers.containers.Length}\n" +
                            //    $"containers[0].name = {containers.containers[0].name}");
                        }
                    }
                }
                else
                {
                    Debug.Log("Document {0} does not exist! " + snapshot.Id);
                }
            });
        }
    



    //Utilities:

    protected string documentId = "";
    protected string collectionPath = "col1";
    protected string fieldContents;


    private void GetPropertyValues(object obj)
    {
        Type t = obj.GetType();
        Debug.Log("Type is: {0}" + t.Name);
        PropertyInfo[] props = t.GetProperties();
        Debug.Log("Properties (N = {0}):" + props.Length);
        foreach (var prop in props)
        {
            if (prop.GetIndexParameters().Length == 0)
                Debug.Log("   {0} ({1}): {2} " + prop.Name + ", " +
                                  prop.PropertyType.Name + ", " +
                                  prop.GetValue(obj));
            else
                Debug.Log("   {0} ({1}): <Indexed> " + prop.Name + ", " +
                                  prop.PropertyType.Name);
        }

    }

    private CollectionReference GetCollectionReference()
    {
        return db.Collection(collectionPath);
    }
    private DocumentReference GetDocumentReference()
    {
        if (documentId == "")
        {
            Debug.Log(GetCollectionReference().Document().Id);
            Debug.Log(GetCollectionReference().Document().Collection("cecilia_devices").Id);
            return GetCollectionReference().Document();
        }
        return GetCollectionReference().Document(documentId);
    }

    private static string DictToString(IDictionary<string, object> d)
    {
        return "{ " + d
            .Select(kv => "(" + kv.Key + ", " + kv.Value + ")")
            .Aggregate("", (current, next) => current + next + ", ")
            + "}";
    }


    async void ReadDoc(DocumentReference doc)
    {
        Task<DocumentSnapshot> getTask = doc.GetSnapshotAsync();
        await getTask;
        if (!(getTask.IsFaulted || getTask.IsCanceled))
        {
            DocumentSnapshot snap = getTask.Result;
            // TODO(rgowman): Handle `!snap.exists()` case.
            IDictionary<string, object> resultData = snap.ToDictionary();
            fieldContents = "Ok: " + DictToString(resultData);
        }
        else
        {
            fieldContents = "Error";
        }
    }



}



[FirestoreData]
public class Containers
{
    [FirestoreProperty]
    public Device[] containers { get; set; }
}

[FirestoreData]
public class Device
{
    [FirestoreProperty]
    public int container_number { get; set; }
    [FirestoreProperty]
    public int currentVolume { get; set; }
    [FirestoreProperty]
    public string image { get; set; }
    [FirestoreProperty]
    public int maxVolume { get; set; }
    [FirestoreProperty]
    public string name { get; set; }
    [FirestoreProperty]
    public int orangeLine { get; set; }
    [FirestoreProperty]
    public int redLine { get; set; }
}

[FirestoreData]
public class CeciliaDevice
{
    [FirestoreProperty]
    public string[] ceciliaUsers { get; set; }
    [FirestoreProperty]
    public string color { get; set; }
    [FirestoreProperty]
    public Device[] containers { get; set; }
    [FirestoreProperty]
    public string duid { get; set; }
    [FirestoreProperty]
    public bool faceRecognition { get; set; }
    [FirestoreProperty]
    public bool idScanner { get; set; }
    [FirestoreProperty]
    public string manufacturingDate { get; set; }
    [FirestoreProperty]
    public string name { get; set; }
    [FirestoreProperty]
    public int softwareVersion { get; set; }
    [FirestoreProperty]
    public bool squarePaymentTerminal { get; set; }

}
